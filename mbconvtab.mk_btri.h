#include "mbcesconf.h"
#include "mblangconf.h"
%%BEGIN default_conv_tab
"ascii",mb_conv_ascii
"ces",mb_conv_for_ces
#ifdef USE_UCS
"to-ucs",mb_conv1_to_ucs
"full-to-half",mb_conv_f2h
"f2h",mb_conv_f2h
"half-to-full",mb_conv_h2f
"h2f",mb_conv_h2f
"jisx0213",mb_conv_to_jisx0213
"jisx0213-aggressive",mb_conv_to_jisx0213_aggressive
#ifdef USE_WIN1252
"ms-latin1",mb_conv_ms_latin1
#endif
"jis0208-to-ucs-extra",mb_jisx0208_to_ucs_extra
"ucs-to-jis0208-extra",mb_ucs_to_jisx0208_extra
#endif
#ifdef USE_UCS
"to-gb2312",mb_conv_ws_to_GB2312
"to-cn-gb",mb_conv_ws_to_GB2312
#endif
#ifdef USE_UCS
"to-cn-gb-isoir165",mb_conv_ws_to_GB_ISO_IR_165
#endif
#ifdef USE_UCS
"to-iso-2022-cn",mb_conv_ws_to_CN
#endif
#ifdef USE_UCS
"to-iso-2022-cn-ext",mb_conv_ws_to_CN_EXT
#endif
#ifdef USE_UCS
"to-euc-jp",mb_conv_ws_to_EUC_JP
"to-x-euc-jp",mb_conv_ws_to_EUC_JP
"to-ujis",mb_conv_ws_to_EUC_JP
"to-jis8",mb_conv_ws_to_EUC_JP
#endif
#ifdef USE_UCS
"to-euc-jisx0213",mb_conv_ws_to_EUC_JISX0213
"to-x-euc-jisx0213",mb_conv_ws_to_EUC_JISX0213
#endif
#ifdef USE_UCS
"to-iso-2022-jp",mb_conv_ws_to_ISO2022JP
"to-jis",mb_conv_ws_to_ISO2022JP
"to-jis7",mb_conv_ws_to_ISO2022JP
#endif
#ifdef USE_UCS
"to-iso-2022-jp-2",mb_conv_ws_to_ISO2022JP2
#endif
#ifdef USE_UCS
"to-iso-2022-jp-3",mb_conv_ws_to_ISO2022JP3
#endif
#ifdef USE_UCS
"to-euc-kr",mb_conv_ws_to_EUC_KR
"to-x-euc-kr",mb_conv_ws_to_EUC_KR
#endif
#ifdef USE_UCS
"to-iso-2022-kr",mb_conv_ws_to_ISO2022KR
#endif
#ifdef USE_UCS
"to-iso-8859-1",mb_conv_ws_to_ISO8859_1
#endif
#ifdef USE_UCS
"to-iso-8859-2",mb_conv_ws_to_ISO8859_2
#endif
#ifdef USE_UCS
"to-iso-8859-3",mb_conv_ws_to_ISO8859_3
#endif
#ifdef USE_UCS
"to-iso-8859-4",mb_conv_ws_to_ISO8859_4
#endif
#ifdef USE_UCS
"to-iso-8859-5",mb_conv_ws_to_ISO8859_5
#endif
#ifdef USE_UCS
"to-iso-8859-6",mb_conv_ws_to_ISO8859_6
#endif
#ifdef USE_UCS
"to-iso-8859-7",mb_conv_ws_to_ISO8859_7
#endif
#ifdef USE_UCS
"to-iso-8859-8",mb_conv_ws_to_ISO8859_8
#endif
#ifdef USE_UCS
"to-iso-8859-9",mb_conv_ws_to_ISO8859_9
#endif
#ifdef USE_UCS
"to-iso-8859-10",mb_conv_ws_to_ISO8859_10
#endif
#ifdef USE_UCS
"to-x-ctext",mb_conv_ws_to_XCTEXT
#endif
#if defined(USE_KOI8R) && defined(USE_UCS)
"to-koi8-r",mb_conv_ws_to_KOI8R
#endif
#if defined(USE_KOI8U) && defined(USE_UCS)
"to-koi8-u",mb_conv_ws_to_KOI8U
#endif
#if defined(USE_WIN1250) && defined(USE_UCS)
"to-windows-1250",mb_conv_ws_to_WIN1250
#endif
#if defined(USE_WIN1251) && defined(USE_UCS)
"to-windows-1251",mb_conv_ws_to_WIN1251
#endif
#if defined(USE_WIN1252) && defined(USE_UCS)
"to-windows-1252",mb_conv_ws_to_WIN1252
#endif
#if defined(USE_WIN1253) && defined(USE_UCS)
"to-windows-1253",mb_conv_ws_to_WIN1253
#endif
#if defined(USE_WIN1254) && defined(USE_UCS)
"to-windows-1254",mb_conv_ws_to_WIN1254
#endif
#if defined(USE_WIN1255) && defined(USE_UCS)
"to-windows-1255",mb_conv_ws_to_WIN1255
#endif
#if defined(USE_WIN1256) && defined(USE_UCS)
"to-windows-1256",mb_conv_ws_to_WIN1256
#endif
#if defined(USE_WIN1257) && defined(USE_UCS)
"to-windows-1257",mb_conv_ws_to_WIN1257
#endif
#if defined(USE_WIN1258) && defined(USE_UCS)
"to-windows-1258",mb_conv_ws_to_WIN1258
#endif
#if defined(USE_SJIS) && defined(USE_UCS)
"to-shift_jis",mb_conv_ws_to_SJIS
"to-x-sjis",mb_conv_ws_to_SJIS
"to-sjis",mb_conv_ws_to_SJIS
"to-shift-jis",mb_conv_ws_to_SJIS
#endif
#if defined(USE_SJIS0213) && defined(USE_UCS)
"to-shift_jisx0213",mb_conv_ws_to_SJIS0213
"to-shift-jisx0213",mb_conv_ws_to_SJIS0213
#endif
#if defined(USE_BIG5) && defined(USE_UCS)
"to-big5",mb_conv_ws_to_BIG5
"to-cn-big5",mb_conv_ws_to_BIG5
#endif
#if defined(USE_JOHAB) && defined(USE_UCS)
"to-x-johab",mb_conv_ws_to_JOHAB
#endif
#if defined(USE_UHANG) && defined(USE_UCS)
"to-x-unified-hangle",mb_conv_ws_to_UHANG
"to-x-uhc",mb_conv_ws_to_UHANG
#endif
#if defined(USE_EUC_TW) && defined(USE_UCS)
"to-x-euc-tw",mb_conv_ws_to_EUC_TW
"to-euc-tw",mb_conv_ws_to_EUC_TW
#endif
#if defined(USE_EUC_JISX0213_PACKED) && defined(USE_UCS)
"to-x-euc-jisx0213-packed",mb_conv_ws_to_EUC_JISX0213_PACKED
#endif
#if defined(USE_GBK) && defined(USE_UCS)
"to-x-gbk",mb_conv_ws_to_GBK
#endif
#if defined(USE_GBK2K) && defined(USE_UCS)
"to-x-gb-18030-2000",mb_conv_ws_to_GBK2K
"to-x-gbk2k",mb_conv_ws_to_GBK2K
#endif
#ifdef USE_UCS
"to-tis-620",mb_conv_ws_to_TIS620
"to-iso-8859-11",mb_conv_ws_to_TIS620
#endif
