#include "mb.h"
mb_wchar_range_t mb_cm_wcv_CLx3x1x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xF,1U},
};
mb_wchar_range_t mb_cm_wcv_CLx3x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1F,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xFF,16U},
};
mb_wchar_range_t mb_cm_dectab_CLx3x1x0x0[] = {
{0x0,0xF,0U},
};
size_t mb_cm_dectab_bv_CLx3x1x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x1x0x0[] = {
{0x30,0x3F,mb_cm_wcv_CLx3x1x0x0x0,NULL,NULL,{NULL,14U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x3x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7F,32U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x0x0[] = {
{0x0,0x1F,0U},
};
size_t mb_cm_dectab_bv_CLx3x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x0x0[] = {
{0x40,0x5F,mb_cm_wcv_CLx3x0x0x0,NULL,NULL,{NULL,11U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x0x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_dectab_CLx3x1x0[] = {
{0x0,0xFF,0U},
};
size_t mb_cm_dectab_bv_CLx3x1x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x1x0[] = {
{0x20,0x2F,mb_cm_wcv_CLx3x1x0x0,NULL,NULL,{mb_cm_nodev_CLx3x1x0x0,1U,mb_cm_dectab_CLx3x1x0x0,mb_cm_dectab_bv_CLx3x1x0x0}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,3U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x1x1[] = {
{0x0,0x2,0U},
};
size_t mb_cm_dectab_bv_CLx3x1x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x1x1[] = {
{0x4A,0x4C,mb_cm_wcv_CLx3x1x1x0,NULL,NULL,{NULL,12U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,63U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x3x0[] = {
{0x0,0x3E,0U},
};
size_t mb_cm_dectab_bv_CLx3x3x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x3x0[] = {
{0x40,0x7E,mb_cm_wcv_CLx3x3x0x0,NULL,NULL,{NULL,13U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5F,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,3U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7F,128U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x0[] = {
{0x0,0x2,1U},
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_CLx3x0[] = {
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
2U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_CLx3x0[] = {
{0x28,0x2B,mb_cm_wcv_CLx3x0x0,mb_escfun_cs_94x94,NULL,{mb_cm_nodev_CLx3x0x0,1U,mb_cm_dectab_CLx3x0x0,mb_cm_dectab_bv_CLx3x0x0}},
{0x40,0x42,mb_cm_wcv_CLx3x0x1,mb_escfun_cs_94x94,NULL,{NULL,4U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,3U},
{0,0,0},
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_dectab_CLx3x1[] = {
{0x0,0x0,2U},
{0x0,0x0,3U},
{0x0,0x2,1U},
{0x0,0xFF,0U},
};
size_t mb_cm_dectab_bv_CLx3x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
1U,
2U,
2U,
2U,
2U,
2U,
2U,
3U,
3U,
4U,
};
mb_char_node_t mb_cm_nodev_CLx3x1[] = {
{0x21,0x21,mb_cm_wcv_CLx3x1x0,mb_escfun_cs_misc,NULL,{mb_cm_nodev_CLx3x1x0,1U,mb_cm_dectab_CLx3x1x0,mb_cm_dectab_bv_CLx3x1x0}},
{0x2F,0x2F,mb_cm_wcv_CLx3x1x1,mb_escfun_cs_utf16,NULL,{mb_cm_nodev_CLx3x1x1,1U,mb_cm_dectab_CLx3x1x1,mb_cm_dectab_bv_CLx3x1x1}},
{0x40,0x40,mb_cm_wcv_CLx3x1x2,mb_escfun_cs_return,NULL,{NULL,5U,NULL,NULL}},
{0x47,0x47,mb_cm_wcv_CLx3x1x3,mb_escfun_cs_utf8,NULL,{NULL,6U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,63U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x2[] = {
{0x0,0x3E,0U},
};
size_t mb_cm_dectab_bv_CLx3x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x2[] = {
{0x40,0x7E,mb_cm_wcv_CLx3x2x0,NULL,NULL,{NULL,7U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xFB,63U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xFB,63U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x3[] = {
{0x0,0x3E,1U},
{0x0,0x3E,0U},
};
size_t mb_cm_dectab_bv_CLx3x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_CLx3x3[] = {
{0x21,0x21,mb_cm_wcv_CLx3x3x0,mb_escfun_cs_94_i,NULL,{mb_cm_nodev_CLx3x3x0,1U,mb_cm_dectab_CLx3x3x0,mb_cm_dectab_bv_CLx3x3x0}},
{0x40,0x7E,mb_cm_wcv_CLx3x3x1,mb_escfun_cs_94,NULL,{NULL,8U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBC,63U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x4[] = {
{0x0,0x3E,0U},
};
size_t mb_cm_dectab_bv_CLx3x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x4[] = {
{0x40,0x7E,mb_cm_wcv_CLx3x4x0,NULL,NULL,{NULL,9U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBF,96U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_CLx3x5[] = {
{0x0,0x5F,0U},
};
size_t mb_cm_dectab_bv_CLx3x5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_CLx3x5[] = {
{0x20,0x7F,mb_cm_wcv_CLx3x5x0,NULL,NULL,{NULL,10U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_CLx3x6[] = {
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3x7[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx0[] = {
{0x0,0x2,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx1[] = {
{0x3,0x4,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx2[] = {
{0,0,0},
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_CLx3[] = {
{0,0,0},
{0,0,0},
{0x0,0x1,2U},
{0x0,0x2,3U},
{0x0,0x2,3U},
{0x0,0x0,1U},
{0x0,0x0,1U},
{0x0,0x3E,63U},
{0x0,0xFB,252U},
{0x0,0xBC,189U},
{0x0,0xBF,192U},
{0x0,0x7F,128U},
{0x0,0x2,3U},
{0x0,0xFB,252U},
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_dectab_CLx3[] = {
{0x0,0x1,6U},
{0x0,0x2,7U},
{0x0,0x2,0U},
{0x0,0x0,1U},
{0x0,0x0,1U},
{0x0,0x3E,2U},
{0x0,0xFB,3U},
{0x0,0xBC,4U},
{0x0,0xBF,5U},
{0x0,0x7F,0U},
{0x0,0x2,1U},
{0x0,0xFB,3U},
{0x0,0xFF,1U},
};
size_t mb_cm_dectab_bv_CLx3[] = {
0U,
0U,
0U,
1U,
2U,
3U,
4U,
5U,
6U,
7U,
8U,
9U,
10U,
11U,
12U,
13U,
};
mb_char_node_t mb_cm_nodev_CLx3[] = {
{0x24,0x24,mb_cm_wcv_CLx3x0,NULL,NULL,{mb_cm_nodev_CLx3x0,2U,mb_cm_dectab_CLx3x0,mb_cm_dectab_bv_CLx3x0}},
{0x25,0x25,mb_cm_wcv_CLx3x1,NULL,NULL,{mb_cm_nodev_CLx3x1,4U,mb_cm_dectab_CLx3x1,mb_cm_dectab_bv_CLx3x1}},
{0x26,0x26,mb_cm_wcv_CLx3x2,mb_escfun_cs_rev,NULL,{mb_cm_nodev_CLx3x2,1U,mb_cm_dectab_CLx3x2,mb_cm_dectab_bv_CLx3x2}},
{0x28,0x2B,mb_cm_wcv_CLx3x3,NULL,NULL,{mb_cm_nodev_CLx3x3,2U,mb_cm_dectab_CLx3x3,mb_cm_dectab_bv_CLx3x3}},
{0x2D,0x2F,mb_cm_wcv_CLx3x4,mb_escfun_cs_96,NULL,{mb_cm_nodev_CLx3x4,1U,mb_cm_dectab_CLx3x4,mb_cm_dectab_bv_CLx3x4}},
{0x4E,0x4F,mb_cm_wcv_CLx3x5,mb_iso2022_SSL_encoder,NULL,{mb_cm_nodev_CLx3x5,1U,mb_cm_dectab_CLx3x5,mb_cm_dectab_bv_CLx3x5}},
{0x6E,0x6F,mb_cm_wcv_CLx3x6,mb_escfun_sl23,NULL,{NULL,2U,NULL,NULL}},
{0x7C,0x7E,mb_cm_wcv_CLx3x7,mb_escfun_sr,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_CL[] = {
{0x0,0x2,0U},
{0x3,0x4,1U},
{0x0,0x1,2U},
{0x0,0x1,3U},
{0x0,0x2,3U},
{0x0,0x2,3U},
{0x0,0x0,3U},
{0x0,0x0,3U},
{0x0,0x3E,3U},
{0x0,0xFB,3U},
{0x0,0xBC,3U},
{0x0,0xBF,3U},
{0x0,0x7F,3U},
{0x0,0x2,3U},
{0x0,0xFB,3U},
{0x0,0xFF,3U},
};
size_t mb_cm_dectab_bv_CL[] = {
0U,
2U,
3U,
4U,
5U,
6U,
7U,
8U,
9U,
10U,
11U,
12U,
13U,
14U,
15U,
16U,
};
mb_char_node_t mb_cm_nodev_CL[] = {
{0x8,0xA,mb_cm_wcv_CLx0,mb_ctl_encoder,NULL,{NULL,0U,NULL,NULL}},
{0xC,0xD,mb_cm_wcv_CLx1,mb_ctl_encoder,NULL,{NULL,0U,NULL,NULL}},
{0xE,0xF,mb_cm_wcv_CLx2,mb_escfun_sl01,NULL,{NULL,1U,NULL,NULL}},
{0x1B,0x1B,mb_cm_wcv_CLx3,NULL,NULL,{mb_cm_nodev_CLx3,8U,mb_cm_dectab_CLx3,mb_cm_dectab_bv_CLx3}},
};
mb_char_map_t mb_cm_CL = {
mb_cm_nodev_CL,
4U,
mb_cm_dectab_CL,
mb_cm_dectab_bv_CL,
};
mb_wchar_range_t mb_wcv_CL[] = {
{0x0,0x4,5U},
{0x0,0x1,2U},
{0x0,0x1,2U},
{0x0,0x2,3U},
{0x0,0x2,3U},
{0x0,0x0,1U},
{0x0,0x0,1U},
{0x0,0x3E,63U},
{0x0,0xFB,252U},
{0x0,0xBC,189U},
{0x0,0xBF,192U},
{0x0,0x7F,128U},
{0x0,0x2,3U},
{0x0,0xFB,252U},
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_wcv_GLx0[] = {
{0x0,0x5F,1U},
};
mb_wchar_range_t mb_cm_dectab_GL[] = {
{0x0,0x5F,0U},
};
size_t mb_cm_dectab_bv_GL[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GL[] = {
{0x20,0x7F,mb_cm_wcv_GLx0,mb_iso2022_GL_encoder,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_GL = {
mb_cm_nodev_GL,
1U,
mb_cm_dectab_GL,
mb_cm_dectab_bv_GL,
};
mb_wchar_range_t mb_wcv_GL[] = {
{0x20,0x7F,96U},
};
mb_wchar_range_t mb_cm_wcv_94x94Lx0x0x0[] = {
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_94x94Lx0x0[] = {
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_94x94Lx0x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_94x94Lx0x0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94x94Lx0x0[] = {
{0x21,0x7E,mb_cm_wcv_94x94Lx0x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_94x94Lx0[] = {
{0x0,0x1141FF,8836U},
};
mb_wchar_range_t mb_cm_dectab_94x94Lx0[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_94x94Lx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94x94Lx0[] = {
{0x21,0x7E,mb_cm_wcv_94x94Lx0x0,NULL,NULL,{mb_cm_nodev_94x94Lx0x0,1U,mb_cm_dectab_94x94Lx0x0,mb_cm_dectab_bv_94x94Lx0x0}},
};
mb_wchar_range_t mb_cm_dectab_94x94L[] = {
{0x0,0x1141FF,0U},
};
size_t mb_cm_dectab_bv_94x94L[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94x94L[] = {
{0x0,0x7F,mb_cm_wcv_94x94Lx0,NULL,NULL,{mb_cm_nodev_94x94Lx0,1U,mb_cm_dectab_94x94Lx0,mb_cm_dectab_bv_94x94Lx0}},
};
mb_char_map_t mb_cm_94x94L = {
mb_cm_nodev_94x94L,
1U,
mb_cm_dectab_94x94L,
mb_cm_dectab_bv_94x94L,
};
mb_wchar_range_t mb_wcv_94x94L[] = {
{0x213E00,0x327FFF,1131008U},
};
mb_wchar_range_t mb_cm_wcv_96Lx0x0[] = {
{0x0,0x5F,1U},
};
mb_wchar_range_t mb_cm_wcv_96Lx0[] = {
{0x0,0x5FFF,96U},
};
mb_wchar_range_t mb_cm_dectab_96Lx0[] = {
{0x0,0x5F,0U},
};
size_t mb_cm_dectab_bv_96Lx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_96Lx0[] = {
{0x20,0x7F,mb_cm_wcv_96Lx0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_96L[] = {
{0x0,0x5FFF,0U},
};
size_t mb_cm_dectab_bv_96L[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_96L[] = {
{0x0,0xFF,mb_cm_wcv_96Lx0,NULL,NULL,{mb_cm_nodev_96Lx0,1U,mb_cm_dectab_96Lx0,mb_cm_dectab_bv_96Lx0}},
};
mb_char_map_t mb_cm_96L = {
mb_cm_nodev_96L,
1U,
mb_cm_dectab_96L,
mb_cm_dectab_bv_96L,
};
mb_wchar_range_t mb_wcv_96L[] = {
{0x205E00,0x20BDFF,24576U},
};
mb_wchar_range_t mb_cm_wcv_94Lx0x0[] = {
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_94Lx0[] = {
{0x0,0x5DFF,94U},
};
mb_wchar_range_t mb_cm_dectab_94Lx0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_94Lx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94Lx0[] = {
{0x21,0x7E,mb_cm_wcv_94Lx0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_94L[] = {
{0x0,0x5DFF,0U},
};
size_t mb_cm_dectab_bv_94L[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94L[] = {
{0x0,0xFF,mb_cm_wcv_94Lx0,NULL,NULL,{mb_cm_nodev_94Lx0,1U,mb_cm_dectab_94Lx0,mb_cm_dectab_bv_94Lx0}},
};
mb_char_map_t mb_cm_94L = {
mb_cm_nodev_94L,
1U,
mb_cm_dectab_94L,
mb_cm_dectab_bv_94L,
};
mb_wchar_range_t mb_wcv_94L[] = {
{0x200000,0x205DFF,24064U},
};
mb_wchar_range_t mb_cm_wcv_GRx0x0[] = {
{0x0,0x5F,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GRx0[] = {
{0x0,0xBF,96U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GRx0[] = {
{0x0,0x5F,0U},
};
size_t mb_cm_dectab_bv_GRx0[] = {
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GRx0[] = {
{0xA0,0xFF,mb_cm_wcv_GRx0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GRx1[] = {
{0,0,0},
{0x0,0x5F,1U},
};
mb_wchar_range_t mb_cm_dectab_GR[] = {
{0x0,0xBF,0U},
{0x0,0x5F,1U},
};
size_t mb_cm_dectab_bv_GR[] = {
0U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GR[] = {
{0x8E,0x8F,mb_cm_wcv_GRx0,mb_iso2022_SSR_encoder,NULL,{mb_cm_nodev_GRx0,1U,mb_cm_dectab_GRx0,mb_cm_dectab_bv_GRx0}},
{0xA0,0xFF,mb_cm_wcv_GRx1,mb_iso2022_GR_encoder,NULL,{NULL,1U,NULL,NULL}},
};
mb_char_map_t mb_cm_GR = {
mb_cm_nodev_GR,
2U,
mb_cm_dectab_GR,
mb_cm_dectab_bv_GR,
};
mb_wchar_range_t mb_wcv_GR[] = {
{0x0,0xBF,192U},
{0xA0,0xFF,96U},
};
mb_wchar_range_t mb_cm_wcv_94x94Rx0x0x0[] = {
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_94x94Rx0x0[] = {
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_94x94Rx0x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_94x94Rx0x0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94x94Rx0x0[] = {
{0xA1,0xFE,mb_cm_wcv_94x94Rx0x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_94x94Rx0[] = {
{0x0,0x1141FF,8836U},
};
mb_wchar_range_t mb_cm_dectab_94x94Rx0[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_94x94Rx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94x94Rx0[] = {
{0xA1,0xFE,mb_cm_wcv_94x94Rx0x0,NULL,NULL,{mb_cm_nodev_94x94Rx0x0,1U,mb_cm_dectab_94x94Rx0x0,mb_cm_dectab_bv_94x94Rx0x0}},
};
mb_wchar_range_t mb_cm_dectab_94x94R[] = {
{0x0,0x1141FF,0U},
};
size_t mb_cm_dectab_bv_94x94R[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94x94R[] = {
{0x0,0x7F,mb_cm_wcv_94x94Rx0,NULL,NULL,{mb_cm_nodev_94x94Rx0,1U,mb_cm_dectab_94x94Rx0,mb_cm_dectab_bv_94x94Rx0}},
};
mb_char_map_t mb_cm_94x94R = {
mb_cm_nodev_94x94R,
1U,
mb_cm_dectab_94x94R,
mb_cm_dectab_bv_94x94R,
};
mb_wchar_range_t mb_wcv_94x94R[] = {
{0x213E00,0x327FFF,1131008U},
};
mb_wchar_range_t mb_cm_wcv_96Rx0x0[] = {
{0x0,0x5F,1U},
};
mb_wchar_range_t mb_cm_wcv_96Rx0[] = {
{0x0,0x5FFF,96U},
};
mb_wchar_range_t mb_cm_dectab_96Rx0[] = {
{0x0,0x5F,0U},
};
size_t mb_cm_dectab_bv_96Rx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_96Rx0[] = {
{0xA0,0xFF,mb_cm_wcv_96Rx0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_96R[] = {
{0x0,0x5FFF,0U},
};
size_t mb_cm_dectab_bv_96R[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_96R[] = {
{0x0,0xFF,mb_cm_wcv_96Rx0,NULL,NULL,{mb_cm_nodev_96Rx0,1U,mb_cm_dectab_96Rx0,mb_cm_dectab_bv_96Rx0}},
};
mb_char_map_t mb_cm_96R = {
mb_cm_nodev_96R,
1U,
mb_cm_dectab_96R,
mb_cm_dectab_bv_96R,
};
mb_wchar_range_t mb_wcv_96R[] = {
{0x205E00,0x20BDFF,24576U},
};
mb_wchar_range_t mb_cm_wcv_94Rx0x0[] = {
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_94Rx0[] = {
{0x0,0x5DFF,94U},
};
mb_wchar_range_t mb_cm_dectab_94Rx0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_94Rx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94Rx0[] = {
{0xA1,0xFE,mb_cm_wcv_94Rx0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_94R[] = {
{0x0,0x5DFF,0U},
};
size_t mb_cm_dectab_bv_94R[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_94R[] = {
{0x0,0xFF,mb_cm_wcv_94Rx0,NULL,NULL,{mb_cm_nodev_94Rx0,1U,mb_cm_dectab_94Rx0,mb_cm_dectab_bv_94Rx0}},
};
mb_char_map_t mb_cm_94R = {
mb_cm_nodev_94R,
1U,
mb_cm_dectab_94R,
mb_cm_dectab_bv_94R,
};
mb_wchar_range_t mb_wcv_94R[] = {
{0x200000,0x205DFF,24064U},
};
mb_wchar_range_t mb_cm_wcv_CLGLx0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_CLGL[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_CLGL[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_CLGL[] = {
{0x0,0x7F,mb_cm_wcv_CLGLx0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_CLGL = {
mb_cm_nodev_CLGL,
1U,
mb_cm_dectab_CLGL,
mb_cm_dectab_bv_CLGL,
};
mb_wchar_range_t mb_wcv_CLGL[] = {
{0x0,0x7F,128U},
};
mb_wchar_range_t mb_cm_wcv_ASCIIx0[] = {
{0x0,0x5E,1U},
};
mb_wchar_range_t mb_cm_dectab_ASCII[] = {
{0x0,0x5E,0U},
};
size_t mb_cm_dectab_bv_ASCII[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_ASCII[] = {
{0x21,0x7F,mb_cm_wcv_ASCIIx0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_ASCII = {
mb_cm_nodev_ASCII,
1U,
mb_cm_dectab_ASCII,
mb_cm_dectab_bv_ASCII,
};
mb_wchar_range_t mb_wcv_ASCII[] = {
{0x21,0x7F,95U},
};
mb_wchar_range_t mb_cm_wcv_UTF8x2x0x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x3F,1U},
};
mb_wchar_range_t mb_cm_wcv_UTF8x1x0x0[] = {
{0,0,0},
{0x0,0x3F,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_UTF8x2x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0xFFF,64U},
};
mb_wchar_range_t mb_cm_dectab_UTF8x2x0x0[] = {
{0x0,0x3F,0U},
};
size_t mb_cm_dectab_bv_UTF8x2x0x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF8x2x0x0[] = {
{0x80,0xBF,mb_cm_wcv_UTF8x2x0x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_UTF8x0x0[] = {
{0x0,0x3F,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_UTF8x1x0[] = {
{0,0,0},
{0x0,0xFFF,64U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_UTF8x1x0[] = {
{0x0,0x3F,0U},
};
size_t mb_cm_dectab_bv_UTF8x1x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF8x1x0[] = {
{0x80,0xBF,mb_cm_wcv_UTF8x1x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_UTF8x2x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x3FFFF,4096U},
};
mb_wchar_range_t mb_cm_dectab_UTF8x2x0[] = {
{0x0,0xFFF,0U},
};
size_t mb_cm_dectab_bv_UTF8x2x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF8x2x0[] = {
{0x80,0xBF,mb_cm_wcv_UTF8x2x0x0,NULL,NULL,{mb_cm_nodev_UTF8x2x0x0,1U,mb_cm_dectab_UTF8x2x0x0,mb_cm_dectab_bv_UTF8x2x0x0}},
};
mb_wchar_range_t mb_cm_wcv_UTF8x0[] = {
{0x0,0x7FF,64U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_UTF8x0[] = {
{0x0,0x3F,0U},
};
size_t mb_cm_dectab_bv_UTF8x0[] = {
0U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF8x0[] = {
{0x80,0xBF,mb_cm_wcv_UTF8x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_UTF8x1[] = {
{0,0,0},
{0x0,0xFFFF,4096U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_UTF8x1[] = {
{0x0,0xFFF,0U},
};
size_t mb_cm_dectab_bv_UTF8x1[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF8x1[] = {
{0x80,0xBF,mb_cm_wcv_UTF8x1x0,NULL,NULL,{mb_cm_nodev_UTF8x1x0,1U,mb_cm_dectab_UTF8x1x0,mb_cm_dectab_bv_UTF8x1x0}},
};
mb_wchar_range_t mb_cm_wcv_UTF8x2[] = {
{0,0,0},
{0,0,0},
{0x0,0x1FFFFF,262144U},
};
mb_wchar_range_t mb_cm_dectab_UTF8x2[] = {
{0x0,0x3FFFF,0U},
};
size_t mb_cm_dectab_bv_UTF8x2[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF8x2[] = {
{0x80,0xBF,mb_cm_wcv_UTF8x2x0,NULL,NULL,{mb_cm_nodev_UTF8x2x0,1U,mb_cm_dectab_UTF8x2x0,mb_cm_dectab_bv_UTF8x2x0}},
};
mb_wchar_range_t mb_cm_dectab_UTF8[] = {
{0x0,0x7FF,0U},
{0x0,0xFFFF,1U},
{0x0,0x1FFFFF,2U},
};
size_t mb_cm_dectab_bv_UTF8[] = {
0U,
1U,
2U,
3U,
};
mb_char_node_t mb_cm_nodev_UTF8[] = {
{0xC0,0xDF,mb_cm_wcv_UTF8x0,NULL,NULL,{mb_cm_nodev_UTF8x0,1U,mb_cm_dectab_UTF8x0,mb_cm_dectab_bv_UTF8x0}},
{0xE0,0xEF,mb_cm_wcv_UTF8x1,NULL,NULL,{mb_cm_nodev_UTF8x1,1U,mb_cm_dectab_UTF8x1,mb_cm_dectab_bv_UTF8x1}},
{0xF0,0xF7,mb_cm_wcv_UTF8x2,NULL,NULL,{mb_cm_nodev_UTF8x2,1U,mb_cm_dectab_UTF8x2,mb_cm_dectab_bv_UTF8x2}},
};
mb_char_map_t mb_cm_UTF8 = {
mb_cm_nodev_UTF8,
3U,
mb_cm_dectab_UTF8,
mb_cm_dectab_bv_UTF8,
};
mb_wchar_range_t mb_wcv_UTF8[] = {
{0x0,0x7FF,2048U},
{0x0,0xFFFF,65536U},
{0x0,0x1FFFFF,2097152U},
};
mb_wchar_range_t mb_cm_wcv_UTF16x0[] = {
{0x0,0xFF,1U},
};
mb_wchar_range_t mb_cm_dectab_UTF16[] = {
{0x0,0xFF,0U},
};
size_t mb_cm_dectab_bv_UTF16[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF16[] = {
{0x0,0xFF,mb_cm_wcv_UTF16x0,mb_utf16_encoder,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_UTF16 = {
mb_cm_nodev_UTF16,
1U,
mb_cm_dectab_UTF16,
mb_cm_dectab_bv_UTF16,
};
mb_wchar_range_t mb_wcv_UTF16[] = {
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_wcv_UTF16BEx0[] = {
{0x0,0xFF,1U},
};
mb_wchar_range_t mb_cm_dectab_UTF16BE[] = {
{0x0,0xFF,0U},
};
size_t mb_cm_dectab_bv_UTF16BE[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF16BE[] = {
{0x0,0xFF,mb_cm_wcv_UTF16BEx0,mb_utf16_encoder,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_UTF16BE = {
mb_cm_nodev_UTF16BE,
1U,
mb_cm_dectab_UTF16BE,
mb_cm_dectab_bv_UTF16BE,
};
mb_wchar_range_t mb_wcv_UTF16BE[] = {
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_wcv_UTF16LEx0[] = {
{0x0,0xFF,1U},
};
mb_wchar_range_t mb_cm_dectab_UTF16LE[] = {
{0x0,0xFF,0U},
};
size_t mb_cm_dectab_bv_UTF16LE[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_UTF16LE[] = {
{0x0,0xFF,mb_cm_wcv_UTF16LEx0,mb_utf16le_encoder,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_UTF16LE = {
mb_cm_nodev_UTF16LE,
1U,
mb_cm_dectab_UTF16LE,
mb_cm_dectab_bv_UTF16LE,
};
mb_wchar_range_t mb_wcv_UTF16LE[] = {
{0x0,0xFF,256U},
};
mb_wchar_range_t mb_cm_wcv_MOEINTERNALx0x0x0x0[] = {
{0x0,0x3F,1U},
};
mb_wchar_range_t mb_cm_wcv_MOEINTERNALx0x0x0[] = {
{0x0,0xFFF,64U},
};
mb_wchar_range_t mb_cm_dectab_MOEINTERNALx0x0x0[] = {
{0x0,0x3F,0U},
};
size_t mb_cm_dectab_bv_MOEINTERNALx0x0x0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_MOEINTERNALx0x0x0[] = {
{0x80,0xBF,mb_cm_wcv_MOEINTERNALx0x0x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_MOEINTERNALx0x0[] = {
{0x0,0x3FFFF,4096U},
};
mb_wchar_range_t mb_cm_dectab_MOEINTERNALx0x0[] = {
{0x0,0xFFF,0U},
};
size_t mb_cm_dectab_bv_MOEINTERNALx0x0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_MOEINTERNALx0x0[] = {
{0x80,0xBF,mb_cm_wcv_MOEINTERNALx0x0x0,NULL,NULL,{mb_cm_nodev_MOEINTERNALx0x0x0,1U,mb_cm_dectab_MOEINTERNALx0x0x0,mb_cm_dectab_bv_MOEINTERNALx0x0x0}},
};
mb_wchar_range_t mb_cm_wcv_MOEINTERNALx0[] = {
{0x0,0xFFFFFF,262144U},
};
mb_wchar_range_t mb_cm_dectab_MOEINTERNALx0[] = {
{0x0,0x3FFFF,0U},
};
size_t mb_cm_dectab_bv_MOEINTERNALx0[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_MOEINTERNALx0[] = {
{0x80,0xBF,mb_cm_wcv_MOEINTERNALx0x0,NULL,NULL,{mb_cm_nodev_MOEINTERNALx0x0,1U,mb_cm_dectab_MOEINTERNALx0x0,mb_cm_dectab_bv_MOEINTERNALx0x0}},
};
mb_wchar_range_t mb_cm_dectab_MOEINTERNAL[] = {
{0x0,0xFFFFFF,0U},
};
size_t mb_cm_dectab_bv_MOEINTERNAL[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_MOEINTERNAL[] = {
{0xC0,0xFF,mb_cm_wcv_MOEINTERNALx0,NULL,NULL,{mb_cm_nodev_MOEINTERNALx0,1U,mb_cm_dectab_MOEINTERNALx0,mb_cm_dectab_bv_MOEINTERNALx0}},
};
mb_char_map_t mb_cm_MOEINTERNAL = {
mb_cm_nodev_MOEINTERNAL,
1U,
mb_cm_dectab_MOEINTERNAL,
mb_cm_dectab_bv_MOEINTERNAL,
};
mb_wchar_range_t mb_wcv_MOEINTERNAL[] = {
{0x0,0xFFFFFF,16777216U},
};
mb_encoder_t mb_encmap_nodev_ISO2022[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_GR,&mb_cm_nodev_GR[0U]},
{mb_wcv_GR,&mb_cm_nodev_GR[1U]}
};
short mb_encmap_iv_ISO2022[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,5,5,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6
};
mb_encoder_map_t mb_encmap_ISO2022 = {
mb_encmap_nodev_ISO2022,
mb_encmap_iv_ISO2022,
};
mb_encoder_t mb_encmap_nodev_UTF8[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_UTF8,&mb_cm_nodev_UTF8[0U]},
{mb_wcv_UTF8,&mb_cm_nodev_UTF8[1U]},
{mb_wcv_UTF8,&mb_cm_nodev_UTF8[2U]}
};
short mb_encmap_iv_UTF8[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
7,7,7,7,7,7,7,7,-1,-1,-1,-1,-1,-1,-1,-1
};
mb_encoder_map_t mb_encmap_UTF8 = {
mb_encmap_nodev_UTF8,
mb_encmap_iv_UTF8,
};
mb_encoder_t mb_encmap_nodev_UTF16[] = {
{mb_wcv_UTF16,&mb_cm_nodev_UTF16[0U]}
};
short mb_encmap_iv_UTF16[] = {
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};
mb_encoder_map_t mb_encmap_UTF16 = {
mb_encmap_nodev_UTF16,
mb_encmap_iv_UTF16,
};
mb_encoder_t mb_encmap_nodev_UTF16BE[] = {
{mb_wcv_UTF16BE,&mb_cm_nodev_UTF16BE[0U]}
};
short mb_encmap_iv_UTF16BE[] = {
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};
mb_encoder_map_t mb_encmap_UTF16BE = {
mb_encmap_nodev_UTF16BE,
mb_encmap_iv_UTF16BE,
};
mb_encoder_t mb_encmap_nodev_UTF16LE[] = {
{mb_wcv_UTF16LE,&mb_cm_nodev_UTF16LE[0U]}
};
short mb_encmap_iv_UTF16LE[] = {
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};
mb_encoder_map_t mb_encmap_UTF16LE = {
mb_encmap_nodev_UTF16LE,
mb_encmap_iv_UTF16LE,
};
mb_encoder_t mb_encmap_nodev_MOEINTERNAL[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_MOEINTERNAL,&mb_cm_nodev_MOEINTERNAL[0U]}
};
short mb_encmap_iv_MOEINTERNAL[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_MOEINTERNAL = {
mb_encmap_nodev_MOEINTERNAL,
mb_encmap_iv_MOEINTERNAL,
};
mb_wchar_range_t mb_decmap_tab_ASCII[] = {
{0x0,0x7F,0U},
};
mb_decoder_t mb_decmap_destv_ASCII[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
};
mb_decoder_map_t mb_decmap_ASCII = {
mb_decmap_tab_ASCII,
1U,
mb_decmap_destv_ASCII,
};
static const char *mb_namev_ASCII[] = {
"us-ascii",
"ascii",
};
mb_ces_t mb_ces_ASCII = {
mb_namev_ASCII,
mb_flag_plus,0,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
NULL,
&mb_decmap_ASCII,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_GB2312[] = {
{0x0,0x7F,0U},
{0x216084,0x218307,1U},
};
mb_decoder_t mb_decmap_destv_GB2312[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94x94R_decoder,&mb_G1SR,0x213E00,&mb_cm_94x94R,0U},
};
mb_decoder_map_t mb_decmap_GB2312 = {
mb_decmap_tab_GB2312,
2U,
mb_decmap_destv_GB2312,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_GB2312(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_GB2312);
}
size_t
mb_conv_ws_to_GB2312(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_GB2312);
}
#endif
static const char *mb_namev_GB2312[] = {
"gb2312",
"cn-gb",
};
static mb_conv_t mb_convv_GB2312[] = {
#ifdef USE_UCS
mb_conv_to_GB2312,
#else
#endif
NULL,
};
mb_ces_t mb_ces_GB2312 = {
mb_namev_GB2312,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_94x94,mb_nSETs,mb_nSETs},{0x42,0x41,0x00,0x00}},
mb_convv_GB2312,
&mb_decmap_GB2312,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_GB_ISO_IR_165[] = {
{0x0,0x7F,0U},
{0x21EA94,0x220D17,1U},
};
mb_decoder_t mb_decmap_destv_GB_ISO_IR_165[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94x94R_decoder,&mb_G1SR,0x213E00,&mb_cm_94x94R,0U},
};
mb_decoder_map_t mb_decmap_GB_ISO_IR_165 = {
mb_decmap_tab_GB_ISO_IR_165,
2U,
mb_decmap_destv_GB_ISO_IR_165,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_GB_ISO_IR_165(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_GB_ISO_IR_165);
}
size_t
mb_conv_ws_to_GB_ISO_IR_165(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_GB_ISO_IR_165);
}
#endif
static const char *mb_namev_GB_ISO_IR_165[] = {
"cn-gb-isoir165",
};
static mb_conv_t mb_convv_GB_ISO_IR_165[] = {
#ifdef USE_UCS
mb_conv_to_GB_ISO_IR_165,
#else
#endif
NULL,
};
mb_ces_t mb_ces_GB_ISO_IR_165 = {
mb_namev_GB_ISO_IR_165,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_94x94,mb_nSETs,mb_nSETs},{0x42,0x45,0x00,0x00}},
mb_convv_GB_ISO_IR_165,
&mb_decmap_GB_ISO_IR_165,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_CN[] = {
{0x0,0x7F,0U},
{0x216084,0x218307,1U},
{0x222F9C,0x22521F,2U},
{0x225220,0x2274A3,3U},
};
mb_decoder_t mb_decmap_destv_CN[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94x94L_decoder,&mb_G1SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G1SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G2SSL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_CN = {
mb_decmap_tab_CN,
4U,
mb_decmap_destv_CN,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_CN(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_CN);
}
size_t
mb_conv_ws_to_CN(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_CN);
}
#endif
static const char *mb_namev_CN[] = {
"iso-2022-cn",
};
static mb_conv_t mb_convv_CN[] = {
#ifdef USE_UCS
mb_conv_to_CN,
#else
#endif
NULL,
};
mb_ces_t mb_ces_CN = {
mb_namev_CN,
mb_flag_plus,0,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_CN,
&mb_decmap_CN,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_CN_EXT[] = {
{0x0,0x7F,0U},
{0x216084,0x218307,1U},
{0x21EA94,0x220D17,2U},
{0x222F9C,0x22521F,3U},
{0x225220,0x2274A3,4U},
{0x2274A4,0x229727,5U},
{0x229728,0x22B9AB,6U},
{0x22B9AC,0x22DC2F,7U},
{0x22DC30,0x22FEB3,8U},
{0x22FEB4,0x232137,9U},
};
mb_decoder_t mb_decmap_destv_CN_EXT[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94x94L_decoder,&mb_G1SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G1SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G1SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G2SSL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G3SSL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G3SSL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G3SSL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G3SSL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G3SSL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_CN_EXT = {
mb_decmap_tab_CN_EXT,
10U,
mb_decmap_destv_CN_EXT,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_CN_EXT(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_CN_EXT);
}
size_t
mb_conv_ws_to_CN_EXT(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_CN_EXT);
}
#endif
static const char *mb_namev_CN_EXT[] = {
"iso-2022-cn-ext",
};
static mb_conv_t mb_convv_CN_EXT[] = {
#ifdef USE_UCS
mb_conv_to_CN_EXT,
#else
#endif
NULL,
};
mb_ces_t mb_ces_CN_EXT = {
mb_namev_CN_EXT,
mb_flag_plus,0,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_CN_EXT,
&mb_decmap_CN_EXT,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_EUC_JP[] = {
{0x0,0x7F,0U},
{0x20034E,0x2003AB,1U},
{0x218308,0x21A58B,2U},
{0x21C810,0x21EA93,3U},
};
mb_decoder_t mb_decmap_destv_EUC_JP[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94R_decoder,&mb_G2SSR,0x200000,&mb_cm_94R,0U},
{mb_94x94R_decoder,&mb_G1SR,0x213E00,&mb_cm_94x94R,0U},
{mb_94x94R_decoder,&mb_G3SSR,0x213E00,&mb_cm_94x94R,0U},
};
mb_decoder_map_t mb_decmap_EUC_JP = {
mb_decmap_tab_EUC_JP,
4U,
mb_decmap_destv_EUC_JP,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_EUC_JP(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_EUC_JP);
}
size_t
mb_conv_ws_to_EUC_JP(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_EUC_JP);
}
#endif
static const char *mb_namev_EUC_JP[] = {
"euc-jp",
"x-euc-jp",
"ujis",
"jis8",
};
static mb_conv_t mb_convv_EUC_JP[] = {
#ifdef USE_UCS
mb_conv_to_EUC_JP,
#else
#endif
NULL,
};
mb_ces_t mb_ces_EUC_JP = {
mb_namev_EUC_JP,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_94x94,mb_94,mb_94x94},{0x42,0x42,0x49,0x44}},
mb_convv_EUC_JP,
&mb_decmap_EUC_JP,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_EUC_JISX0213[] = {
{0x0,0x7F,0U},
{0x20034E,0x2003AB,1U},
{0x2343BC,0x23663F,2U},
{0x236640,0x2388C3,3U},
};
mb_decoder_t mb_decmap_destv_EUC_JISX0213[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94R_decoder,&mb_G2SSR,0x200000,&mb_cm_94R,0U},
{mb_94x94R_decoder,&mb_G1SR,0x213E00,&mb_cm_94x94R,0U},
{mb_94x94R_decoder,&mb_G3SSR,0x213E00,&mb_cm_94x94R,0U},
};
mb_decoder_map_t mb_decmap_EUC_JISX0213 = {
mb_decmap_tab_EUC_JISX0213,
4U,
mb_decmap_destv_EUC_JISX0213,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_EUC_JISX0213(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_EUC_JISX0213);
}
size_t
mb_conv_ws_to_EUC_JISX0213(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_EUC_JISX0213);
}
#endif
static const char *mb_namev_EUC_JISX0213[] = {
"euc-jisx0213",
"x-euc-jisx0213",
};
static mb_conv_t mb_convv_EUC_JISX0213[] = {
#ifdef USE_UCS
mb_conv_to_EUC_JISX0213,
#else
#endif
NULL,
};
mb_ces_t mb_ces_EUC_JISX0213 = {
mb_namev_EUC_JISX0213,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_94x94,mb_94,mb_94x94},{0x42,0x4F,0x49,0x50}},
mb_convv_EUC_JISX0213,
&mb_decmap_EUC_JISX0213,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO2022JP[] = {
{0x0,0x7F,0U},
{0x2003AC,0x200409,1U},
{0x213E00,0x216083,2U},
{0x218308,0x21A58B,3U},
};
mb_decoder_t mb_decmap_destv_ISO2022JP[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94L_decoder,&mb_G0SL,0x200000,&mb_cm_94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_ISO2022JP = {
mb_decmap_tab_ISO2022JP,
4U,
mb_decmap_destv_ISO2022JP,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO2022JP(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO2022JP);
}
size_t
mb_conv_ws_to_ISO2022JP(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO2022JP);
}
#endif
static const char *mb_namev_ISO2022JP[] = {
"iso-2022-jp",
"jis",
"jis7",
};
static mb_conv_t mb_convv_ISO2022JP[] = {
#ifdef USE_UCS
mb_conv_to_ISO2022JP,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO2022JP = {
mb_namev_ISO2022JP,
mb_flag_plus,MB_FLAG_ASCIIATCTL,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_ISO2022JP,
&mb_decmap_ISO2022JP,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO2022JP2[] = {
{0x0,0x7F,0U},
{0x2003AC,0x200409,1U},
{0x213E00,0x216083,2U},
{0x218308,0x21A58B,3U},
{0x21C810,0x21EA93,4U},
};
mb_decoder_t mb_decmap_destv_ISO2022JP2[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94L_decoder,&mb_G0SL,0x200000,&mb_cm_94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_ISO2022JP2 = {
mb_decmap_tab_ISO2022JP2,
5U,
mb_decmap_destv_ISO2022JP2,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO2022JP2(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO2022JP2);
}
size_t
mb_conv_ws_to_ISO2022JP2(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO2022JP2);
}
#endif
static const char *mb_namev_ISO2022JP2[] = {
"iso-2022-jp-2",
};
static mb_conv_t mb_convv_ISO2022JP2[] = {
#ifdef USE_UCS
mb_conv_to_ISO2022JP2,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO2022JP2 = {
mb_namev_ISO2022JP2,
mb_flag_plus,MB_FLAG_ASCIIATCTL,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_ISO2022JP2,
&mb_decmap_ISO2022JP2,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO2022JP3[] = {
{0x0,0x7F,0U},
{0x2003AC,0x200409,1U},
{0x21C810,0x21EA93,2U},
{0x2343BC,0x23663F,3U},
{0x236640,0x2388C3,4U},
};
mb_decoder_t mb_decmap_destv_ISO2022JP3[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94L_decoder,&mb_G0SL,0x200000,&mb_cm_94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_ISO2022JP3 = {
mb_decmap_tab_ISO2022JP3,
5U,
mb_decmap_destv_ISO2022JP3,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO2022JP3(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO2022JP3);
}
size_t
mb_conv_ws_to_ISO2022JP3(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO2022JP3);
}
#endif
static const char *mb_namev_ISO2022JP3[] = {
"iso-2022-jp-3",
};
static mb_conv_t mb_convv_ISO2022JP3[] = {
#ifdef USE_UCS
mb_conv_to_ISO2022JP3,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO2022JP3 = {
mb_namev_ISO2022JP3,
mb_flag_plus,MB_FLAG_ASCIIATCTL,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_ISO2022JP3,
&mb_decmap_ISO2022JP3,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_EUC_KR[] = {
{0x0,0x7F,0U},
{0x21A58C,0x21C80F,1U},
};
mb_decoder_t mb_decmap_destv_EUC_KR[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94x94R_decoder,&mb_G1SR,0x213E00,&mb_cm_94x94R,0U},
};
mb_decoder_map_t mb_decmap_EUC_KR = {
mb_decmap_tab_EUC_KR,
2U,
mb_decmap_destv_EUC_KR,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_EUC_KR(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_EUC_KR);
}
size_t
mb_conv_ws_to_EUC_KR(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_EUC_KR);
}
#endif
static const char *mb_namev_EUC_KR[] = {
"euc-kr",
"x-euc-kr",
};
static mb_conv_t mb_convv_EUC_KR[] = {
#ifdef USE_UCS
mb_conv_to_EUC_KR,
#else
#endif
NULL,
};
mb_ces_t mb_ces_EUC_KR = {
mb_namev_EUC_KR,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_94x94,mb_nSETs,mb_nSETs},{0x42,0x43,0x00,0x00}},
mb_convv_EUC_KR,
&mb_decmap_EUC_KR,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO2022KR[] = {
{0x0,0x7F,0U},
{0x21A58C,0x21C80F,1U},
};
mb_decoder_t mb_decmap_destv_ISO2022KR[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_ISO2022KR = {
mb_decmap_tab_ISO2022KR,
2U,
mb_decmap_destv_ISO2022KR,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO2022KR(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO2022KR);
}
size_t
mb_conv_ws_to_ISO2022KR(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO2022KR);
}
#endif
static const char *mb_namev_ISO2022KR[] = {
"iso-2022-kr",
};
static mb_conv_t mb_convv_ISO2022KR[] = {
#ifdef USE_UCS
mb_conv_to_ISO2022KR,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO2022KR = {
mb_namev_ISO2022KR,
mb_flag_plus,0,
{mb_G0,mb_Gn,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_ISO2022KR,
&mb_decmap_ISO2022KR,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_1[] = {
{0x0,0x7F,0U},
{0x205E60,0x205EBF,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_1[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_1 = {
mb_decmap_tab_ISO8859_1,
2U,
mb_decmap_destv_ISO8859_1,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_1(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_1);
}
size_t
mb_conv_ws_to_ISO8859_1(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_1);
}
#endif
static const char *mb_namev_ISO8859_1[] = {
"iso-8859-1",
};
static mb_conv_t mb_convv_ISO8859_1[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_1,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_1 = {
mb_namev_ISO8859_1,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x41,0x00,0x00}},
mb_convv_ISO8859_1,
&mb_decmap_ISO8859_1,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_2[] = {
{0x0,0x7F,0U},
{0x205EC0,0x205F1F,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_2[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_2 = {
mb_decmap_tab_ISO8859_2,
2U,
mb_decmap_destv_ISO8859_2,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_2(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_2);
}
size_t
mb_conv_ws_to_ISO8859_2(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_2);
}
#endif
static const char *mb_namev_ISO8859_2[] = {
"iso-8859-2",
};
static mb_conv_t mb_convv_ISO8859_2[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_2,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_2 = {
mb_namev_ISO8859_2,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x42,0x00,0x00}},
mb_convv_ISO8859_2,
&mb_decmap_ISO8859_2,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_3[] = {
{0x0,0x7F,0U},
{0x205F20,0x205F7F,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_3[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_3 = {
mb_decmap_tab_ISO8859_3,
2U,
mb_decmap_destv_ISO8859_3,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_3(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_3);
}
size_t
mb_conv_ws_to_ISO8859_3(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_3);
}
#endif
static const char *mb_namev_ISO8859_3[] = {
"iso-8859-3",
};
static mb_conv_t mb_convv_ISO8859_3[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_3,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_3 = {
mb_namev_ISO8859_3,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x43,0x00,0x00}},
mb_convv_ISO8859_3,
&mb_decmap_ISO8859_3,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_4[] = {
{0x0,0x7F,0U},
{0x205F80,0x205FDF,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_4[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_4 = {
mb_decmap_tab_ISO8859_4,
2U,
mb_decmap_destv_ISO8859_4,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_4(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_4);
}
size_t
mb_conv_ws_to_ISO8859_4(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_4);
}
#endif
static const char *mb_namev_ISO8859_4[] = {
"iso-8859-4",
};
static mb_conv_t mb_convv_ISO8859_4[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_4,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_4 = {
mb_namev_ISO8859_4,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x44,0x00,0x00}},
mb_convv_ISO8859_4,
&mb_decmap_ISO8859_4,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_5[] = {
{0x0,0x7F,0U},
{0x206280,0x2062DF,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_5[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_5 = {
mb_decmap_tab_ISO8859_5,
2U,
mb_decmap_destv_ISO8859_5,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_5(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_5);
}
size_t
mb_conv_ws_to_ISO8859_5(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_5);
}
#endif
static const char *mb_namev_ISO8859_5[] = {
"iso-8859-5",
};
static mb_conv_t mb_convv_ISO8859_5[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_5,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_5 = {
mb_namev_ISO8859_5,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x4C,0x00,0x00}},
mb_convv_ISO8859_5,
&mb_decmap_ISO8859_5,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_6[] = {
{0x0,0x7F,0U},
{0x2060A0,0x2060FF,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_6[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_6 = {
mb_decmap_tab_ISO8859_6,
2U,
mb_decmap_destv_ISO8859_6,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_6(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_6);
}
size_t
mb_conv_ws_to_ISO8859_6(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_6);
}
#endif
static const char *mb_namev_ISO8859_6[] = {
"iso-8859-6",
};
static mb_conv_t mb_convv_ISO8859_6[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_6,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_6 = {
mb_namev_ISO8859_6,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x47,0x00,0x00}},
mb_convv_ISO8859_6,
&mb_decmap_ISO8859_6,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_7[] = {
{0x0,0x7F,0U},
{0x206040,0x20609F,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_7[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_7 = {
mb_decmap_tab_ISO8859_7,
2U,
mb_decmap_destv_ISO8859_7,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_7(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_7);
}
size_t
mb_conv_ws_to_ISO8859_7(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_7);
}
#endif
static const char *mb_namev_ISO8859_7[] = {
"iso-8859-7",
};
static mb_conv_t mb_convv_ISO8859_7[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_7,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_7 = {
mb_namev_ISO8859_7,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x46,0x00,0x00}},
mb_convv_ISO8859_7,
&mb_decmap_ISO8859_7,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_8[] = {
{0x0,0x7F,0U},
{0x206100,0x20615F,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_8[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_8 = {
mb_decmap_tab_ISO8859_8,
2U,
mb_decmap_destv_ISO8859_8,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_8(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_8);
}
size_t
mb_conv_ws_to_ISO8859_8(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_8);
}
#endif
static const char *mb_namev_ISO8859_8[] = {
"iso-8859-8",
};
static mb_conv_t mb_convv_ISO8859_8[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_8,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_8 = {
mb_namev_ISO8859_8,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x48,0x00,0x00}},
mb_convv_ISO8859_8,
&mb_decmap_ISO8859_8,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_9[] = {
{0x0,0x7F,0U},
{0x2062E0,0x20633F,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_9[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_9 = {
mb_decmap_tab_ISO8859_9,
2U,
mb_decmap_destv_ISO8859_9,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_9(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_9);
}
size_t
mb_conv_ws_to_ISO8859_9(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_9);
}
#endif
static const char *mb_namev_ISO8859_9[] = {
"iso-8859-9",
};
static mb_conv_t mb_convv_ISO8859_9[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_9,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_9 = {
mb_namev_ISO8859_9,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x4D,0x00,0x00}},
mb_convv_ISO8859_9,
&mb_decmap_ISO8859_9,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_ISO8859_10[] = {
{0x0,0x7F,0U},
{0x206640,0x20669F,1U},
};
mb_decoder_t mb_decmap_destv_ISO8859_10[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_ISO8859_10 = {
mb_decmap_tab_ISO8859_10,
2U,
mb_decmap_destv_ISO8859_10,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_ISO8859_10(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_ISO8859_10);
}
size_t
mb_conv_ws_to_ISO8859_10(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_ISO8859_10);
}
#endif
static const char *mb_namev_ISO8859_10[] = {
"iso-8859-10",
};
static mb_conv_t mb_convv_ISO8859_10[] = {
#ifdef USE_UCS
mb_conv_to_ISO8859_10,
#else
#endif
NULL,
};
mb_ces_t mb_ces_ISO8859_10 = {
mb_namev_ISO8859_10,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x56,0x00,0x00}},
mb_convv_ISO8859_10,
&mb_decmap_ISO8859_10,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_XCTEXT[] = {
{0x0,0x7F,0U},
{0x20034E,0x2003AB,1U},
{0x2003AC,0x200409,2U},
{0x205E60,0x205EBF,3U},
{0x205EC0,0x205F1F,4U},
{0x205F20,0x205F7F,5U},
{0x205F80,0x205FDF,6U},
{0x206040,0x20609F,7U},
{0x2060A0,0x2060FF,8U},
{0x206100,0x20615F,9U},
{0x206280,0x2062DF,10U},
{0x2062E0,0x20633F,11U},
{0x206640,0x20669F,12U},
{0x213E00,0x216083,13U},
{0x216084,0x218307,14U},
{0x218308,0x21A58B,15U},
{0x21A58C,0x21C80F,16U},
{0x21C810,0x21EA93,17U},
};
mb_decoder_t mb_decmap_destv_XCTEXT[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_94R_decoder,&mb_G1SR,0x200000,&mb_cm_94R,0U},
{mb_94R_decoder,&mb_G1SR,0x200000,&mb_cm_94R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
{mb_94x94L_decoder,&mb_G0SL,0x213E00,&mb_cm_94x94L,0U},
};
mb_decoder_map_t mb_decmap_XCTEXT = {
mb_decmap_tab_XCTEXT,
18U,
mb_decmap_destv_XCTEXT,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_XCTEXT(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_XCTEXT);
}
size_t
mb_conv_ws_to_XCTEXT(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_XCTEXT);
}
#endif
static const char *mb_namev_XCTEXT[] = {
"x-ctext",
};
static mb_conv_t mb_convv_XCTEXT[] = {
#ifdef USE_UCS
mb_conv_to_XCTEXT,
#else
#endif
NULL,
};
mb_ces_t mb_ces_XCTEXT = {
mb_namev_XCTEXT,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x41,0x00,0x00}},
mb_convv_XCTEXT,
&mb_decmap_XCTEXT,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_UTF8[] = {
{0x0,0x7F,0U},
{0x80,0x7FF,1U},
{0x800,0xFFFF,2U},
{0x10000,0x1FFFFF,3U},
};
mb_decoder_t mb_decmap_destv_UTF8[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x0,&mb_cm_UTF8,0U},
{NULL,NULL,0x0,&mb_cm_UTF8,1U},
{NULL,NULL,0x0,&mb_cm_UTF8,2U},
};
mb_decoder_map_t mb_decmap_UTF8 = {
mb_decmap_tab_UTF8,
4U,
mb_decmap_destv_UTF8,
};
static const char *mb_namev_UTF8[] = {
"utf-8",
};
static mb_conv_t mb_convv_UTF8[] = {
#ifdef USE_UCS
mb_conv1_to_ucs,
#else
mb_conv1_to_ucs,
#endif
NULL,
};
mb_ces_t mb_ces_UTF8 = {
mb_namev_UTF8,
mb_flag_plus,0,
{mb_G0,mb_UTF8,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_UTF8,
&mb_decmap_UTF8,
NULL,
1,
};
mb_wchar_range_t mb_decmap_tab_UTF16[] = {
{0x0,0x10FFFF,0U},
};
mb_decoder_t mb_decmap_destv_UTF16[] = {
{mb_utf16_decoder,NULL,0x0,NULL,0U},
};
mb_decoder_map_t mb_decmap_UTF16 = {
mb_decmap_tab_UTF16,
1U,
mb_decmap_destv_UTF16,
};
static const char *mb_namev_UTF16[] = {
"utf-16",
};
static mb_conv_t mb_convv_UTF16[] = {
#ifdef USE_UCS
mb_conv1_to_ucs,
#else
mb_conv1_to_ucs,
#endif
NULL,
};
mb_ces_t mb_ces_UTF16 = {
mb_namev_UTF16,
mb_flag_plus,0,
{mb_GN,mb_UTF16,{mb_nSETs,mb_nSETs,mb_nSETs,mb_nSETs},{0x00,0x00,0x00,0x00}},
mb_convv_UTF16,
&mb_decmap_UTF16,
NULL,
2,
};
mb_wchar_range_t mb_decmap_tab_UTF16BE[] = {
{0x0,0x10FFFF,0U},
};
mb_decoder_t mb_decmap_destv_UTF16BE[] = {
{mb_utf16_decoder,NULL,0x0,NULL,0U},
};
mb_decoder_map_t mb_decmap_UTF16BE = {
mb_decmap_tab_UTF16BE,
1U,
mb_decmap_destv_UTF16BE,
};
static const char *mb_namev_UTF16BE[] = {
"utf-16be",
};
static mb_conv_t mb_convv_UTF16BE[] = {
#ifdef USE_UCS
mb_conv1_to_ucs,
#else
mb_conv1_to_ucs,
#endif
NULL,
};
mb_ces_t mb_ces_UTF16BE = {
mb_namev_UTF16BE,
mb_flag_plus,0,
{mb_UTF16BE,mb_UTF16BE,{mb_nSETs,mb_nSETs,mb_nSETs,mb_nSETs},{0x00,0x00,0x00,0x00}},
mb_convv_UTF16BE,
&mb_decmap_UTF16BE,
NULL,
2,
};
mb_wchar_range_t mb_decmap_tab_UTF16LE[] = {
{0x0,0x10FFFF,0U},
};
mb_decoder_t mb_decmap_destv_UTF16LE[] = {
{mb_utf16le_decoder,NULL,0x0,NULL,0U},
};
mb_decoder_map_t mb_decmap_UTF16LE = {
mb_decmap_tab_UTF16LE,
1U,
mb_decmap_destv_UTF16LE,
};
static const char *mb_namev_UTF16LE[] = {
"utf-16le",
};
static mb_conv_t mb_convv_UTF16LE[] = {
#ifdef USE_UCS
mb_conv1_to_ucs,
#else
mb_conv1_to_ucs,
#endif
NULL,
};
mb_ces_t mb_ces_UTF16LE = {
mb_namev_UTF16LE,
mb_flag_plus,0,
{mb_UTF16LE,mb_UTF16LE,{mb_nSETs,mb_nSETs,mb_nSETs,mb_nSETs},{0x00,0x00,0x00,0x00}},
mb_convv_UTF16LE,
&mb_decmap_UTF16LE,
NULL,
2,
};
mb_wchar_range_t mb_decmap_tab_MOEINTERNAL[] = {
{0x0,0x7F,0U},
{0x80,0xFFFFFF,1U},
};
mb_decoder_t mb_decmap_destv_MOEINTERNAL[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x0,&mb_cm_MOEINTERNAL,0U},
};
mb_decoder_map_t mb_decmap_MOEINTERNAL = {
mb_decmap_tab_MOEINTERNAL,
2U,
mb_decmap_destv_MOEINTERNAL,
};
static const char *mb_namev_MOEINTERNAL[] = {
"x-moe-internal",
};
mb_ces_t mb_ces_MOEINTERNAL = {
mb_namev_MOEINTERNAL,
mb_flag_plus,0,
{mb_G0,mb_MOEINTERNAL,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
NULL,
&mb_decmap_MOEINTERNAL,
NULL,
1,
};
#ifdef USE_KOI8R
mb_wchar_range_t mb_cm_wcv_KOI8Rx0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_KOI8R[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_KOI8R[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_KOI8R[] = {
{0x80,0xFF,mb_cm_wcv_KOI8Rx0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_KOI8R = {
mb_cm_nodev_KOI8R,
1U,
mb_cm_dectab_KOI8R,
mb_cm_dectab_bv_KOI8R,
};
mb_wchar_range_t mb_wcv_KOI8R[] = {
{0x20BF00,0x20BF7F,128U},
};
mb_wchar_range_t mb_decmap_tab_KOI8R[] = {
{0x0,0x7F,0U},
{0x20BF00,0x20BF7F,1U},
};
mb_decoder_t mb_decmap_destv_KOI8R[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20BF00,&mb_cm_KOI8R,0U},
};
mb_decoder_map_t mb_decmap_KOI8R = {
mb_decmap_tab_KOI8R,
2U,
mb_decmap_destv_KOI8R,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_KOI8R(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_KOI8R);
}
size_t
mb_conv_ws_to_KOI8R(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_KOI8R);
}
#endif
static const char *mb_namev_KOI8R[] = {
"koi8-r",
};
static mb_conv_t mb_convv_KOI8R[] = {
#ifdef USE_UCS
mb_conv_to_KOI8R,
#else
#endif
NULL,
};
mb_ces_t mb_ces_KOI8R = {
mb_namev_KOI8R,
mb_flag_plus,0,
{mb_G0,mb_KOI8R,{2,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_KOI8R,
&mb_decmap_KOI8R,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_KOI8R[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_KOI8R,&mb_cm_nodev_KOI8R[0U]}
};
short mb_encmap_iv_KOI8R[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_KOI8R = {
mb_encmap_nodev_KOI8R,
mb_encmap_iv_KOI8R,
};
#endif
#ifdef USE_KOI8U
mb_wchar_range_t mb_cm_wcv_KOI8Ux0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_KOI8U[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_KOI8U[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_KOI8U[] = {
{0x80,0xFF,mb_cm_wcv_KOI8Ux0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_KOI8U = {
mb_cm_nodev_KOI8U,
1U,
mb_cm_dectab_KOI8U,
mb_cm_dectab_bv_KOI8U,
};
mb_wchar_range_t mb_wcv_KOI8U[] = {
{0x20BF80,0x20BFFF,128U},
};
mb_wchar_range_t mb_decmap_tab_KOI8U[] = {
{0x0,0x7F,0U},
{0x20BF80,0x20BFFF,1U},
};
mb_decoder_t mb_decmap_destv_KOI8U[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20BF80,&mb_cm_KOI8U,0U},
};
mb_decoder_map_t mb_decmap_KOI8U = {
mb_decmap_tab_KOI8U,
2U,
mb_decmap_destv_KOI8U,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_KOI8U(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_KOI8U);
}
size_t
mb_conv_ws_to_KOI8U(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_KOI8U);
}
#endif
static const char *mb_namev_KOI8U[] = {
"koi8-u",
};
static mb_conv_t mb_convv_KOI8U[] = {
#ifdef USE_UCS
mb_conv_to_KOI8U,
#else
#endif
NULL,
};
mb_ces_t mb_ces_KOI8U = {
mb_namev_KOI8U,
mb_flag_plus,0,
{mb_G0,mb_KOI8U,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_KOI8U,
&mb_decmap_KOI8U,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_KOI8U[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_KOI8U,&mb_cm_nodev_KOI8U[0U]}
};
short mb_encmap_iv_KOI8U[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_KOI8U = {
mb_encmap_nodev_KOI8U,
mb_encmap_iv_KOI8U,
};
#endif
#ifdef USE_WIN1250
mb_wchar_range_t mb_cm_wcv_WIN1250x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1250[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1250[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1250[] = {
{0x80,0xFF,mb_cm_wcv_WIN1250x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1250 = {
mb_cm_nodev_WIN1250,
1U,
mb_cm_dectab_WIN1250,
mb_cm_dectab_bv_WIN1250,
};
mb_wchar_range_t mb_wcv_WIN1250[] = {
{0x20C000,0x20C07F,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1250[] = {
{0x0,0x7F,0U},
{0x20C000,0x20C07F,1U},
};
mb_decoder_t mb_decmap_destv_WIN1250[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C000,&mb_cm_WIN1250,0U},
};
mb_decoder_map_t mb_decmap_WIN1250 = {
mb_decmap_tab_WIN1250,
2U,
mb_decmap_destv_WIN1250,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1250(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1250);
}
size_t
mb_conv_ws_to_WIN1250(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1250);
}
#endif
static const char *mb_namev_WIN1250[] = {
"windows-1250",
};
static mb_conv_t mb_convv_WIN1250[] = {
#ifdef USE_UCS
mb_conv_to_WIN1250,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1250 = {
mb_namev_WIN1250,
mb_flag_plus,0,
{mb_G0,mb_WIN1250,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1250,
&mb_decmap_WIN1250,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1250[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1250,&mb_cm_nodev_WIN1250[0U]}
};
short mb_encmap_iv_WIN1250[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1250 = {
mb_encmap_nodev_WIN1250,
mb_encmap_iv_WIN1250,
};
#endif
#ifdef USE_WIN1251
mb_wchar_range_t mb_cm_wcv_WIN1251x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1251[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1251[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1251[] = {
{0x80,0xFF,mb_cm_wcv_WIN1251x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1251 = {
mb_cm_nodev_WIN1251,
1U,
mb_cm_dectab_WIN1251,
mb_cm_dectab_bv_WIN1251,
};
mb_wchar_range_t mb_wcv_WIN1251[] = {
{0x20C080,0x20C0FF,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1251[] = {
{0x0,0x7F,0U},
{0x20C080,0x20C0FF,1U},
};
mb_decoder_t mb_decmap_destv_WIN1251[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C080,&mb_cm_WIN1251,0U},
};
mb_decoder_map_t mb_decmap_WIN1251 = {
mb_decmap_tab_WIN1251,
2U,
mb_decmap_destv_WIN1251,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1251(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1251);
}
size_t
mb_conv_ws_to_WIN1251(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1251);
}
#endif
static const char *mb_namev_WIN1251[] = {
"windows-1251",
};
static mb_conv_t mb_convv_WIN1251[] = {
#ifdef USE_UCS
mb_conv_to_WIN1251,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1251 = {
mb_namev_WIN1251,
mb_flag_plus,0,
{mb_G0,mb_WIN1251,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1251,
&mb_decmap_WIN1251,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1251[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1251,&mb_cm_nodev_WIN1251[0U]}
};
short mb_encmap_iv_WIN1251[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1251 = {
mb_encmap_nodev_WIN1251,
mb_encmap_iv_WIN1251,
};
#endif
#ifdef USE_WIN1252
mb_wchar_range_t mb_cm_wcv_WIN1252x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1252[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1252[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1252[] = {
{0x80,0xFF,mb_cm_wcv_WIN1252x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1252 = {
mb_cm_nodev_WIN1252,
1U,
mb_cm_dectab_WIN1252,
mb_cm_dectab_bv_WIN1252,
};
mb_wchar_range_t mb_wcv_WIN1252[] = {
{0x20C100,0x20C17F,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1252[] = {
{0x0,0x7F,0U},
{0x20C100,0x20C17F,1U},
};
mb_decoder_t mb_decmap_destv_WIN1252[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C100,&mb_cm_WIN1252,0U},
};
mb_decoder_map_t mb_decmap_WIN1252 = {
mb_decmap_tab_WIN1252,
2U,
mb_decmap_destv_WIN1252,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1252(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1252);
}
size_t
mb_conv_ws_to_WIN1252(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1252);
}
#endif
static const char *mb_namev_WIN1252[] = {
"windows-1252",
};
static mb_conv_t mb_convv_WIN1252[] = {
#ifdef USE_UCS
mb_conv_to_WIN1252,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1252 = {
mb_namev_WIN1252,
mb_flag_plus,0,
{mb_G0,mb_WIN1252,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1252,
&mb_decmap_WIN1252,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1252[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1252,&mb_cm_nodev_WIN1252[0U]}
};
short mb_encmap_iv_WIN1252[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1252 = {
mb_encmap_nodev_WIN1252,
mb_encmap_iv_WIN1252,
};
#endif
#ifdef USE_WIN1253
mb_wchar_range_t mb_cm_wcv_WIN1253x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1253[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1253[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1253[] = {
{0x80,0xFF,mb_cm_wcv_WIN1253x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1253 = {
mb_cm_nodev_WIN1253,
1U,
mb_cm_dectab_WIN1253,
mb_cm_dectab_bv_WIN1253,
};
mb_wchar_range_t mb_wcv_WIN1253[] = {
{0x20C180,0x20C1FF,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1253[] = {
{0x0,0x7F,0U},
{0x20C180,0x20C1FF,1U},
};
mb_decoder_t mb_decmap_destv_WIN1253[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C180,&mb_cm_WIN1253,0U},
};
mb_decoder_map_t mb_decmap_WIN1253 = {
mb_decmap_tab_WIN1253,
2U,
mb_decmap_destv_WIN1253,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1253(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1253);
}
size_t
mb_conv_ws_to_WIN1253(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1253);
}
#endif
static const char *mb_namev_WIN1253[] = {
"windows-1253",
};
static mb_conv_t mb_convv_WIN1253[] = {
#ifdef USE_UCS
mb_conv_to_WIN1253,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1253 = {
mb_namev_WIN1253,
mb_flag_plus,0,
{mb_G0,mb_WIN1253,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1253,
&mb_decmap_WIN1253,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1253[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1253,&mb_cm_nodev_WIN1253[0U]}
};
short mb_encmap_iv_WIN1253[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1253 = {
mb_encmap_nodev_WIN1253,
mb_encmap_iv_WIN1253,
};
#endif
#ifdef USE_WIN1254
mb_wchar_range_t mb_cm_wcv_WIN1254x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1254[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1254[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1254[] = {
{0x80,0xFF,mb_cm_wcv_WIN1254x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1254 = {
mb_cm_nodev_WIN1254,
1U,
mb_cm_dectab_WIN1254,
mb_cm_dectab_bv_WIN1254,
};
mb_wchar_range_t mb_wcv_WIN1254[] = {
{0x20C200,0x20C27F,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1254[] = {
{0x0,0x7F,0U},
{0x20C200,0x20C27F,1U},
};
mb_decoder_t mb_decmap_destv_WIN1254[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C200,&mb_cm_WIN1254,0U},
};
mb_decoder_map_t mb_decmap_WIN1254 = {
mb_decmap_tab_WIN1254,
2U,
mb_decmap_destv_WIN1254,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1254(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1254);
}
size_t
mb_conv_ws_to_WIN1254(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1254);
}
#endif
static const char *mb_namev_WIN1254[] = {
"windows-1254",
};
static mb_conv_t mb_convv_WIN1254[] = {
#ifdef USE_UCS
mb_conv_to_WIN1254,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1254 = {
mb_namev_WIN1254,
mb_flag_plus,0,
{mb_G0,mb_WIN1254,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1254,
&mb_decmap_WIN1254,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1254[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1254,&mb_cm_nodev_WIN1254[0U]}
};
short mb_encmap_iv_WIN1254[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1254 = {
mb_encmap_nodev_WIN1254,
mb_encmap_iv_WIN1254,
};
#endif
#ifdef USE_WIN1255
mb_wchar_range_t mb_cm_wcv_WIN1255x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1255[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1255[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1255[] = {
{0x80,0xFF,mb_cm_wcv_WIN1255x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1255 = {
mb_cm_nodev_WIN1255,
1U,
mb_cm_dectab_WIN1255,
mb_cm_dectab_bv_WIN1255,
};
mb_wchar_range_t mb_wcv_WIN1255[] = {
{0x20C280,0x20C2FF,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1255[] = {
{0x0,0x7F,0U},
{0x20C280,0x20C2FF,1U},
};
mb_decoder_t mb_decmap_destv_WIN1255[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C280,&mb_cm_WIN1255,0U},
};
mb_decoder_map_t mb_decmap_WIN1255 = {
mb_decmap_tab_WIN1255,
2U,
mb_decmap_destv_WIN1255,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1255(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1255);
}
size_t
mb_conv_ws_to_WIN1255(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1255);
}
#endif
static const char *mb_namev_WIN1255[] = {
"windows-1255",
};
static mb_conv_t mb_convv_WIN1255[] = {
#ifdef USE_UCS
mb_conv_to_WIN1255,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1255 = {
mb_namev_WIN1255,
mb_flag_plus,0,
{mb_G0,mb_WIN1255,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1255,
&mb_decmap_WIN1255,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1255[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1255,&mb_cm_nodev_WIN1255[0U]}
};
short mb_encmap_iv_WIN1255[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1255 = {
mb_encmap_nodev_WIN1255,
mb_encmap_iv_WIN1255,
};
#endif
#ifdef USE_WIN1256
mb_wchar_range_t mb_cm_wcv_WIN1256x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1256[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1256[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1256[] = {
{0x80,0xFF,mb_cm_wcv_WIN1256x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1256 = {
mb_cm_nodev_WIN1256,
1U,
mb_cm_dectab_WIN1256,
mb_cm_dectab_bv_WIN1256,
};
mb_wchar_range_t mb_wcv_WIN1256[] = {
{0x20C300,0x20C37F,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1256[] = {
{0x0,0x7F,0U},
{0x20C300,0x20C37F,1U},
};
mb_decoder_t mb_decmap_destv_WIN1256[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C300,&mb_cm_WIN1256,0U},
};
mb_decoder_map_t mb_decmap_WIN1256 = {
mb_decmap_tab_WIN1256,
2U,
mb_decmap_destv_WIN1256,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1256(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1256);
}
size_t
mb_conv_ws_to_WIN1256(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1256);
}
#endif
static const char *mb_namev_WIN1256[] = {
"windows-1256",
};
static mb_conv_t mb_convv_WIN1256[] = {
#ifdef USE_UCS
mb_conv_to_WIN1256,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1256 = {
mb_namev_WIN1256,
mb_flag_plus,0,
{mb_G0,mb_WIN1256,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1256,
&mb_decmap_WIN1256,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1256[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1256,&mb_cm_nodev_WIN1256[0U]}
};
short mb_encmap_iv_WIN1256[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1256 = {
mb_encmap_nodev_WIN1256,
mb_encmap_iv_WIN1256,
};
#endif
#ifdef USE_WIN1257
mb_wchar_range_t mb_cm_wcv_WIN1257x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1257[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1257[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1257[] = {
{0x80,0xFF,mb_cm_wcv_WIN1257x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1257 = {
mb_cm_nodev_WIN1257,
1U,
mb_cm_dectab_WIN1257,
mb_cm_dectab_bv_WIN1257,
};
mb_wchar_range_t mb_wcv_WIN1257[] = {
{0x20C380,0x20C3FF,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1257[] = {
{0x0,0x7F,0U},
{0x20C380,0x20C3FF,1U},
};
mb_decoder_t mb_decmap_destv_WIN1257[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C380,&mb_cm_WIN1257,0U},
};
mb_decoder_map_t mb_decmap_WIN1257 = {
mb_decmap_tab_WIN1257,
2U,
mb_decmap_destv_WIN1257,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1257(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1257);
}
size_t
mb_conv_ws_to_WIN1257(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1257);
}
#endif
static const char *mb_namev_WIN1257[] = {
"windows-1257",
};
static mb_conv_t mb_convv_WIN1257[] = {
#ifdef USE_UCS
mb_conv_to_WIN1257,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1257 = {
mb_namev_WIN1257,
mb_flag_plus,0,
{mb_G0,mb_WIN1257,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1257,
&mb_decmap_WIN1257,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1257[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1257,&mb_cm_nodev_WIN1257[0U]}
};
short mb_encmap_iv_WIN1257[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1257 = {
mb_encmap_nodev_WIN1257,
mb_encmap_iv_WIN1257,
};
#endif
#ifdef USE_WIN1258
mb_wchar_range_t mb_cm_wcv_WIN1258x0[] = {
{0x0,0x7F,1U},
};
mb_wchar_range_t mb_cm_dectab_WIN1258[] = {
{0x0,0x7F,0U},
};
size_t mb_cm_dectab_bv_WIN1258[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_WIN1258[] = {
{0x80,0xFF,mb_cm_wcv_WIN1258x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_char_map_t mb_cm_WIN1258 = {
mb_cm_nodev_WIN1258,
1U,
mb_cm_dectab_WIN1258,
mb_cm_dectab_bv_WIN1258,
};
mb_wchar_range_t mb_wcv_WIN1258[] = {
{0x20C400,0x20C47F,128U},
};
mb_wchar_range_t mb_decmap_tab_WIN1258[] = {
{0x0,0x7F,0U},
{0x20C400,0x20C47F,1U},
};
mb_decoder_t mb_decmap_destv_WIN1258[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20C400,&mb_cm_WIN1258,0U},
};
mb_decoder_map_t mb_decmap_WIN1258 = {
mb_decmap_tab_WIN1258,
2U,
mb_decmap_destv_WIN1258,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_WIN1258(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_WIN1258);
}
size_t
mb_conv_ws_to_WIN1258(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_WIN1258);
}
#endif
static const char *mb_namev_WIN1258[] = {
"windows-1258",
};
static mb_conv_t mb_convv_WIN1258[] = {
#ifdef USE_UCS
mb_conv_to_WIN1258,
#else
#endif
NULL,
};
mb_ces_t mb_ces_WIN1258 = {
mb_namev_WIN1258,
mb_flag_plus,0,
{mb_G0,mb_WIN1258,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_WIN1258,
&mb_decmap_WIN1258,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_WIN1258[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_WIN1258,&mb_cm_nodev_WIN1258[0U]}
};
short mb_encmap_iv_WIN1258[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5
};
mb_encoder_map_t mb_encmap_WIN1258 = {
mb_encmap_nodev_WIN1258,
mb_encmap_iv_WIN1258,
};
#endif
#ifdef USE_SJIS
mb_wchar_range_t mb_cm_wcv_SJISx0x0[] = {
{0,0,0},
{0x0,0x3E,1U},
};
mb_wchar_range_t mb_cm_wcv_SJISx0x1[] = {
{0,0,0},
{0x3F,0xBB,1U},
};
mb_wchar_range_t mb_cm_wcv_SJISx0[] = {
{0,0,0},
{0x0,0x16C3,188U},
};
mb_wchar_range_t mb_cm_dectab_SJISx0[] = {
{0x0,0x3E,0U},
{0x3F,0xBB,1U},
};
size_t mb_cm_dectab_bv_SJISx0[] = {
0U,
0U,
2U,
};
mb_char_node_t mb_cm_nodev_SJISx0[] = {
{0x40,0x7E,mb_cm_wcv_SJISx0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x80,0xFC,mb_cm_wcv_SJISx0x1,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJISx1[] = {
{0x0,0x3E,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJISx2[] = {
{0,0,0},
{0x16C4,0x2283,188U},
};
mb_wchar_range_t mb_cm_dectab_SJIS[] = {
{0x0,0x3E,1U},
{0x0,0x16C3,0U},
{0x16C4,0x2283,2U},
};
size_t mb_cm_dectab_bv_SJIS[] = {
0U,
1U,
3U,
};
mb_char_node_t mb_cm_nodev_SJIS[] = {
{0x81,0x9F,mb_cm_wcv_SJISx0,NULL,NULL,{mb_cm_nodev_SJISx0,2U,mb_cm_dectab_SJISx0,mb_cm_dectab_bv_SJISx0}},
{0xA1,0xDF,mb_cm_wcv_SJISx1,NULL,NULL,{NULL,0U,NULL,NULL}},
{0xE0,0xEF,mb_cm_wcv_SJISx2,NULL,NULL,{mb_cm_nodev_SJISx0,2U,mb_cm_dectab_SJISx0,mb_cm_dectab_bv_SJISx0}},
};
mb_char_map_t mb_cm_SJIS = {
mb_cm_nodev_SJIS,
3U,
mb_cm_dectab_SJIS,
mb_cm_dectab_bv_SJIS,
};
mb_wchar_range_t mb_wcv_SJIS[] = {
{0x20034E,0x20038C,63U},
{0x218308,0x21A58B,8836U},
};
mb_wchar_range_t mb_decmap_tab_SJIS[] = {
{0x0,0x7F,0U},
{0x20034E,0x20038C,1U},
{0x218308,0x21A58B,2U},
};
mb_decoder_t mb_decmap_destv_SJIS[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20034E,&mb_cm_SJIS,0U},
{NULL,NULL,0x218308,&mb_cm_SJIS,1U},
};
mb_decoder_map_t mb_decmap_SJIS = {
mb_decmap_tab_SJIS,
3U,
mb_decmap_destv_SJIS,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_SJIS(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_SJIS);
}
size_t
mb_conv_ws_to_SJIS(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_SJIS);
}
#endif
static const char *mb_namev_SJIS[] = {
"shift_jis",
"x-sjis",
"sjis",
"shift-jis",
};
static mb_conv_t mb_convv_SJIS[] = {
#ifdef USE_UCS
mb_conv_to_SJIS,
#else
#endif
NULL,
};
mb_ces_t mb_ces_SJIS = {
mb_namev_SJIS,
mb_flag_plus,0,
{mb_G0,mb_SJIS,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_SJIS,
&mb_decmap_SJIS,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_SJIS[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_SJIS,&mb_cm_nodev_SJIS[0U]},
{mb_wcv_SJIS,&mb_cm_nodev_SJIS[1U]},
{mb_wcv_SJIS,&mb_cm_nodev_SJIS[2U]}
};
short mb_encmap_iv_SJIS[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
-1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
};
mb_encoder_map_t mb_encmap_SJIS = {
mb_encmap_nodev_SJIS,
mb_encmap_iv_SJIS,
};
#endif
#ifdef USE_SJIS0213
mb_wchar_range_t mb_cm_wcv_SJIS0213x0x0[] = {
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x0x1[] = {
{0,0,0},
{0x3F,0xBB,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x3x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x3x1[] = {
{0,0,0},
{0,0,0},
{0x3F,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x3x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x4x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x3F,0xBB,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3F,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x5x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x6x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x6x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3F,0xBB,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x7x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x7x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3F,0x5D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x7x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x8x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3E,1U},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x8x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3F,0xBB,1U},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x0[] = {
{0,0,0},
{0x0,0x16C3,188U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x0[] = {
{0x0,0x3E,0U},
{0x3F,0xBB,1U},
};
size_t mb_cm_dectab_bv_SJIS0213x0[] = {
0U,
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x0[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x80,0xFC,mb_cm_wcv_SJIS0213x0x1,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x1[] = {
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x2[] = {
{0,0,0},
{0x16C4,0x2283,188U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x3[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x3[] = {
{0x0,0x3E,0U},
{0x3F,0x5D,1U},
{0x0,0x5D,2U},
};
size_t mb_cm_dectab_bv_SJIS0213x3[] = {
0U,
0U,
0U,
2U,
2U,
2U,
3U,
3U,
3U,
3U,
3U,
3U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x3[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x3x0,NULL,NULL,{NULL,2U,NULL,NULL}},
{0x80,0x9E,mb_cm_wcv_SJIS0213x3x1,NULL,NULL,{NULL,2U,NULL,NULL}},
{0x9F,0xFC,mb_cm_wcv_SJIS0213x3x2,NULL,NULL,{NULL,5U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBB,188U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x4[] = {
{0x0,0x3E,0U},
{0x3F,0xBB,1U},
};
size_t mb_cm_dectab_bv_SJIS0213x4[] = {
0U,
0U,
0U,
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x4[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x4x0,NULL,NULL,{NULL,3U,NULL,NULL}},
{0x80,0xFC,mb_cm_wcv_SJIS0213x4x1,NULL,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x5[] = {
{0x0,0x3E,0U},
{0x3F,0x5D,1U},
{0x0,0x5D,2U},
};
size_t mb_cm_dectab_bv_SJIS0213x5[] = {
0U,
0U,
0U,
0U,
0U,
2U,
2U,
3U,
3U,
3U,
3U,
3U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x5[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x5x0,NULL,NULL,{NULL,4U,NULL,NULL}},
{0x80,0x9E,mb_cm_wcv_SJIS0213x5x1,NULL,NULL,{NULL,4U,NULL,NULL}},
{0x9F,0xFC,mb_cm_wcv_SJIS0213x5x2,NULL,NULL,{NULL,6U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBB,188U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x6[] = {
{0x0,0x3E,0U},
{0x3F,0xBB,1U},
};
size_t mb_cm_dectab_bv_SJIS0213x6[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
2U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x6[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x6x0,NULL,NULL,{NULL,7U,NULL,NULL}},
{0x80,0xFC,mb_cm_wcv_SJIS0213x6x1,NULL,NULL,{NULL,7U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x7[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,94U},
{0x0,0x5D,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x7[] = {
{0x0,0x3E,0U},
{0x3F,0x5D,1U},
{0x0,0x5D,2U},
};
size_t mb_cm_dectab_bv_SJIS0213x7[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
3U,
3U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x7[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x7x0,NULL,NULL,{NULL,8U,NULL,NULL}},
{0x80,0x9E,mb_cm_wcv_SJIS0213x7x1,NULL,NULL,{NULL,8U,NULL,NULL}},
{0x9F,0xFC,mb_cm_wcv_SJIS0213x7x2,NULL,NULL,{NULL,9U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_SJIS0213x8[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5DF,188U},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213x8[] = {
{0x0,0x3E,0U},
{0x3F,0xBB,1U},
};
size_t mb_cm_dectab_bv_SJIS0213x8[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
};
mb_char_node_t mb_cm_nodev_SJIS0213x8[] = {
{0x40,0x7E,mb_cm_wcv_SJIS0213x8x0,NULL,NULL,{NULL,10U,NULL,NULL}},
{0x80,0xFC,mb_cm_wcv_SJIS0213x8x1,NULL,NULL,{NULL,10U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_SJIS0213[] = {
{0x0,0x3E,1U},
{0x0,0x16C3,0U},
{0x16C4,0x2283,2U},
{0x0,0x5D,3U},
{0x0,0xBB,4U},
{0x0,0x5D,5U},
{0x0,0x5D,3U},
{0x0,0x5D,5U},
{0x0,0xBB,6U},
{0x0,0x5D,7U},
{0x0,0x5D,7U},
{0x0,0x5DF,8U},
};
size_t mb_cm_dectab_bv_SJIS0213[] = {
0U,
1U,
3U,
4U,
5U,
6U,
7U,
8U,
9U,
10U,
11U,
12U,
};
mb_char_node_t mb_cm_nodev_SJIS0213[] = {
{0x81,0x9F,mb_cm_wcv_SJIS0213x0,NULL,NULL,{mb_cm_nodev_SJIS0213x0,2U,mb_cm_dectab_SJIS0213x0,mb_cm_dectab_bv_SJIS0213x0}},
{0xA1,0xDF,mb_cm_wcv_SJIS0213x1,NULL,NULL,{NULL,0U,NULL,NULL}},
{0xE0,0xEF,mb_cm_wcv_SJIS0213x2,NULL,NULL,{mb_cm_nodev_SJIS0213x0,2U,mb_cm_dectab_SJIS0213x0,mb_cm_dectab_bv_SJIS0213x0}},
{0xF0,0xF0,mb_cm_wcv_SJIS0213x3,NULL,NULL,{mb_cm_nodev_SJIS0213x3,3U,mb_cm_dectab_SJIS0213x3,mb_cm_dectab_bv_SJIS0213x3}},
{0xF1,0xF1,mb_cm_wcv_SJIS0213x4,NULL,NULL,{mb_cm_nodev_SJIS0213x4,2U,mb_cm_dectab_SJIS0213x4,mb_cm_dectab_bv_SJIS0213x4}},
{0xF2,0xF2,mb_cm_wcv_SJIS0213x5,NULL,NULL,{mb_cm_nodev_SJIS0213x5,3U,mb_cm_dectab_SJIS0213x5,mb_cm_dectab_bv_SJIS0213x5}},
{0xF3,0xF3,mb_cm_wcv_SJIS0213x6,NULL,NULL,{mb_cm_nodev_SJIS0213x6,2U,mb_cm_dectab_SJIS0213x6,mb_cm_dectab_bv_SJIS0213x6}},
{0xF4,0xF4,mb_cm_wcv_SJIS0213x7,NULL,NULL,{mb_cm_nodev_SJIS0213x7,3U,mb_cm_dectab_SJIS0213x7,mb_cm_dectab_bv_SJIS0213x7}},
{0xF5,0xFC,mb_cm_wcv_SJIS0213x8,NULL,NULL,{mb_cm_nodev_SJIS0213x8,2U,mb_cm_dectab_SJIS0213x8,mb_cm_dectab_bv_SJIS0213x8}},
};
mb_char_map_t mb_cm_SJIS0213 = {
mb_cm_nodev_SJIS0213,
9U,
mb_cm_dectab_SJIS0213,
mb_cm_dectab_bv_SJIS0213,
};
mb_wchar_range_t mb_wcv_SJIS0213[] = {
{0x20034E,0x20038C,63U},
{0x2343BC,0x23663F,8836U},
{0x236640,0x23669D,94U},
{0x2366FC,0x2367B7,188U},
{0x2367B8,0x236815,94U},
{0x2368D2,0x23692F,94U},
{0x236A4A,0x236AA7,94U},
{0x236AA8,0x236B63,188U},
{0x236B64,0x236BC1,94U},
{0x238286,0x2382E3,94U},
{0x2382E4,0x2388C3,1504U},
};
mb_wchar_range_t mb_decmap_tab_SJIS0213[] = {
{0x0,0x7F,0U},
{0x20034E,0x20038C,1U},
{0x2343BC,0x23663F,2U},
{0x236640,0x23669D,3U},
{0x2366FC,0x2367B7,4U},
{0x2367B8,0x236815,5U},
{0x2368D2,0x23692F,6U},
{0x236A4A,0x236AA7,7U},
{0x236AA8,0x236B63,8U},
{0x236B64,0x236BC1,9U},
{0x238286,0x2382E3,10U},
{0x2382E4,0x2388C3,11U},
};
mb_decoder_t mb_decmap_destv_SJIS0213[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20034E,&mb_cm_SJIS0213,0U},
{NULL,NULL,0x2343BC,&mb_cm_SJIS0213,1U},
{NULL,NULL,0x236640,&mb_cm_SJIS0213,2U},
{NULL,NULL,0x2366FC,&mb_cm_SJIS0213,3U},
{NULL,NULL,0x2367B8,&mb_cm_SJIS0213,4U},
{NULL,NULL,0x2368D2,&mb_cm_SJIS0213,5U},
{NULL,NULL,0x236A4A,&mb_cm_SJIS0213,6U},
{NULL,NULL,0x236AA8,&mb_cm_SJIS0213,7U},
{NULL,NULL,0x236B64,&mb_cm_SJIS0213,8U},
{NULL,NULL,0x238286,&mb_cm_SJIS0213,9U},
{NULL,NULL,0x2382E4,&mb_cm_SJIS0213,10U},
};
mb_decoder_map_t mb_decmap_SJIS0213 = {
mb_decmap_tab_SJIS0213,
12U,
mb_decmap_destv_SJIS0213,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_SJIS0213(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_SJIS0213);
}
size_t
mb_conv_ws_to_SJIS0213(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_SJIS0213);
}
#endif
static const char *mb_namev_SJIS0213[] = {
"shift_jisx0213",
"shift-jisx0213",
};
static mb_conv_t mb_convv_SJIS0213[] = {
#ifdef USE_UCS
mb_conv_to_SJIS0213,
#else
#endif
NULL,
};
mb_ces_t mb_ces_SJIS0213 = {
mb_namev_SJIS0213,
mb_flag_plus,0,
{mb_G0,mb_SJIS0213,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_SJIS0213,
&mb_decmap_SJIS0213,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_SJIS0213[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[0U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[1U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[2U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[3U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[4U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[5U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[6U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[7U]},
{mb_wcv_SJIS0213,&mb_cm_nodev_SJIS0213[8U]}
};
short mb_encmap_iv_SJIS0213[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
-1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
8,9,10,11,12,13,13,13,13,13,13,13,13,-1,-1,-1
};
mb_encoder_map_t mb_encmap_SJIS0213 = {
mb_encmap_nodev_SJIS0213,
mb_encmap_iv_SJIS0213,
};
#endif
#ifdef USE_BIG5
mb_wchar_range_t mb_cm_wcv_BIG5x0x0[] = {
{0x0,0x3E,1U},
};
mb_wchar_range_t mb_cm_wcv_BIG5x0x1[] = {
{0x3F,0x9C,1U},
};
mb_wchar_range_t mb_cm_wcv_BIG5x0[] = {
{0x0,0x39A5,157U},
};
mb_wchar_range_t mb_cm_dectab_BIG5x0[] = {
{0x0,0x3E,0U},
{0x3F,0x9C,1U},
};
size_t mb_cm_dectab_bv_BIG5x0[] = {
0U,
2U,
};
mb_char_node_t mb_cm_nodev_BIG5x0[] = {
{0x40,0x7E,mb_cm_wcv_BIG5x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
{0xA1,0xFE,mb_cm_wcv_BIG5x0x1,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_BIG5[] = {
{0x0,0x39A5,0U},
};
size_t mb_cm_dectab_bv_BIG5[] = {
0U,
1U,
};
mb_char_node_t mb_cm_nodev_BIG5[] = {
{0xA1,0xFE,mb_cm_wcv_BIG5x0,NULL,NULL,{mb_cm_nodev_BIG5x0,2U,mb_cm_dectab_BIG5x0,mb_cm_dectab_bv_BIG5x0}},
};
mb_char_map_t mb_cm_BIG5 = {
mb_cm_nodev_BIG5,
1U,
mb_cm_dectab_BIG5,
mb_cm_dectab_bv_BIG5,
};
mb_wchar_range_t mb_wcv_BIG5[] = {
{0x338000,0x33B9A5,14758U},
};
mb_wchar_range_t mb_decmap_tab_BIG5[] = {
{0x0,0x7F,0U},
{0x338000,0x33B9A5,1U},
};
mb_decoder_t mb_decmap_destv_BIG5[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x338000,&mb_cm_BIG5,0U},
};
mb_decoder_map_t mb_decmap_BIG5 = {
mb_decmap_tab_BIG5,
2U,
mb_decmap_destv_BIG5,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_BIG5(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_BIG5);
}
size_t
mb_conv_ws_to_BIG5(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_BIG5);
}
#endif
static const char *mb_namev_BIG5[] = {
"big5",
"cn-big5",
};
static mb_conv_t mb_convv_BIG5[] = {
#ifdef USE_UCS
mb_conv_to_BIG5,
#else
#endif
NULL,
};
mb_ces_t mb_ces_BIG5 = {
mb_namev_BIG5,
mb_flag_plus,0,
{mb_G0,mb_BIG5,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_BIG5,
&mb_decmap_BIG5,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_BIG5[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_BIG5,&mb_cm_nodev_BIG5[0U]}
};
short mb_encmap_iv_BIG5[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,-1
};
mb_encoder_map_t mb_encmap_BIG5 = {
mb_encmap_nodev_BIG5,
mb_encmap_iv_BIG5,
};
#endif
#ifdef USE_JOHAB
mb_wchar_range_t mb_cm_wcv_JOHABx0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3D,1U},
};
mb_wchar_range_t mb_cm_wcv_JOHABx0x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3E,0xBB,1U},
};
mb_wchar_range_t mb_cm_wcv_JOHABx1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4D,1U},
};
mb_wchar_range_t mb_cm_wcv_JOHABx1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x4E,0xBB,1U},
};
mb_wchar_range_t mb_cm_wcv_JOHABx2x0[] = {
{0x0,0x4D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx2x1[] = {
{0x4E,0xBB,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx3x0[] = {
{0,0,0},
{0x0,0x4D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx3x1[] = {
{0,0,0},
{0x4E,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx3x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x32,1U},
};
mb_wchar_range_t mb_cm_wcv_JOHABx3x3[] = {
{0,0,0},
{0,0,0},
{0x0,0x2A,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx4x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x4E,0xBB,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x4E,0xBB,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_JOHABx0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3ABF,188U},
};
mb_wchar_range_t mb_cm_dectab_JOHABx0[] = {
{0x0,0x3D,0U},
{0x3E,0xBB,1U},
};
size_t mb_cm_dectab_bv_JOHABx0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
2U,
};
mb_char_node_t mb_cm_nodev_JOHABx0[] = {
{0x41,0x7E,mb_cm_wcv_JOHABx0x0,NULL,NULL,{NULL,5U,NULL,NULL}},
{0x81,0xFE,mb_cm_wcv_JOHABx0x1,NULL,NULL,{NULL,5U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_JOHABx1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3AC0,0x3B7B,188U},
};
mb_wchar_range_t mb_cm_dectab_JOHABx1[] = {
{0x0,0x4D,0U},
{0x4E,0xBB,1U},
};
size_t mb_cm_dectab_bv_JOHABx1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
2U,
};
mb_char_node_t mb_cm_nodev_JOHABx1[] = {
{0x31,0x7E,mb_cm_wcv_JOHABx1x0,NULL,NULL,{NULL,5U,NULL,NULL}},
{0x91,0xFE,mb_cm_wcv_JOHABx1x1,NULL,NULL,{NULL,5U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_JOHABx2[] = {
{0x0,0xBB,188U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_JOHABx2[] = {
{0x0,0x4D,0U},
{0x4E,0xBB,1U},
};
size_t mb_cm_dectab_bv_JOHABx2[] = {
0U,
2U,
2U,
2U,
2U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_JOHABx2[] = {
{0x31,0x7E,mb_cm_wcv_JOHABx2x0,NULL,NULL,{NULL,0U,NULL,NULL}},
{0x91,0xFE,mb_cm_wcv_JOHABx2x1,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_JOHABx3[] = {
{0,0,0},
{0x0,0x5D,94U},
{0x0,0x2A,43U},
{0,0,0},
{0,0,0},
{0x3B7C,0x3BAE,51U},
};
mb_wchar_range_t mb_cm_dectab_JOHABx3[] = {
{0x0,0x4D,0U},
{0x4E,0x5D,1U},
{0x0,0x2A,3U},
{0x0,0x32,2U},
};
size_t mb_cm_dectab_bv_JOHABx3[] = {
0U,
0U,
2U,
3U,
3U,
3U,
4U,
};
mb_char_node_t mb_cm_nodev_JOHABx3[] = {
{0x31,0x7E,mb_cm_wcv_JOHABx3x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x91,0xA0,mb_cm_wcv_JOHABx3x1,NULL,NULL,{NULL,1U,NULL,NULL}},
{0xA1,0xD3,mb_cm_wcv_JOHABx3x2,NULL,NULL,{NULL,5U,NULL,NULL}},
{0xD4,0xFE,mb_cm_wcv_JOHABx3x3,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_JOHABx4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2EF,188U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_JOHABx4[] = {
{0x0,0x4D,0U},
{0x4E,0xBB,1U},
};
size_t mb_cm_dectab_bv_JOHABx4[] = {
0U,
0U,
0U,
0U,
2U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_JOHABx4[] = {
{0x31,0x7E,mb_cm_wcv_JOHABx4x0,NULL,NULL,{NULL,3U,NULL,NULL}},
{0x91,0xFE,mb_cm_wcv_JOHABx4x1,NULL,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_JOHABx5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1317,188U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_JOHABx5[] = {
{0x0,0x4D,0U},
{0x4E,0xBB,1U},
};
size_t mb_cm_dectab_bv_JOHABx5[] = {
0U,
0U,
0U,
0U,
0U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_JOHABx5[] = {
{0x31,0x7E,mb_cm_wcv_JOHABx5x0,NULL,NULL,{NULL,4U,NULL,NULL}},
{0x91,0xFE,mb_cm_wcv_JOHABx5x1,NULL,NULL,{NULL,4U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_JOHAB[] = {
{0x0,0xBB,2U},
{0x0,0x5D,3U},
{0x0,0x2A,3U},
{0x0,0x2EF,4U},
{0x0,0x1317,5U},
{0x0,0x3ABF,0U},
{0x3AC0,0x3B7B,1U},
{0x3B7C,0x3BAE,3U},
};
size_t mb_cm_dectab_bv_JOHAB[] = {
0U,
1U,
2U,
3U,
4U,
5U,
8U,
};
mb_char_node_t mb_cm_nodev_JOHAB[] = {
{0x84,0xD3,mb_cm_wcv_JOHABx0,NULL,NULL,{mb_cm_nodev_JOHABx0,2U,mb_cm_dectab_JOHABx0,mb_cm_dectab_bv_JOHABx0}},
{0xD8,0xD8,mb_cm_wcv_JOHABx1,NULL,NULL,{mb_cm_nodev_JOHABx1,2U,mb_cm_dectab_JOHABx1,mb_cm_dectab_bv_JOHABx1}},
{0xD9,0xD9,mb_cm_wcv_JOHABx2,NULL,NULL,{mb_cm_nodev_JOHABx2,2U,mb_cm_dectab_JOHABx2,mb_cm_dectab_bv_JOHABx2}},
{0xDA,0xDA,mb_cm_wcv_JOHABx3,NULL,NULL,{mb_cm_nodev_JOHABx3,4U,mb_cm_dectab_JOHABx3,mb_cm_dectab_bv_JOHABx3}},
{0xDB,0xDE,mb_cm_wcv_JOHABx4,NULL,NULL,{mb_cm_nodev_JOHABx4,2U,mb_cm_dectab_JOHABx4,mb_cm_dectab_bv_JOHABx4}},
{0xE0,0xF9,mb_cm_wcv_JOHABx5,NULL,NULL,{mb_cm_nodev_JOHABx5,2U,mb_cm_dectab_JOHABx5,mb_cm_dectab_bv_JOHABx5}},
};
mb_char_map_t mb_cm_JOHAB = {
mb_cm_nodev_JOHAB,
6U,
mb_cm_dectab_JOHAB,
mb_cm_dectab_bv_JOHAB,
};
mb_wchar_range_t mb_wcv_JOHAB[] = {
{0x21A58C,0x21A647,188U},
{0x21A648,0x21A6A5,94U},
{0x21A6D9,0x21A703,43U},
{0x21A704,0x21A9F3,752U},
{0x21B49A,0x21C7B1,4888U},
{0x340000,0x343BAE,15279U},
};
mb_wchar_range_t mb_decmap_tab_JOHAB[] = {
{0x0,0x7F,0U},
{0x21A58C,0x21A647,1U},
{0x21A648,0x21A6A5,2U},
{0x21A6D9,0x21A703,3U},
{0x21A704,0x21A9F3,4U},
{0x21B49A,0x21C7B1,5U},
{0x340000,0x343BAE,6U},
};
mb_decoder_t mb_decmap_destv_JOHAB[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x21A58C,&mb_cm_JOHAB,0U},
{NULL,NULL,0x21A648,&mb_cm_JOHAB,1U},
{NULL,NULL,0x21A6D9,&mb_cm_JOHAB,2U},
{NULL,NULL,0x21A704,&mb_cm_JOHAB,3U},
{NULL,NULL,0x21B49A,&mb_cm_JOHAB,4U},
{NULL,NULL,0x340000,&mb_cm_JOHAB,5U},
};
mb_decoder_map_t mb_decmap_JOHAB = {
mb_decmap_tab_JOHAB,
7U,
mb_decmap_destv_JOHAB,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_JOHAB(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_JOHAB);
}
size_t
mb_conv_ws_to_JOHAB(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_JOHAB);
}
#endif
static const char *mb_namev_JOHAB[] = {
"x-johab",
};
static mb_conv_t mb_convv_JOHAB[] = {
#ifdef USE_UCS
mb_conv_to_JOHAB,
#else
#endif
NULL,
};
mb_ces_t mb_ces_JOHAB = {
mb_namev_JOHAB,
mb_flag_plus,0,
{mb_G0,mb_JOHAB,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_JOHAB,
&mb_decmap_JOHAB,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_JOHAB[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_JOHAB,&mb_cm_nodev_JOHAB[0U]},
{mb_wcv_JOHAB,&mb_cm_nodev_JOHAB[1U]},
{mb_wcv_JOHAB,&mb_cm_nodev_JOHAB[2U]},
{mb_wcv_JOHAB,&mb_cm_nodev_JOHAB[3U]},
{mb_wcv_JOHAB,&mb_cm_nodev_JOHAB[4U]},
{mb_wcv_JOHAB,&mb_cm_nodev_JOHAB[5U]}
};
short mb_encmap_iv_JOHAB[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,-1,-1,-1,-1,6,7,8,9,9,9,9,-1,
10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
10,10,10,10,10,10,10,10,10,10,-1,-1,-1,-1,-1,-1
};
mb_encoder_map_t mb_encmap_JOHAB = {
mb_encmap_nodev_JOHAB,
mb_encmap_iv_JOHAB,
};
#endif
#ifdef USE_UHANG
mb_wchar_range_t mb_cm_wcv_UHANGx0x0[] = {
{0,0,0},
{0x0,0x19,1U},
};
mb_wchar_range_t mb_cm_wcv_UHANGx0x1[] = {
{0,0,0},
{0x1A,0x33,1U},
};
mb_wchar_range_t mb_cm_wcv_UHANGx0x2[] = {
{0,0,0},
{0x34,0xB1,1U},
};
mb_wchar_range_t mb_cm_wcv_UHANGx1x0[] = {
{0,0,0},
{0x0,0x19,1U},
};
mb_wchar_range_t mb_cm_wcv_UHANGx1x1[] = {
{0,0,0},
{0x1A,0x33,1U},
};
mb_wchar_range_t mb_cm_wcv_UHANGx1x2[] = {
{0,0,0},
{0x34,0x53,1U},
};
mb_wchar_range_t mb_cm_wcv_UHANGx1x3[] = {
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_UHANGx2x0[] = {
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_UHANGx0[] = {
{0,0,0},
{0x0,0x163F,178U},
};
mb_wchar_range_t mb_cm_dectab_UHANGx0[] = {
{0x0,0x19,0U},
{0x1A,0x33,1U},
{0x34,0xB1,2U},
};
size_t mb_cm_dectab_bv_UHANGx0[] = {
0U,
0U,
3U,
};
mb_char_node_t mb_cm_nodev_UHANGx0[] = {
{0x41,0x5A,mb_cm_wcv_UHANGx0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x61,0x7A,mb_cm_wcv_UHANGx0x1,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x81,0xFE,mb_cm_wcv_UHANGx0x2,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_UHANGx1[] = {
{0x0,0xDF3,94U},
{0x1640,0x22B7,84U},
};
mb_wchar_range_t mb_cm_dectab_UHANGx1[] = {
{0x0,0x5D,3U},
{0x0,0x19,0U},
{0x1A,0x33,1U},
{0x34,0x53,2U},
};
size_t mb_cm_dectab_bv_UHANGx1[] = {
0U,
1U,
4U,
};
mb_char_node_t mb_cm_nodev_UHANGx1[] = {
{0x41,0x5A,mb_cm_wcv_UHANGx1x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x61,0x7A,mb_cm_wcv_UHANGx1x1,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x81,0xA0,mb_cm_wcv_UHANGx1x2,NULL,NULL,{NULL,1U,NULL,NULL}},
{0xA1,0xFE,mb_cm_wcv_UHANGx1x3,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_UHANGx2[] = {
{0xDF4,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_UHANGx2[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_UHANGx2[] = {
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_UHANGx2[] = {
{0xA1,0xFE,mb_cm_wcv_UHANGx2x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_UHANG[] = {
{0x0,0xDF3,1U},
{0xDF4,0x2283,2U},
{0x0,0x163F,0U},
{0x1640,0x22B7,1U},
};
size_t mb_cm_dectab_bv_UHANG[] = {
0U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_UHANG[] = {
{0x81,0xA0,mb_cm_wcv_UHANGx0,NULL,NULL,{mb_cm_nodev_UHANGx0,3U,mb_cm_dectab_UHANGx0,mb_cm_dectab_bv_UHANGx0}},
{0xA1,0xC6,mb_cm_wcv_UHANGx1,NULL,NULL,{mb_cm_nodev_UHANGx1,4U,mb_cm_dectab_UHANGx1,mb_cm_dectab_bv_UHANGx1}},
{0xC7,0xFE,mb_cm_wcv_UHANGx2,NULL,NULL,{mb_cm_nodev_UHANGx2,1U,mb_cm_dectab_UHANGx2,mb_cm_dectab_bv_UHANGx2}},
};
mb_char_map_t mb_cm_UHANG = {
mb_cm_nodev_UHANG,
3U,
mb_cm_dectab_UHANG,
mb_cm_dectab_bv_UHANG,
};
mb_wchar_range_t mb_wcv_UHANG[] = {
{0x21A58C,0x21C80F,8836U},
{0x348000,0x34A2B7,8888U},
};
mb_wchar_range_t mb_decmap_tab_UHANG[] = {
{0x0,0x7F,0U},
{0x21A58C,0x21C80F,1U},
{0x348000,0x34A2B7,2U},
};
mb_decoder_t mb_decmap_destv_UHANG[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x21A58C,&mb_cm_UHANG,0U},
{NULL,NULL,0x348000,&mb_cm_UHANG,1U},
};
mb_decoder_map_t mb_decmap_UHANG = {
mb_decmap_tab_UHANG,
3U,
mb_decmap_destv_UHANG,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_UHANG(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_UHANG);
}
size_t
mb_conv_ws_to_UHANG(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_UHANG);
}
#endif
static const char *mb_namev_UHANG[] = {
"x-unified-hangle",
"x-uhc",
};
static mb_conv_t mb_convv_UHANG[] = {
#ifdef USE_UCS
mb_conv_to_UHANG,
#else
#endif
NULL,
};
mb_ces_t mb_ces_UHANG = {
mb_namev_UHANG,
mb_flag_plus,0,
{mb_G0,mb_UHANG,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_UHANG,
&mb_decmap_UHANG,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_UHANG[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_UHANG,&mb_cm_nodev_UHANG[0U]},
{mb_wcv_UHANG,&mb_cm_nodev_UHANG[1U]},
{mb_wcv_UHANG,&mb_cm_nodev_UHANG[2U]}
};
short mb_encmap_iv_UHANG[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,-1
};
mb_encoder_map_t mb_encmap_UHANG = {
mb_encmap_nodev_UHANG,
mb_encmap_iv_UHANG,
};
#endif
#ifdef USE_EUC_TW
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x0x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x1x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x2x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x3x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x4x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x5x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x6x0x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x7x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x8x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x9x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x10x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x11x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x12x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x13x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x14x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x15x0x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x0x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x0x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x0x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x0x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x0x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x1x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x1x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x1x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x1x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x1x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x2x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x2x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x2x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x2x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x2x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x3x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x3x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x3x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x3x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x3x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x4x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x4x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x4x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x4x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x4x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x5x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x5x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x5x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x5x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x5x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x6x0[] = {
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x6x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x6x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x6x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x6x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x7x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x7x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x7x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x7x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x7x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x8x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x8x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x8x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x8x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x8x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x9x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x9x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x9x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x9x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x9x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x10x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x10x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x10x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x10x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x10x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x11x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x11x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x11x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x11x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x11x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x12x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x12x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x12x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x12x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x12x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x13x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x13x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x13x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x13x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x13x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x14x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x14x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x14x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x14x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x14x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x15x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x15x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x15x0[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x15x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x15x0x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x0[] = {
{0,0,0},
{0x0,0x2283,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x0[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x0[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x0x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x0x0,1U,mb_cm_dectab_EUC_TWx0x0x0,mb_cm_dectab_bv_EUC_TWx0x0x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x1[] = {
{0,0,0},
{0x2284,0x4507,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x1[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x1[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x1[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x1x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x1x0,1U,mb_cm_dectab_EUC_TWx0x1x0,mb_cm_dectab_bv_EUC_TWx0x1x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x2[] = {
{0,0,0},
{0x4508,0x678B,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x2[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x2[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x2[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x2x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x2x0,1U,mb_cm_dectab_EUC_TWx0x2x0,mb_cm_dectab_bv_EUC_TWx0x2x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x3[] = {
{0,0,0},
{0x678C,0x8A0F,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x3[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x3[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x3[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x3x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x3x0,1U,mb_cm_dectab_EUC_TWx0x3x0,mb_cm_dectab_bv_EUC_TWx0x3x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x4[] = {
{0,0,0},
{0x8A10,0xAC93,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x4[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x4[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x4[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x4x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x4x0,1U,mb_cm_dectab_EUC_TWx0x4x0,mb_cm_dectab_bv_EUC_TWx0x4x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x5[] = {
{0,0,0},
{0xAC94,0xCF17,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x5[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x5[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x5[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x5x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x5x0,1U,mb_cm_dectab_EUC_TWx0x5x0,mb_cm_dectab_bv_EUC_TWx0x5x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x6[] = {
{0,0,0},
{0xCF18,0xF19B,8836U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x6[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x6[] = {
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x6[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x6x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x6x0,1U,mb_cm_dectab_EUC_TWx0x6x0,mb_cm_dectab_bv_EUC_TWx0x6x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x7[] = {
{0,0,0},
{0,0,0},
{0x0,0x2283,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x7[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x7[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x7[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x7x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x7x0,1U,mb_cm_dectab_EUC_TWx0x7x0,mb_cm_dectab_bv_EUC_TWx0x7x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x8[] = {
{0,0,0},
{0,0,0},
{0x2284,0x4507,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x8[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x8[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x8[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x8x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x8x0,1U,mb_cm_dectab_EUC_TWx0x8x0,mb_cm_dectab_bv_EUC_TWx0x8x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x9[] = {
{0,0,0},
{0,0,0},
{0x4508,0x678B,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x9[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x9[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x9[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x9x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x9x0,1U,mb_cm_dectab_EUC_TWx0x9x0,mb_cm_dectab_bv_EUC_TWx0x9x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x10[] = {
{0,0,0},
{0,0,0},
{0x678C,0x8A0F,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x10[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x10[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x10[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x10x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x10x0,1U,mb_cm_dectab_EUC_TWx0x10x0,mb_cm_dectab_bv_EUC_TWx0x10x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x11[] = {
{0,0,0},
{0,0,0},
{0x8A10,0xAC93,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x11[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x11[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x11[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x11x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x11x0,1U,mb_cm_dectab_EUC_TWx0x11x0,mb_cm_dectab_bv_EUC_TWx0x11x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x12[] = {
{0,0,0},
{0,0,0},
{0xAC94,0xCF17,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x12[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x12[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x12[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x12x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x12x0,1U,mb_cm_dectab_EUC_TWx0x12x0,mb_cm_dectab_bv_EUC_TWx0x12x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x13[] = {
{0,0,0},
{0,0,0},
{0xCF18,0xF19B,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x13[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x13[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x13[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x13x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x13x0,1U,mb_cm_dectab_EUC_TWx0x13x0,mb_cm_dectab_bv_EUC_TWx0x13x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x14[] = {
{0,0,0},
{0,0,0},
{0xF19C,0x1141F,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x14[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x14[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x14[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x14x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x14x0,1U,mb_cm_dectab_EUC_TWx0x14x0,mb_cm_dectab_bv_EUC_TWx0x14x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0x15[] = {
{0,0,0},
{0,0,0},
{0x11420,0x136A3,8836U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0x15[] = {
{0x0,0x2283,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx0x15[] = {
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0x15[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx0x15x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x15x0,1U,mb_cm_dectab_EUC_TWx0x15x0,mb_cm_dectab_bv_EUC_TWx0x15x0}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx1x0[] = {
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx0[] = {
{0,0,0},
{0x0,0xF19B,61852U},
{0x0,0x136A3,79524U},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx0[] = {
{0x0,0x2283,0U},
{0x2284,0x4507,1U},
{0x4508,0x678B,2U},
{0x678C,0x8A0F,3U},
{0x8A10,0xAC93,4U},
{0xAC94,0xCF17,5U},
{0xCF18,0xF19B,6U},
{0x0,0x2283,7U},
{0x2284,0x4507,8U},
{0x4508,0x678B,9U},
{0x678C,0x8A0F,10U},
{0x8A10,0xAC93,11U},
{0xAC94,0xCF17,12U},
{0xCF18,0xF19B,13U},
{0xF19C,0x1141F,14U},
{0x11420,0x136A3,15U},
};
size_t mb_cm_dectab_bv_EUC_TWx0[] = {
0U,
0U,
7U,
16U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx0[] = {
{0xA1,0xA1,mb_cm_wcv_EUC_TWx0x0,NULL,NULL,{mb_cm_nodev_EUC_TWx0x0,1U,mb_cm_dectab_EUC_TWx0x0,mb_cm_dectab_bv_EUC_TWx0x0}},
{0xA2,0xA2,mb_cm_wcv_EUC_TWx0x1,NULL,NULL,{mb_cm_nodev_EUC_TWx0x1,1U,mb_cm_dectab_EUC_TWx0x1,mb_cm_dectab_bv_EUC_TWx0x1}},
{0xA3,0xA3,mb_cm_wcv_EUC_TWx0x2,NULL,NULL,{mb_cm_nodev_EUC_TWx0x2,1U,mb_cm_dectab_EUC_TWx0x2,mb_cm_dectab_bv_EUC_TWx0x2}},
{0xA4,0xA4,mb_cm_wcv_EUC_TWx0x3,NULL,NULL,{mb_cm_nodev_EUC_TWx0x3,1U,mb_cm_dectab_EUC_TWx0x3,mb_cm_dectab_bv_EUC_TWx0x3}},
{0xA5,0xA5,mb_cm_wcv_EUC_TWx0x4,NULL,NULL,{mb_cm_nodev_EUC_TWx0x4,1U,mb_cm_dectab_EUC_TWx0x4,mb_cm_dectab_bv_EUC_TWx0x4}},
{0xA6,0xA6,mb_cm_wcv_EUC_TWx0x5,NULL,NULL,{mb_cm_nodev_EUC_TWx0x5,1U,mb_cm_dectab_EUC_TWx0x5,mb_cm_dectab_bv_EUC_TWx0x5}},
{0xA7,0xA7,mb_cm_wcv_EUC_TWx0x6,NULL,NULL,{mb_cm_nodev_EUC_TWx0x6,1U,mb_cm_dectab_EUC_TWx0x6,mb_cm_dectab_bv_EUC_TWx0x6}},
{0xA8,0xA8,mb_cm_wcv_EUC_TWx0x7,NULL,NULL,{mb_cm_nodev_EUC_TWx0x7,1U,mb_cm_dectab_EUC_TWx0x7,mb_cm_dectab_bv_EUC_TWx0x7}},
{0xA9,0xA9,mb_cm_wcv_EUC_TWx0x8,NULL,NULL,{mb_cm_nodev_EUC_TWx0x8,1U,mb_cm_dectab_EUC_TWx0x8,mb_cm_dectab_bv_EUC_TWx0x8}},
{0xAA,0xAA,mb_cm_wcv_EUC_TWx0x9,NULL,NULL,{mb_cm_nodev_EUC_TWx0x9,1U,mb_cm_dectab_EUC_TWx0x9,mb_cm_dectab_bv_EUC_TWx0x9}},
{0xAB,0xAB,mb_cm_wcv_EUC_TWx0x10,NULL,NULL,{mb_cm_nodev_EUC_TWx0x10,1U,mb_cm_dectab_EUC_TWx0x10,mb_cm_dectab_bv_EUC_TWx0x10}},
{0xAC,0xAC,mb_cm_wcv_EUC_TWx0x11,NULL,NULL,{mb_cm_nodev_EUC_TWx0x11,1U,mb_cm_dectab_EUC_TWx0x11,mb_cm_dectab_bv_EUC_TWx0x11}},
{0xAD,0xAD,mb_cm_wcv_EUC_TWx0x12,NULL,NULL,{mb_cm_nodev_EUC_TWx0x12,1U,mb_cm_dectab_EUC_TWx0x12,mb_cm_dectab_bv_EUC_TWx0x12}},
{0xAE,0xAE,mb_cm_wcv_EUC_TWx0x13,NULL,NULL,{mb_cm_nodev_EUC_TWx0x13,1U,mb_cm_dectab_EUC_TWx0x13,mb_cm_dectab_bv_EUC_TWx0x13}},
{0xAF,0xAF,mb_cm_wcv_EUC_TWx0x14,NULL,NULL,{mb_cm_nodev_EUC_TWx0x14,1U,mb_cm_dectab_EUC_TWx0x14,mb_cm_dectab_bv_EUC_TWx0x14}},
{0xB0,0xB0,mb_cm_wcv_EUC_TWx0x15,NULL,NULL,{mb_cm_nodev_EUC_TWx0x15,1U,mb_cm_dectab_EUC_TWx0x15,mb_cm_dectab_bv_EUC_TWx0x15}},
};
mb_wchar_range_t mb_cm_wcv_EUC_TWx1[] = {
{0x0,0x2283,94U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_TWx1[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_TWx1[] = {
0U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_TWx1[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_TWx1x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_EUC_TW[] = {
{0x0,0x2283,1U},
{0x0,0xF19B,0U},
{0x0,0x136A3,0U},
};
size_t mb_cm_dectab_bv_EUC_TW[] = {
0U,
1U,
2U,
3U,
};
mb_char_node_t mb_cm_nodev_EUC_TW[] = {
{0x8E,0x8E,mb_cm_wcv_EUC_TWx0,NULL,NULL,{mb_cm_nodev_EUC_TWx0,16U,mb_cm_dectab_EUC_TWx0,mb_cm_dectab_bv_EUC_TWx0}},
{0xA1,0xFE,mb_cm_wcv_EUC_TWx1,NULL,NULL,{mb_cm_nodev_EUC_TWx1,1U,mb_cm_dectab_EUC_TWx1,mb_cm_dectab_bv_EUC_TWx1}},
};
mb_char_map_t mb_cm_EUC_TW = {
mb_cm_nodev_EUC_TW,
2U,
mb_cm_dectab_EUC_TW,
mb_cm_dectab_bv_EUC_TW,
};
mb_wchar_range_t mb_wcv_EUC_TW[] = {
{0x222F9C,0x22521F,8836U},
{0x222F9C,0x232137,61852U},
{0x350000,0x3636A3,79524U},
};
mb_wchar_range_t mb_decmap_tab_EUC_TW[] = {
{0x0,0x7F,0U},
{0x222F9C,0x22521F,1U},
{0x225220,0x232137,2U},
{0x350000,0x3636A3,3U},
};
mb_decoder_t mb_decmap_destv_EUC_TW[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x222F9C,&mb_cm_EUC_TW,0U},
{NULL,NULL,0x222F9C,&mb_cm_EUC_TW,1U},
{NULL,NULL,0x350000,&mb_cm_EUC_TW,2U},
};
mb_decoder_map_t mb_decmap_EUC_TW = {
mb_decmap_tab_EUC_TW,
4U,
mb_decmap_destv_EUC_TW,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_EUC_TW(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_EUC_TW);
}
size_t
mb_conv_ws_to_EUC_TW(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_EUC_TW);
}
#endif
static const char *mb_namev_EUC_TW[] = {
"x-euc-tw",
"euc-tw",
};
static mb_conv_t mb_convv_EUC_TW[] = {
#ifdef USE_UCS
mb_conv_to_EUC_TW,
#else
#endif
NULL,
};
mb_ces_t mb_ces_EUC_TW = {
mb_namev_EUC_TW,
mb_flag_plus,0,
{mb_G0,mb_EUC_TW,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_EUC_TW,
&mb_decmap_EUC_TW,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_EUC_TW[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_EUC_TW,&mb_cm_nodev_EUC_TW[0U]},
{mb_wcv_EUC_TW,&mb_cm_nodev_EUC_TW[1U]}
};
short mb_encmap_iv_EUC_TW[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,5,-1,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,-1
};
mb_encoder_map_t mb_encmap_EUC_TW = {
mb_encmap_nodev_EUC_TW,
mb_encmap_iv_EUC_TW,
};
#endif
#ifdef USE_EUC_JISX0213_PACKED
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x1x0[] = {
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x3x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x6x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x7x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x8x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x9x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx0x0[] = {
{0x0,0x3E,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x0[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x0[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x0x0,NULL,NULL,{NULL,7U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x1[] = {
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x1[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x1[] = {
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x1[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x1x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x119,94U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x2[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x2[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x2x0,NULL,NULL,{NULL,8U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x3[] = {
{0,0,0},
{0,0,0},
{0x0,0xBB,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x3[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x3[] = {
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x3[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x3x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,94U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x4[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x4[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x4x0,NULL,NULL,{NULL,9U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x119,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x5[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x5[] = {
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x5[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x5x0,NULL,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x177,94U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x6[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x6[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x6[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x6x0,NULL,NULL,{NULL,10U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x7[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x16C3,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x7[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x7[] = {
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x7[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x7x0,NULL,NULL,{NULL,4U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x8[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x34D,94U},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x8[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x8[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x8[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x8x0,NULL,NULL,{NULL,11U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1x9[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2EF,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1x9[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x9[] = {
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1x9[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x9x0,NULL,NULL,{NULL,5U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx0[] = {
{0x0,0x3E,63U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx0[] = {
{0x0,0x3E,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx0[] = {
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx0[] = {
{0xA1,0xDF,mb_cm_wcv_EUC_JISX0213_PACKEDx0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx1[] = {
{0,0,0},
{0x0,0x5D,94U},
{0x0,0xBB,188U},
{0x0,0x119,282U},
{0x0,0x16C3,5828U},
{0x0,0x2EF,752U},
{0,0,0},
{0x0,0x5D,94U},
{0x0,0x119,282U},
{0x0,0x5D,94U},
{0x0,0x177,376U},
{0x0,0x34D,846U},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx1[] = {
{0x0,0x5D,1U},
{0x0,0xBB,3U},
{0x0,0x119,5U},
{0x0,0x16C3,7U},
{0x0,0x2EF,9U},
{0x0,0x5D,0U},
{0x0,0x119,2U},
{0x0,0x5D,4U},
{0x0,0x177,6U},
{0x0,0x34D,8U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1[] = {
0U,
0U,
1U,
2U,
3U,
4U,
5U,
5U,
6U,
7U,
8U,
9U,
10U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx1[] = {
{0xA1,0xA1,mb_cm_wcv_EUC_JISX0213_PACKEDx1x0,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x0,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x0,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x0}},
{0xA2,0xA2,mb_cm_wcv_EUC_JISX0213_PACKEDx1x1,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x1,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x1,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x1}},
{0xA3,0xA5,mb_cm_wcv_EUC_JISX0213_PACKEDx1x2,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x2,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x2,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x2}},
{0xA6,0xA7,mb_cm_wcv_EUC_JISX0213_PACKEDx1x3,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x3,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x3,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x3}},
{0xA8,0xA8,mb_cm_wcv_EUC_JISX0213_PACKEDx1x4,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x4,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x4,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x4}},
{0xA9,0xAB,mb_cm_wcv_EUC_JISX0213_PACKEDx1x5,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x5,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x5,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x5}},
{0xAC,0xAF,mb_cm_wcv_EUC_JISX0213_PACKEDx1x6,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x6,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x6,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x6}},
{0xB0,0xED,mb_cm_wcv_EUC_JISX0213_PACKEDx1x7,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x7,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x7,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x7}},
{0xEE,0xF6,mb_cm_wcv_EUC_JISX0213_PACKEDx1x8,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x8,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x8,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x8}},
{0xF7,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx1x9,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1x9,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx1x9,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1x9}},
};
mb_wchar_range_t mb_cm_wcv_EUC_JISX0213_PACKEDx2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2283,94U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKEDx2[] = {
{0x0,0x5D,0U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKEDx2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKEDx2[] = {
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx2x0,NULL,NULL,{NULL,6U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_EUC_JISX0213_PACKED[] = {
{0x0,0x3E,0U},
{0x0,0x5D,1U},
{0x0,0xBB,1U},
{0x0,0x119,1U},
{0x0,0x16C3,1U},
{0x0,0x2EF,1U},
{0x0,0x2283,2U},
{0x0,0x5D,1U},
{0x0,0x119,1U},
{0x0,0x5D,1U},
{0x0,0x177,1U},
{0x0,0x34D,1U},
};
size_t mb_cm_dectab_bv_EUC_JISX0213_PACKED[] = {
0U,
1U,
2U,
3U,
4U,
5U,
6U,
7U,
8U,
9U,
10U,
11U,
12U,
};
mb_char_node_t mb_cm_nodev_EUC_JISX0213_PACKED[] = {
{0x8E,0x8E,mb_cm_wcv_EUC_JISX0213_PACKEDx0,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx0,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx0,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx0}},
{0x8F,0x8F,mb_cm_wcv_EUC_JISX0213_PACKEDx1,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx1,10U,mb_cm_dectab_EUC_JISX0213_PACKEDx1,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx1}},
{0xA1,0xFE,mb_cm_wcv_EUC_JISX0213_PACKEDx2,NULL,NULL,{mb_cm_nodev_EUC_JISX0213_PACKEDx2,1U,mb_cm_dectab_EUC_JISX0213_PACKEDx2,mb_cm_dectab_bv_EUC_JISX0213_PACKEDx2}},
};
mb_char_map_t mb_cm_EUC_JISX0213_PACKED = {
mb_cm_nodev_EUC_JISX0213_PACKED,
3U,
mb_cm_dectab_EUC_JISX0213_PACKED,
mb_cm_dectab_bv_EUC_JISX0213_PACKED,
};
mb_wchar_range_t mb_wcv_EUC_JISX0213_PACKED[] = {
{0x20034E,0x20038C,63U},
{0x21C86E,0x21C8CB,94U},
{0x21C9E6,0x21CAA1,188U},
{0x21CB00,0x21CC19,282U},
{0x21CD92,0x21E455,5828U},
{0x21E7A4,0x21EA93,752U},
{0x2343BC,0x23663F,8836U},
{0x236640,0x23669D,94U},
{0x2366FC,0x236815,282U},
{0x2368D2,0x23692F,94U},
{0x236A4A,0x236BC1,376U},
{0x238286,0x2385D3,846U},
};
mb_wchar_range_t mb_decmap_tab_EUC_JISX0213_PACKED[] = {
{0x0,0x7F,0U},
{0x20034E,0x20038C,1U},
{0x21C86E,0x21C8CB,2U},
{0x21C9E6,0x21CAA1,3U},
{0x21CB00,0x21CC19,4U},
{0x21CD92,0x21E455,5U},
{0x21E7A4,0x21EA93,6U},
{0x2343BC,0x23663F,7U},
{0x236640,0x23669D,8U},
{0x2366FC,0x236815,9U},
{0x2368D2,0x23692F,10U},
{0x236A4A,0x236BC1,11U},
{0x238286,0x2385D3,12U},
};
mb_decoder_t mb_decmap_destv_EUC_JISX0213_PACKED[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x20034E,&mb_cm_EUC_JISX0213_PACKED,0U},
{NULL,NULL,0x21C86E,&mb_cm_EUC_JISX0213_PACKED,1U},
{NULL,NULL,0x21C9E6,&mb_cm_EUC_JISX0213_PACKED,2U},
{NULL,NULL,0x21CB00,&mb_cm_EUC_JISX0213_PACKED,3U},
{NULL,NULL,0x21CD92,&mb_cm_EUC_JISX0213_PACKED,4U},
{NULL,NULL,0x21E7A4,&mb_cm_EUC_JISX0213_PACKED,5U},
{NULL,NULL,0x2343BC,&mb_cm_EUC_JISX0213_PACKED,6U},
{NULL,NULL,0x236640,&mb_cm_EUC_JISX0213_PACKED,7U},
{NULL,NULL,0x2366FC,&mb_cm_EUC_JISX0213_PACKED,8U},
{NULL,NULL,0x2368D2,&mb_cm_EUC_JISX0213_PACKED,9U},
{NULL,NULL,0x236A4A,&mb_cm_EUC_JISX0213_PACKED,10U},
{NULL,NULL,0x238286,&mb_cm_EUC_JISX0213_PACKED,11U},
};
mb_decoder_map_t mb_decmap_EUC_JISX0213_PACKED = {
mb_decmap_tab_EUC_JISX0213_PACKED,
13U,
mb_decmap_destv_EUC_JISX0213_PACKED,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_EUC_JISX0213_PACKED(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_EUC_JISX0213_PACKED);
}
size_t
mb_conv_ws_to_EUC_JISX0213_PACKED(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_EUC_JISX0213_PACKED);
}
#endif
static const char *mb_namev_EUC_JISX0213_PACKED[] = {
"x-euc-jisx0213-packed",
};
static mb_conv_t mb_convv_EUC_JISX0213_PACKED[] = {
#ifdef USE_UCS
mb_conv_to_EUC_JISX0213_PACKED,
#else
#endif
NULL,
};
mb_ces_t mb_ces_EUC_JISX0213_PACKED = {
mb_namev_EUC_JISX0213_PACKED,
mb_flag_plus,0,
{mb_G0,mb_EUC_JISX0213_PACKED,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_EUC_JISX0213_PACKED,
&mb_decmap_EUC_JISX0213_PACKED,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_EUC_JISX0213_PACKED[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_EUC_JISX0213_PACKED,&mb_cm_nodev_EUC_JISX0213_PACKED[0U]},
{mb_wcv_EUC_JISX0213_PACKED,&mb_cm_nodev_EUC_JISX0213_PACKED[1U]},
{mb_wcv_EUC_JISX0213_PACKED,&mb_cm_nodev_EUC_JISX0213_PACKED[2U]}
};
short mb_encmap_iv_EUC_JISX0213_PACKED[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,5,6,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
-1,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,-1
};
mb_encoder_map_t mb_encmap_EUC_JISX0213_PACKED = {
mb_encmap_nodev_EUC_JISX0213_PACKED,
mb_encmap_iv_EUC_JISX0213_PACKED,
};
#endif
#ifdef USE_GBK
mb_wchar_range_t mb_cm_wcv_GBKx0x0[] = {
{0,0,0},
{0x0,0x3E,1U},
};
mb_wchar_range_t mb_cm_wcv_GBKx0x1[] = {
{0,0,0},
{0x3F,0xBD,1U},
};
mb_wchar_range_t mb_cm_wcv_GBKx1x0[] = {
{0,0,0},
{0x0,0x3E,1U},
};
mb_wchar_range_t mb_cm_wcv_GBKx1x1[] = {
{0,0,0},
{0x3F,0x5F,1U},
};
mb_wchar_range_t mb_cm_wcv_GBKx1x2[] = {
{0x0,0x5D,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBKx0[] = {
{0,0,0},
{0x0,0x17BF,190U},
};
mb_wchar_range_t mb_cm_dectab_GBKx0[] = {
{0x0,0x3E,0U},
{0x3F,0xBD,1U},
};
size_t mb_cm_dectab_bv_GBKx0[] = {
0U,
0U,
2U,
};
mb_char_node_t mb_cm_nodev_GBKx0[] = {
{0x40,0x7E,mb_cm_wcv_GBKx0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x80,0xFE,mb_cm_wcv_GBKx0x1,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBKx1[] = {
{0x0,0x2283,94U},
{0x17C0,0x3AFF,96U},
};
mb_wchar_range_t mb_cm_dectab_GBKx1[] = {
{0x0,0x5D,2U},
{0x0,0x3E,0U},
{0x3F,0x5F,1U},
};
size_t mb_cm_dectab_bv_GBKx1[] = {
0U,
1U,
3U,
};
mb_char_node_t mb_cm_nodev_GBKx1[] = {
{0x40,0x7E,mb_cm_wcv_GBKx1x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x80,0xA0,mb_cm_wcv_GBKx1x1,NULL,NULL,{NULL,1U,NULL,NULL}},
{0xA1,0xFE,mb_cm_wcv_GBKx1x2,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_GBK[] = {
{0x0,0x2283,1U},
{0x0,0x17BF,0U},
{0x17C0,0x3AFF,1U},
};
size_t mb_cm_dectab_bv_GBK[] = {
0U,
1U,
3U,
};
mb_char_node_t mb_cm_nodev_GBK[] = {
{0x81,0xA0,mb_cm_wcv_GBKx0,NULL,NULL,{mb_cm_nodev_GBKx0,2U,mb_cm_dectab_GBKx0,mb_cm_dectab_bv_GBKx0}},
{0xA1,0xFE,mb_cm_wcv_GBKx1,NULL,NULL,{mb_cm_nodev_GBKx1,3U,mb_cm_dectab_GBKx1,mb_cm_dectab_bv_GBKx1}},
};
mb_char_map_t mb_cm_GBK = {
mb_cm_nodev_GBK,
2U,
mb_cm_dectab_GBK,
mb_cm_dectab_bv_GBK,
};
mb_wchar_range_t mb_wcv_GBK[] = {
{0x216084,0x218307,8836U},
{0x368000,0x36BAFF,15104U},
};
mb_wchar_range_t mb_decmap_tab_GBK[] = {
{0x0,0x7F,0U},
{0x216084,0x218307,1U},
{0x368000,0x36BAFF,2U},
};
mb_decoder_t mb_decmap_destv_GBK[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x216084,&mb_cm_GBK,0U},
{NULL,NULL,0x368000,&mb_cm_GBK,1U},
};
mb_decoder_map_t mb_decmap_GBK = {
mb_decmap_tab_GBK,
3U,
mb_decmap_destv_GBK,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_GBK(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_GBK);
}
size_t
mb_conv_ws_to_GBK(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_GBK);
}
#endif
static const char *mb_namev_GBK[] = {
"x-gbk",
};
static mb_conv_t mb_convv_GBK[] = {
#ifdef USE_UCS
mb_conv_to_GBK,
#else
#endif
NULL,
};
mb_ces_t mb_ces_GBK = {
mb_namev_GBK,
mb_flag_plus,0,
{mb_G0,mb_GBK,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_GBK,
&mb_decmap_GBK,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_GBK[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_GBK,&mb_cm_nodev_GBK[0U]},
{mb_wcv_GBK,&mb_cm_nodev_GBK[1U]}
};
short mb_encmap_iv_GBK[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,-1
};
mb_encoder_map_t mb_encmap_GBK = {
mb_encmap_nodev_GBK,
mb_encmap_iv_GBK,
};
#endif
#ifdef USE_GBK2K
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x0x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x1x0x0[] = {
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x0x0[] = {
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x1x0[] = {
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x1x1[] = {
{0,0,0},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x2x0[] = {
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x0x0[] = {
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x1x0[] = {
{0,0,0},
{0x0,0x8,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x5x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x1x1[] = {
{0,0,0},
{0,0,0},
{0x0,0x6,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x2x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x3x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x6,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x6,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x6x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x6x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x6x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x6,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x2x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx4x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx5x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx6x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5,1U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x2x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx8x0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x333,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x0x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x0x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x334,0x4EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x0x1[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x0x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x0x1[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x0x1x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x1x0[] = {
{0x0,0x4EB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x1x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x1x0[] = {
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x1x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x1x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x0[] = {
{0x0,0x167,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x2x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x2x0[] = {
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x2x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x2x0x0,NULL,NULL,{NULL,0U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x1[] = {
{0x168,0x169,2U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x7,8U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x2x1[] = {
{0x0,0x1,0U},
{0x0,0x7,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x2x1[] = {
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x2x1[] = {
{0x30,0x31,mb_cm_wcv_GBK2Kx0x2x1x0,NULL,NULL,{NULL,0U,NULL,NULL}},
{0x32,0x39,mb_cm_wcv_GBK2Kx0x2x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x8,0x381,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x2x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x2x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x2x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x2x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x185,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x3x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x3x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x3x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x3x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x1[] = {
{0,0,0},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x186,0x18E,9U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x3x1[] = {
{0x0,0x0,1U},
{0x0,0x8,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x3x1[] = {
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x3x1[] = {
{0x30,0x38,mb_cm_wcv_GBK2Kx0x3x1x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x39,0x39,mb_cm_wcv_GBK2Kx0x3x1x1,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3x2[] = {
{0,0,0},
{0x1,0x35C,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x3x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x3x2[] = {
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x3x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x3x2x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x0[] = {
{0,0,0},
{0x0,0x4D7,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x4x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x4x0[] = {
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x4x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x4x0x0,NULL,NULL,{NULL,1U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x1[] = {
{0,0,0},
{0x4D8,0x4E0,9U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x4x1[] = {
{0x0,0x8,0U},
{0x0,0x0,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x4x1[] = {
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x4x1[] = {
{0x30,0x38,mb_cm_wcv_GBK2Kx0x4x1x0,NULL,NULL,{NULL,1U,NULL,NULL}},
{0x39,0x39,mb_cm_wcv_GBK2Kx0x4x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1,0xA,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x4x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x4x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x4x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x4x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x5x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x5x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x5x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx0x5x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x171,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x0x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x1[] = {
{0,0,0},
{0,0,0},
{0x0,0x6,7U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x172,0x174,3U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x0x1[] = {
{0x0,0x6,1U},
{0x0,0x2,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x0x1[] = {
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x0x1[] = {
{0x30,0x32,mb_cm_wcv_GBK2Kx1x0x1x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x33,0x39,mb_cm_wcv_GBK2Kx1x0x1x1,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x2[] = {
{0,0,0},
{0,0,0},
{0x7,0x2F4,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x0x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x0x2[] = {
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x0x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x0x2x0,NULL,NULL,{NULL,2U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x3[] = {
{0,0,0},
{0,0,0},
{0x2F5,0x2FC,8U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x175,0x176,2U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x0x3[] = {
{0x0,0x7,0U},
{0x0,0x1,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x0x3[] = {
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x0x3[] = {
{0x30,0x37,mb_cm_wcv_GBK2Kx1x0x3x0,NULL,NULL,{NULL,2U,NULL,NULL}},
{0x38,0x39,mb_cm_wcv_GBK2Kx1x0x3x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x177,0x1EE,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x0x4[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x0x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x0x4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x0x4x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x33D,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x1x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x1x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x1x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x1x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,2U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x33E,0x345,8U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x1x1[] = {
{0x0,0x1,1U},
{0x0,0x7,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x1x1[] = {
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x1x1[] = {
{0x30,0x37,mb_cm_wcv_GBK2Kx1x1x1x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x38,0x39,mb_cm_wcv_GBK2Kx1x1x1x1,NULL,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x2,0x1A5,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x1x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x1x2[] = {
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x1x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x1x2x0,NULL,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1CB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x0[] = {
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x2x0x0,NULL,NULL,{NULL,3U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x1CC,0x1CE,3U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x6,7U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x1[] = {
{0x0,0x2,0U},
{0x0,0x6,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x1[] = {
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x1[] = {
{0x30,0x32,mb_cm_wcv_GBK2Kx1x2x1x0,NULL,NULL,{NULL,3U,NULL,NULL}},
{0x33,0x39,mb_cm_wcv_GBK2Kx1x2x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x7,0x100,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x2x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2,3U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x101,0x107,7U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x3[] = {
{0x0,0x2,1U},
{0x0,0x6,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x3[] = {
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x3[] = {
{0x30,0x36,mb_cm_wcv_GBK2Kx1x2x3x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x37,0x39,mb_cm_wcv_GBK2Kx1x2x3x1,NULL,NULL,{NULL,4U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3,0x1CE,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x4[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x4[] = {
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x2x4x0,NULL,NULL,{NULL,4U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1CF,0x1D6,8U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x108,0x109,2U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x5[] = {
{0x0,0x7,0U},
{0x0,0x1,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x5[] = {
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x5[] = {
{0x30,0x37,mb_cm_wcv_GBK2Kx1x2x5x0,NULL,NULL,{NULL,4U,NULL,NULL}},
{0x38,0x39,mb_cm_wcv_GBK2Kx1x2x5x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x10A,0x145,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2x6[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2x6[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2x6[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x2x6x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x153,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x3x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x154,0x15C,9U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x1[] = {
{0x0,0x0,1U},
{0x0,0x8,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x1[] = {
{0x30,0x38,mb_cm_wcv_GBK2Kx1x3x1x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x39,0x39,mb_cm_wcv_GBK2Kx1x3x1x1,NULL,NULL,{NULL,5U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1,0x172,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x3x2x0,NULL,NULL,{NULL,5U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x173,0x174,2U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x15D,0x164,8U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x3[] = {
{0x0,0x1,0U},
{0x0,0x7,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x3[] = {
{0x30,0x31,mb_cm_wcv_GBK2Kx1x3x3x0,NULL,NULL,{NULL,5U,NULL,NULL}},
{0x32,0x39,mb_cm_wcv_GBK2Kx1x3x3x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x165,0x290,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x4[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x3x4x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,2U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x291,0x298,8U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x5[] = {
{0x0,0x1,1U},
{0x0,0x7,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x5[] = {
{0x30,0x37,mb_cm_wcv_GBK2Kx1x3x5x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x38,0x39,mb_cm_wcv_GBK2Kx1x3x5x1,NULL,NULL,{NULL,6U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x2,0xDD,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3x6[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3x6[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3x6[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x3x6x0,NULL,NULL,{NULL,6U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xD1,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x4x0x0,NULL,NULL,{NULL,6U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xD2,0xDA,9U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x1[] = {
{0x0,0x8,0U},
{0x0,0x0,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x1[] = {
{0x30,0x38,mb_cm_wcv_GBK2Kx1x4x1x0,NULL,NULL,{NULL,6U,NULL,NULL}},
{0x39,0x39,mb_cm_wcv_GBK2Kx1x4x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1,0x64,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x4x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8,9U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x65,0x65,1U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x3[] = {
{0x0,0x8,1U},
{0x0,0x0,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x3[] = {
{0x30,0x30,mb_cm_wcv_GBK2Kx1x4x3x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x31,0x39,mb_cm_wcv_GBK2Kx1x4x3x1,NULL,NULL,{NULL,7U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x9,0x2BA,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x4[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x4x4x0,NULL,NULL,{NULL,7U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x2BB,0x2BE,4U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x66,0x6B,6U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x5[] = {
{0x0,0x3,0U},
{0x0,0x5,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x5[] = {
{0x30,0x33,mb_cm_wcv_GBK2Kx1x4x5x0,NULL,NULL,{NULL,7U,NULL,NULL}},
{0x34,0x39,mb_cm_wcv_GBK2Kx1x4x5x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x6C,0x151,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4x6[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4x6[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4x6[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x4x6x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x8B,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x5x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x5x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x5x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x5x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x6,7U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x8C,0x8E,3U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x5x1[] = {
{0x0,0x6,1U},
{0x0,0x2,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x5x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x5x1[] = {
{0x30,0x32,mb_cm_wcv_GBK2Kx1x5x1x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x33,0x39,mb_cm_wcv_GBK2Kx1x5x1x1,NULL,NULL,{NULL,8U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x7,0x45C,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x5x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x5x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x5x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx1x5x2x0,NULL,NULL,{NULL,8U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx2x0x0x0,NULL,NULL,{NULL,8U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2BB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x1x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x1x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x1x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx2x1x0x0,NULL,NULL,{NULL,8U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x2BC,0x2C4,9U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x0,1U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x1x1[] = {
{0x0,0x8,0U},
{0x0,0x0,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x1x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x1x1[] = {
{0x30,0x38,mb_cm_wcv_GBK2Kx2x1x1x0,NULL,NULL,{NULL,8U,NULL,NULL}},
{0x39,0x39,mb_cm_wcv_GBK2Kx2x1x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1,0x50,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x1x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x1x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x1x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx2x1x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1D5,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x1x3[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x1x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x1x3[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx2x1x3x0,NULL,NULL,{NULL,9U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x27,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x0x0x0,NULL,NULL,{NULL,9U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x28,0x2C,5U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4,5U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x0x1[] = {
{0x0,0x4,0U},
{0x0,0x4,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x0x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x0x1[] = {
{0x30,0x34,mb_cm_wcv_GBK2Kx3x0x1x0,NULL,NULL,{NULL,9U,NULL,NULL}},
{0x35,0x39,mb_cm_wcv_GBK2Kx3x0x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x5,0xE0,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x0x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x0x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x0x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x0x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,2U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xE1,0xE8,8U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x0x3[] = {
{0x0,0x1,1U},
{0x0,0x7,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x0x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x0x3[] = {
{0x30,0x37,mb_cm_wcv_GBK2Kx3x0x3x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x38,0x39,mb_cm_wcv_GBK2Kx3x0x3x1,NULL,NULL,{NULL,10U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x2,0x3D5,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x0x4[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x0x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x0x4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x0x4x0,NULL,NULL,{NULL,10U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x27,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x1x0x0,NULL,NULL,{NULL,10U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x28,0x2F,8U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1,2U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1x1[] = {
{0x0,0x7,0U},
{0x0,0x1,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1x1[] = {
{0x30,0x37,mb_cm_wcv_GBK2Kx3x1x1x0,NULL,NULL,{NULL,10U,NULL,NULL}},
{0x38,0x39,mb_cm_wcv_GBK2Kx3x1x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x2,0x119,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x1x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5,6U},
{0,0,0},
{0,0,0},
{0,0,0},
{0x11A,0x11D,4U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1x3[] = {
{0x0,0x5,1U},
{0x0,0x3,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1x3[] = {
{0x30,0x33,mb_cm_wcv_GBK2Kx3x1x3x0,NULL,NULL,{NULL,15U,NULL,NULL}},
{0x34,0x39,mb_cm_wcv_GBK2Kx3x1x3x1,NULL,NULL,{NULL,11U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x6,0x19,10U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1x4[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1x4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x1x4x0,NULL,NULL,{NULL,11U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x11E,0x4A1,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1x5[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1x5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1x5[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x1x5x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x2x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x2x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x2x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx3x2x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx4x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx4x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx4x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx4x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx4x0x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx5x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx5x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx5x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx5x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx5x0x0x0,NULL,NULL,{NULL,12U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx6x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx6x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx6x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx6x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx6x0x0x0,NULL,NULL,{NULL,12U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx7x0x0x0,NULL,NULL,{NULL,12U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xF9,10U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x1x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x1x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x1x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx7x1x0x0,NULL,NULL,{NULL,12U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xFA,0xFF,6U},
{0,0,0},
{0,0,0},
{0x0,0x3,4U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x1x1[] = {
{0x0,0x5,0U},
{0x0,0x3,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x1x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x1x1[] = {
{0x30,0x35,mb_cm_wcv_GBK2Kx7x1x1x0,NULL,NULL,{NULL,12U,NULL,NULL}},
{0x36,0x39,mb_cm_wcv_GBK2Kx7x1x1x1,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x4,0x3EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x1x2[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x1x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x1x2[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx7x1x2x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x2x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x2x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x2x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx7x2x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx8x0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,10U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx8x0x0[] = {
{0x0,0x9,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx8x0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx8x0x0[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx8x0x0x0,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x4EB,1260U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x0[] = {
{0x0,0x333,0U},
{0x334,0x4EB,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x0[] = {
{0x81,0xD2,mb_cm_wcv_GBK2Kx0x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x0x0,1U,mb_cm_dectab_GBK2Kx0x0x0,mb_cm_dectab_bv_GBK2Kx0x0x0}},
{0xD3,0xFE,mb_cm_wcv_GBK2Kx0x0x1,NULL,NULL,{mb_cm_nodev_GBK2Kx0x0x1,1U,mb_cm_dectab_GBK2Kx0x0x1,mb_cm_dectab_bv_GBK2Kx0x0x1}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x1[] = {
{0x0,0x189B,1260U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x1[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x1[] = {
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x1[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx0x1x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x1x0,1U,mb_cm_dectab_GBK2Kx0x1x0,mb_cm_dectab_bv_GBK2Kx0x1x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x2[] = {
{0x189C,0x1A05,362U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x4EC,0x86D,898U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x2[] = {
{0x0,0x167,0U},
{0x168,0x169,1U},
{0x0,0x7,1U},
{0x8,0x381,2U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x2[] = {
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x2[] = {
{0x81,0xA4,mb_cm_wcv_GBK2Kx0x2x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x2x0,1U,mb_cm_dectab_GBK2Kx0x2x0,mb_cm_dectab_bv_GBK2Kx0x2x0}},
{0xA5,0xA5,mb_cm_wcv_GBK2Kx0x2x1,NULL,NULL,{mb_cm_nodev_GBK2Kx0x2x1,2U,mb_cm_dectab_GBK2Kx0x2x1,mb_cm_dectab_bv_GBK2Kx0x2x1}},
{0xA6,0xFE,mb_cm_wcv_GBK2Kx0x2x2,NULL,NULL,{mb_cm_nodev_GBK2Kx0x2x2,1U,mb_cm_dectab_GBK2Kx0x2x2,mb_cm_dectab_bv_GBK2Kx0x2x2}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x3[] = {
{0,0,0},
{0x0,0x35C,861U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x86E,0x9FC,399U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x3[] = {
{0x0,0x0,1U},
{0x1,0x35C,2U},
{0x0,0x185,0U},
{0x186,0x18E,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x3[] = {
0U,
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x3[] = {
{0x81,0xA7,mb_cm_wcv_GBK2Kx0x3x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x3x0,1U,mb_cm_dectab_GBK2Kx0x3x0,mb_cm_dectab_bv_GBK2Kx0x3x0}},
{0xA8,0xA8,mb_cm_wcv_GBK2Kx0x3x1,NULL,NULL,{mb_cm_nodev_GBK2Kx0x3x1,2U,mb_cm_dectab_GBK2Kx0x3x1,mb_cm_dectab_bv_GBK2Kx0x3x1}},
{0xA9,0xFE,mb_cm_wcv_GBK2Kx0x3x2,NULL,NULL,{mb_cm_nodev_GBK2Kx0x3x2,1U,mb_cm_dectab_GBK2Kx0x3x2,mb_cm_dectab_bv_GBK2Kx0x3x2}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x4[] = {
{0,0,0},
{0x35D,0x83D,1249U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x9FD,0xA07,11U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x4[] = {
{0x0,0x4D7,0U},
{0x4D8,0x4E0,1U},
{0x0,0x0,1U},
{0x1,0xA,2U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x4[] = {
0U,
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x4[] = {
{0x81,0xFC,mb_cm_wcv_GBK2Kx0x4x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x4x0,1U,mb_cm_dectab_GBK2Kx0x4x0,mb_cm_dectab_bv_GBK2Kx0x4x0}},
{0xFD,0xFD,mb_cm_wcv_GBK2Kx0x4x1,NULL,NULL,{mb_cm_nodev_GBK2Kx0x4x1,2U,mb_cm_dectab_GBK2Kx0x4x1,mb_cm_dectab_bv_GBK2Kx0x4x1}},
{0xFE,0xFE,mb_cm_wcv_GBK2Kx0x4x2,NULL,NULL,{mb_cm_nodev_GBK2Kx0x4x2,1U,mb_cm_dectab_GBK2Kx0x4x2,mb_cm_dectab_bv_GBK2Kx0x4x2}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xA08,0xEF3,1260U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0x5[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx0x5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0x5[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx0x5x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x5x0,1U,mb_cm_dectab_GBK2Kx0x5x0,mb_cm_dectab_bv_GBK2Kx0x5x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xEF4,0xFB2,1U},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x0[] = {
{0,0,0},
{0,0,0},
{0x0,0x2FC,765U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1EE,495U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x0[] = {
{0x0,0x6,1U},
{0x7,0x2F4,2U},
{0x2F5,0x2FC,3U},
{0x0,0x171,0U},
{0x172,0x174,1U},
{0x175,0x176,3U},
{0x177,0x1EE,4U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x0[] = {
0U,
0U,
0U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
3U,
7U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x0[] = {
{0x81,0xA5,mb_cm_wcv_GBK2Kx1x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x0x0,1U,mb_cm_dectab_GBK2Kx1x0x0,mb_cm_dectab_bv_GBK2Kx1x0x0}},
{0xA6,0xA6,mb_cm_wcv_GBK2Kx1x0x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x0x1,2U,mb_cm_dectab_GBK2Kx1x0x1,mb_cm_dectab_bv_GBK2Kx1x0x1}},
{0xA7,0xF1,mb_cm_wcv_GBK2Kx1x0x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x0x2,1U,mb_cm_dectab_GBK2Kx1x0x2,mb_cm_dectab_bv_GBK2Kx1x0x2}},
{0xF2,0xF2,mb_cm_wcv_GBK2Kx1x0x3,NULL,NULL,{mb_cm_nodev_GBK2Kx1x0x3,2U,mb_cm_dectab_GBK2Kx1x0x3,mb_cm_dectab_bv_GBK2Kx1x0x3}},
{0xF3,0xFE,mb_cm_wcv_GBK2Kx1x0x4,NULL,NULL,{mb_cm_nodev_GBK2Kx1x0x4,1U,mb_cm_dectab_GBK2Kx1x0x4,mb_cm_dectab_bv_GBK2Kx1x0x4}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1A5,422U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1EF,0x534,838U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x1[] = {
{0x0,0x1,1U},
{0x2,0x1A5,2U},
{0x0,0x33D,0U},
{0x33E,0x345,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x1[] = {
0U,
0U,
0U,
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x1[] = {
{0x81,0xD3,mb_cm_wcv_GBK2Kx1x1x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x1x0,1U,mb_cm_dectab_GBK2Kx1x1x0,mb_cm_dectab_bv_GBK2Kx1x1x0}},
{0xD4,0xD4,mb_cm_wcv_GBK2Kx1x1x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x1x1,2U,mb_cm_dectab_GBK2Kx1x1x1,mb_cm_dectab_bv_GBK2Kx1x1x1}},
{0xD5,0xFE,mb_cm_wcv_GBK2Kx1x1x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x1x2,1U,mb_cm_dectab_GBK2Kx1x1x2,mb_cm_dectab_bv_GBK2Kx1x1x2}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0x1A6,0x374,463U},
{0x0,0x1D6,471U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x535,0x67A,326U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x2[] = {
{0x0,0x1CB,0U},
{0x1CC,0x1CE,1U},
{0x0,0x2,3U},
{0x3,0x1CE,4U},
{0x1CF,0x1D6,5U},
{0x0,0x6,1U},
{0x7,0x100,2U},
{0x101,0x107,3U},
{0x108,0x109,5U},
{0x10A,0x145,6U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x2[] = {
0U,
0U,
0U,
0U,
2U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
10U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x2[] = {
{0x81,0xAE,mb_cm_wcv_GBK2Kx1x2x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x0,1U,mb_cm_dectab_GBK2Kx1x2x0,mb_cm_dectab_bv_GBK2Kx1x2x0}},
{0xAF,0xAF,mb_cm_wcv_GBK2Kx1x2x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x1,2U,mb_cm_dectab_GBK2Kx1x2x1,mb_cm_dectab_bv_GBK2Kx1x2x1}},
{0xB0,0xC8,mb_cm_wcv_GBK2Kx1x2x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x2,1U,mb_cm_dectab_GBK2Kx1x2x2,mb_cm_dectab_bv_GBK2Kx1x2x2}},
{0xC9,0xC9,mb_cm_wcv_GBK2Kx1x2x3,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x3,2U,mb_cm_dectab_GBK2Kx1x2x3,mb_cm_dectab_bv_GBK2Kx1x2x3}},
{0xCA,0xF7,mb_cm_wcv_GBK2Kx1x2x4,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x4,1U,mb_cm_dectab_GBK2Kx1x2x4,mb_cm_dectab_bv_GBK2Kx1x2x4}},
{0xF8,0xF8,mb_cm_wcv_GBK2Kx1x2x5,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x5,2U,mb_cm_dectab_GBK2Kx1x2x5,mb_cm_dectab_bv_GBK2Kx1x2x5}},
{0xF9,0xFE,mb_cm_wcv_GBK2Kx1x2x6,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2x6,1U,mb_cm_dectab_GBK2Kx1x2x6,mb_cm_dectab_bv_GBK2Kx1x2x6}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x174,373U},
{0x0,0xDD,222U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x67B,0x913,665U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x3[] = {
{0x0,0x0,1U},
{0x1,0x172,2U},
{0x173,0x174,3U},
{0x0,0x1,5U},
{0x2,0xDD,6U},
{0x0,0x153,0U},
{0x154,0x15C,1U},
{0x15D,0x164,3U},
{0x165,0x290,4U},
{0x291,0x298,5U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
3U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
10U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x3[] = {
{0x81,0xA2,mb_cm_wcv_GBK2Kx1x3x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x0,1U,mb_cm_dectab_GBK2Kx1x3x0,mb_cm_dectab_bv_GBK2Kx1x3x0}},
{0xA3,0xA3,mb_cm_wcv_GBK2Kx1x3x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x1,2U,mb_cm_dectab_GBK2Kx1x3x1,mb_cm_dectab_bv_GBK2Kx1x3x1}},
{0xA4,0xC8,mb_cm_wcv_GBK2Kx1x3x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x2,1U,mb_cm_dectab_GBK2Kx1x3x2,mb_cm_dectab_bv_GBK2Kx1x3x2}},
{0xC9,0xC9,mb_cm_wcv_GBK2Kx1x3x3,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x3,2U,mb_cm_dectab_GBK2Kx1x3x3,mb_cm_dectab_bv_GBK2Kx1x3x3}},
{0xCA,0xE7,mb_cm_wcv_GBK2Kx1x3x4,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x4,1U,mb_cm_dectab_GBK2Kx1x3x4,mb_cm_dectab_bv_GBK2Kx1x3x4}},
{0xE8,0xE8,mb_cm_wcv_GBK2Kx1x3x5,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x5,2U,mb_cm_dectab_GBK2Kx1x3x5,mb_cm_dectab_bv_GBK2Kx1x3x5}},
{0xE9,0xFE,mb_cm_wcv_GBK2Kx1x3x6,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3x6,1U,mb_cm_dectab_GBK2Kx1x3x6,mb_cm_dectab_bv_GBK2Kx1x3x6}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xDE,0x1B8,219U},
{0x0,0x2BE,703U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x914,0xA65,338U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x4[] = {
{0x0,0xD1,0U},
{0xD2,0xDA,1U},
{0x0,0x8,3U},
{0x9,0x2BA,4U},
{0x2BB,0x2BE,5U},
{0x0,0x0,1U},
{0x1,0x64,2U},
{0x65,0x65,3U},
{0x66,0x6B,5U},
{0x6C,0x151,6U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
5U,
10U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x4[] = {
{0x81,0x95,mb_cm_wcv_GBK2Kx1x4x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x0,1U,mb_cm_dectab_GBK2Kx1x4x0,mb_cm_dectab_bv_GBK2Kx1x4x0}},
{0x96,0x96,mb_cm_wcv_GBK2Kx1x4x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x1,2U,mb_cm_dectab_GBK2Kx1x4x1,mb_cm_dectab_bv_GBK2Kx1x4x1}},
{0x97,0xA0,mb_cm_wcv_GBK2Kx1x4x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x2,1U,mb_cm_dectab_GBK2Kx1x4x2,mb_cm_dectab_bv_GBK2Kx1x4x2}},
{0xA1,0xA1,mb_cm_wcv_GBK2Kx1x4x3,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x3,2U,mb_cm_dectab_GBK2Kx1x4x3,mb_cm_dectab_bv_GBK2Kx1x4x3}},
{0xA2,0xE6,mb_cm_wcv_GBK2Kx1x4x4,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x4,1U,mb_cm_dectab_GBK2Kx1x4x4,mb_cm_dectab_bv_GBK2Kx1x4x4}},
{0xE7,0xE7,mb_cm_wcv_GBK2Kx1x4x5,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x5,2U,mb_cm_dectab_GBK2Kx1x4x5,mb_cm_dectab_bv_GBK2Kx1x4x5}},
{0xE8,0xFE,mb_cm_wcv_GBK2Kx1x4x6,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4x6,1U,mb_cm_dectab_GBK2Kx1x4x6,mb_cm_dectab_bv_GBK2Kx1x4x6}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x45C,1117U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xA66,0xAF4,143U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1x5[] = {
{0x0,0x6,1U},
{0x7,0x45C,2U},
{0x0,0x8B,0U},
{0x8C,0x8E,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx1x5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
2U,
2U,
2U,
2U,
2U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1x5[] = {
{0x81,0x8E,mb_cm_wcv_GBK2Kx1x5x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x5x0,1U,mb_cm_dectab_GBK2Kx1x5x0,mb_cm_dectab_bv_GBK2Kx1x5x0}},
{0x8F,0x8F,mb_cm_wcv_GBK2Kx1x5x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x5x1,2U,mb_cm_dectab_GBK2Kx1x5x1,mb_cm_dectab_bv_GBK2Kx1x5x1}},
{0x90,0xFE,mb_cm_wcv_GBK2Kx1x5x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x5x2,1U,mb_cm_dectab_GBK2Kx1x5x2,mb_cm_dectab_bv_GBK2Kx1x5x2}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1x6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBE,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x1D87,1260U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x0[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x0[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx2x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx2x0x0,1U,mb_cm_dectab_GBK2Kx2x0x0,mb_cm_dectab_bv_GBK2Kx2x0x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1D88,0x204C,709U},
{0x0,0x1D5,470U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x50,81U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2x1[] = {
{0x0,0x2BB,0U},
{0x2BC,0x2C4,1U},
{0x0,0x1D5,3U},
{0x0,0x0,1U},
{0x1,0x50,2U},
};
size_t mb_cm_dectab_bv_GBK2Kx2x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
3U,
3U,
3U,
3U,
3U,
3U,
5U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2x1[] = {
{0x81,0xC6,mb_cm_wcv_GBK2Kx2x1x0,NULL,NULL,{mb_cm_nodev_GBK2Kx2x1x0,1U,mb_cm_dectab_GBK2Kx2x1x0,mb_cm_dectab_bv_GBK2Kx2x1x0}},
{0xC7,0xC7,mb_cm_wcv_GBK2Kx2x1x1,NULL,NULL,{mb_cm_nodev_GBK2Kx2x1x1,2U,mb_cm_dectab_GBK2Kx2x1x1,mb_cm_dectab_bv_GBK2Kx2x1x1}},
{0xC8,0xCF,mb_cm_wcv_GBK2Kx2x1x2,NULL,NULL,{mb_cm_nodev_GBK2Kx2x1x2,1U,mb_cm_dectab_GBK2Kx2x1x2,mb_cm_dectab_bv_GBK2Kx2x1x2}},
{0xD0,0xFE,mb_cm_wcv_GBK2Kx2x1x3,NULL,NULL,{mb_cm_nodev_GBK2Kx2x1x3,1U,mb_cm_dectab_GBK2Kx2x1x3,mb_cm_dectab_bv_GBK2Kx2x1x3}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBE,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x2C,45U},
{0x0,0x3D5,982U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xE8,233U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x0[] = {
{0x0,0x27,0U},
{0x28,0x2C,1U},
{0x0,0x1,3U},
{0x2,0x3D5,4U},
{0x0,0x4,1U},
{0x5,0xE0,2U},
{0xE1,0xE8,3U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
4U,
4U,
4U,
4U,
4U,
7U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x0[] = {
{0x81,0x84,mb_cm_wcv_GBK2Kx3x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx3x0x0,1U,mb_cm_dectab_GBK2Kx3x0x0,mb_cm_dectab_bv_GBK2Kx3x0x0}},
{0x85,0x85,mb_cm_wcv_GBK2Kx3x0x1,NULL,NULL,{mb_cm_nodev_GBK2Kx3x0x1,2U,mb_cm_dectab_GBK2Kx3x0x1,mb_cm_dectab_bv_GBK2Kx3x0x1}},
{0x86,0x9B,mb_cm_wcv_GBK2Kx3x0x2,NULL,NULL,{mb_cm_nodev_GBK2Kx3x0x2,1U,mb_cm_dectab_GBK2Kx3x0x2,mb_cm_dectab_bv_GBK2Kx3x0x2}},
{0x9C,0x9C,mb_cm_wcv_GBK2Kx3x0x3,NULL,NULL,{mb_cm_nodev_GBK2Kx3x0x3,2U,mb_cm_dectab_GBK2Kx3x0x3,mb_cm_dectab_bv_GBK2Kx3x0x3}},
{0x9D,0xFE,mb_cm_wcv_GBK2Kx3x0x4,NULL,NULL,{mb_cm_nodev_GBK2Kx3x0x4,1U,mb_cm_dectab_GBK2Kx3x0x4,mb_cm_dectab_bv_GBK2Kx3x0x4}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3D6,0x405,48U},
{0x0,0x19,26U},
{0,0,0},
{0,0,0},
{0,0,0},
{0xE9,0x58A,1186U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x1[] = {
{0x0,0x27,0U},
{0x28,0x2F,1U},
{0x0,0x5,3U},
{0x6,0x19,4U},
{0x0,0x1,1U},
{0x2,0x119,2U},
{0x11A,0x11D,3U},
{0x11E,0x4A1,5U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
4U,
4U,
4U,
4U,
8U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x1[] = {
{0x81,0x84,mb_cm_wcv_GBK2Kx3x1x0,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1x0,1U,mb_cm_dectab_GBK2Kx3x1x0,mb_cm_dectab_bv_GBK2Kx3x1x0}},
{0x85,0x85,mb_cm_wcv_GBK2Kx3x1x1,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1x1,2U,mb_cm_dectab_GBK2Kx3x1x1,mb_cm_dectab_bv_GBK2Kx3x1x1}},
{0x86,0xA1,mb_cm_wcv_GBK2Kx3x1x2,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1x2,1U,mb_cm_dectab_GBK2Kx3x1x2,mb_cm_dectab_bv_GBK2Kx3x1x2}},
{0xA2,0xA2,mb_cm_wcv_GBK2Kx3x1x3,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1x3,2U,mb_cm_dectab_GBK2Kx3x1x3,mb_cm_dectab_bv_GBK2Kx3x1x3}},
{0xA3,0xA4,mb_cm_wcv_GBK2Kx3x1x4,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1x4,1U,mb_cm_dectab_GBK2Kx3x1x4,mb_cm_dectab_bv_GBK2Kx3x1x4}},
{0xA5,0xFE,mb_cm_wcv_GBK2Kx3x1x5,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1x5,1U,mb_cm_dectab_GBK2Kx3x1x5,mb_cm_dectab_bv_GBK2Kx3x1x5}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x58B,0x2CEA,1260U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3x2[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx3x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3x2[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx3x2x0,NULL,NULL,{mb_cm_nodev_GBK2Kx3x2x0,1U,mb_cm_dectab_GBK2Kx3x2x0,mb_cm_dectab_bv_GBK2Kx3x2x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBE,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx4x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3137,1260U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx4x0[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx4x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx4x0[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx4x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx4x0x0,1U,mb_cm_dectab_GBK2Kx4x0x0,mb_cm_dectab_bv_GBK2Kx4x0x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx4x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBE,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx5x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3137,1260U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx5x0[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx5x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx5x0[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx5x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx5x0x0,1U,mb_cm_dectab_GBK2Kx5x0x0,mb_cm_dectab_bv_GBK2Kx5x0x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx5x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBE,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx6x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3137,1260U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx6x0[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx6x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx6x0[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx6x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx6x0x0,1U,mb_cm_dectab_GBK2Kx6x0x0,mb_cm_dectab_bv_GBK2Kx6x0x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx6x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x60,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx6x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x9D7,1260U},
{0,0,0},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x0[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
1U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x0[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx7x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx7x0x0,1U,mb_cm_dectab_GBK2Kx7x0x0,mb_cm_dectab_bv_GBK2Kx7x0x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x9D8,0xAD7,256U},
{0,0,0},
{0,0,0},
{0x0,0x3EB,1004U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x1[] = {
{0x0,0xF9,0U},
{0xFA,0xFF,1U},
{0x0,0x3,1U},
{0x4,0x3EB,2U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x1[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
2U,
2U,
4U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x1[] = {
{0x81,0x99,mb_cm_wcv_GBK2Kx7x1x0,NULL,NULL,{mb_cm_nodev_GBK2Kx7x1x0,1U,mb_cm_dectab_GBK2Kx7x1x0,mb_cm_dectab_bv_GBK2Kx7x1x0}},
{0x9A,0x9A,mb_cm_wcv_GBK2Kx7x1x1,NULL,NULL,{mb_cm_nodev_GBK2Kx7x1x1,2U,mb_cm_dectab_GBK2Kx7x1x1,mb_cm_dectab_bv_GBK2Kx7x1x1}},
{0x9B,0xFE,mb_cm_wcv_GBK2Kx7x1x2,NULL,NULL,{mb_cm_nodev_GBK2Kx7x1x2,1U,mb_cm_dectab_GBK2Kx7x1x2,mb_cm_dectab_bv_GBK2Kx7x1x2}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x3EC,0x265F,1260U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7x2[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx7x2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7x2[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx7x2x0,NULL,NULL,{mb_cm_nodev_GBK2Kx7x2x0,1U,mb_cm_dectab_GBK2Kx7x2x0,mb_cm_dectab_bv_GBK2Kx7x2x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x60,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7x4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx8x0[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x3137,1260U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx8x0[] = {
{0x0,0x4EB,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx8x0[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx8x0[] = {
{0x81,0xFE,mb_cm_wcv_GBK2Kx8x0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx8x0x0,1U,mb_cm_dectab_GBK2Kx8x0x0,mb_cm_dectab_bv_GBK2Kx8x0x0}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx8x1[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x60,1U},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx8x2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x5D,1U},
{0,0,0},
{0,0,0},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx0[] = {
{0x0,0x1A05,6662U},
{0x0,0x83D,2110U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xFB2,4019U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx0[] = {
{0x0,0x189B,1U},
{0x189C,0x1A05,2U},
{0x0,0x35C,3U},
{0x35D,0x83D,4U},
{0x0,0x4EB,0U},
{0x4EC,0x86D,2U},
{0x86E,0x9FC,3U},
{0x9FD,0xA07,4U},
{0xA08,0xEF3,5U},
{0xEF4,0xFB2,6U},
};
size_t mb_cm_dectab_bv_GBK2Kx0[] = {
0U,
2U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
4U,
10U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx0[] = {
{0x30,0x30,mb_cm_wcv_GBK2Kx0x0,NULL,NULL,{mb_cm_nodev_GBK2Kx0x0,2U,mb_cm_dectab_GBK2Kx0x0,mb_cm_dectab_bv_GBK2Kx0x0}},
{0x31,0x35,mb_cm_wcv_GBK2Kx0x1,NULL,NULL,{mb_cm_nodev_GBK2Kx0x1,1U,mb_cm_dectab_GBK2Kx0x1,mb_cm_dectab_bv_GBK2Kx0x1}},
{0x36,0x36,mb_cm_wcv_GBK2Kx0x2,NULL,NULL,{mb_cm_nodev_GBK2Kx0x2,3U,mb_cm_dectab_GBK2Kx0x2,mb_cm_dectab_bv_GBK2Kx0x2}},
{0x37,0x37,mb_cm_wcv_GBK2Kx0x3,NULL,NULL,{mb_cm_nodev_GBK2Kx0x3,3U,mb_cm_dectab_GBK2Kx0x3,mb_cm_dectab_bv_GBK2Kx0x3}},
{0x38,0x38,mb_cm_wcv_GBK2Kx0x4,NULL,NULL,{mb_cm_nodev_GBK2Kx0x4,3U,mb_cm_dectab_GBK2Kx0x4,mb_cm_dectab_bv_GBK2Kx0x4}},
{0x39,0x39,mb_cm_wcv_GBK2Kx0x5,NULL,NULL,{mb_cm_nodev_GBK2Kx0x5,1U,mb_cm_dectab_GBK2Kx0x5,mb_cm_dectab_bv_GBK2Kx0x5}},
{0x40,0xFE,mb_cm_wcv_GBK2Kx0x6,NULL,NULL,{NULL,15U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx1[] = {
{0,0,0},
{0,0,0},
{0x0,0x2FC,765U},
{0x0,0x374,885U},
{0x0,0x1D6,471U},
{0x0,0x174,373U},
{0x0,0x1B8,441U},
{0x0,0x2BE,703U},
{0x0,0x45C,1117U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0xBE,191U},
{0xFB3,0x1AA7,2805U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx1[] = {
{0x0,0x2FC,0U},
{0x0,0x1A5,1U},
{0x1A6,0x374,2U},
{0x0,0x1D6,2U},
{0x0,0x174,3U},
{0x0,0xDD,3U},
{0xDE,0x1B8,4U},
{0x0,0x2BE,4U},
{0x0,0x45C,5U},
{0x0,0xBE,6U},
{0x0,0x1EE,0U},
{0x1EF,0x534,1U},
{0x535,0x67A,2U},
{0x67B,0x913,3U},
{0x914,0xA65,4U},
{0xA66,0xAF4,5U},
};
size_t mb_cm_dectab_bv_GBK2Kx1[] = {
0U,
0U,
0U,
1U,
3U,
4U,
5U,
7U,
8U,
9U,
9U,
9U,
9U,
9U,
9U,
10U,
16U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx1[] = {
{0x30,0x30,mb_cm_wcv_GBK2Kx1x0,NULL,NULL,{mb_cm_nodev_GBK2Kx1x0,5U,mb_cm_dectab_GBK2Kx1x0,mb_cm_dectab_bv_GBK2Kx1x0}},
{0x31,0x31,mb_cm_wcv_GBK2Kx1x1,NULL,NULL,{mb_cm_nodev_GBK2Kx1x1,3U,mb_cm_dectab_GBK2Kx1x1,mb_cm_dectab_bv_GBK2Kx1x1}},
{0x32,0x32,mb_cm_wcv_GBK2Kx1x2,NULL,NULL,{mb_cm_nodev_GBK2Kx1x2,7U,mb_cm_dectab_GBK2Kx1x2,mb_cm_dectab_bv_GBK2Kx1x2}},
{0x33,0x33,mb_cm_wcv_GBK2Kx1x3,NULL,NULL,{mb_cm_nodev_GBK2Kx1x3,7U,mb_cm_dectab_GBK2Kx1x3,mb_cm_dectab_bv_GBK2Kx1x3}},
{0x34,0x34,mb_cm_wcv_GBK2Kx1x4,NULL,NULL,{mb_cm_nodev_GBK2Kx1x4,7U,mb_cm_dectab_GBK2Kx1x4,mb_cm_dectab_bv_GBK2Kx1x4}},
{0x35,0x35,mb_cm_wcv_GBK2Kx1x5,NULL,NULL,{mb_cm_nodev_GBK2Kx1x5,3U,mb_cm_dectab_GBK2Kx1x5,mb_cm_dectab_bv_GBK2Kx1x5}},
{0x40,0xFE,mb_cm_wcv_GBK2Kx1x6,NULL,NULL,{NULL,14U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx2[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x45D,0x24A9,8269U},
{0x0,0x1D5,470U},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xBF,0x17D,191U},
{0x1AA8,0x1AF8,81U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx2[] = {
{0x0,0x1D87,0U},
{0x1D88,0x204C,1U},
{0x0,0x1D5,1U},
{0x0,0xBE,2U},
{0x0,0x50,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx2[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
3U,
3U,
3U,
3U,
3U,
4U,
5U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx2[] = {
{0x30,0x35,mb_cm_wcv_GBK2Kx2x0,NULL,NULL,{mb_cm_nodev_GBK2Kx2x0,1U,mb_cm_dectab_GBK2Kx2x0,mb_cm_dectab_bv_GBK2Kx2x0}},
{0x36,0x36,mb_cm_wcv_GBK2Kx2x1,NULL,NULL,{mb_cm_nodev_GBK2Kx2x1,4U,mb_cm_dectab_GBK2Kx2x1,mb_cm_dectab_bv_GBK2Kx2x1}},
{0x40,0xFE,mb_cm_wcv_GBK2Kx2x2,NULL,NULL,{NULL,14U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx3[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x1D6,0x202,45U},
{0x0,0x405,1030U},
{0x0,0x19,26U},
{0,0,0},
{0,0,0},
{0x17E,0x23C,191U},
{0x1AF9,0x47E3,11499U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx3[] = {
{0x0,0x2C,0U},
{0x0,0x3D5,0U},
{0x3D6,0x405,1U},
{0x0,0x19,1U},
{0x0,0xBE,3U},
{0x0,0xE8,0U},
{0xE9,0x58A,1U},
{0x58B,0x2CEA,2U},
};
size_t mb_cm_dectab_bv_GBK2Kx3[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
3U,
4U,
4U,
4U,
5U,
8U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx3[] = {
{0x30,0x30,mb_cm_wcv_GBK2Kx3x0,NULL,NULL,{mb_cm_nodev_GBK2Kx3x0,5U,mb_cm_dectab_GBK2Kx3x0,mb_cm_dectab_bv_GBK2Kx3x0}},
{0x31,0x31,mb_cm_wcv_GBK2Kx3x1,NULL,NULL,{mb_cm_nodev_GBK2Kx3x1,6U,mb_cm_dectab_GBK2Kx3x1,mb_cm_dectab_bv_GBK2Kx3x1}},
{0x32,0x39,mb_cm_wcv_GBK2Kx3x2,NULL,NULL,{mb_cm_nodev_GBK2Kx3x2,1U,mb_cm_dectab_GBK2Kx3x2,mb_cm_dectab_bv_GBK2Kx3x2}},
{0x40,0xFE,mb_cm_wcv_GBK2Kx3x3,NULL,NULL,{NULL,14U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx4[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x23D,0xA71,191U},
{0x47E4,0x2654B,12600U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx4[] = {
{0x0,0xBE,1U},
{0x0,0x3137,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx4[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx4[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx4x0,NULL,NULL,{mb_cm_nodev_GBK2Kx4x0,1U,mb_cm_dectab_GBK2Kx4x0,mb_cm_dectab_bv_GBK2Kx4x0}},
{0x40,0xFE,mb_cm_wcv_GBK2Kx4x1,NULL,NULL,{NULL,14U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx5[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x0,0x344B7,12600U},
{0,0,0},
{0xA72,0x1720,191U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx5[] = {
{0x0,0x3137,0U},
{0x0,0xBE,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx5[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
1U,
2U,
2U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx5[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx5x0,NULL,NULL,{mb_cm_nodev_GBK2Kx5x0,1U,mb_cm_dectab_GBK2Kx5x0,mb_cm_dectab_bv_GBK2Kx5x0}},
{0x40,0xFE,mb_cm_wcv_GBK2Kx5x1,NULL,NULL,{NULL,14U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx6[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x344B8,0xFC3EF,12600U},
{0x0,0x17DD,94U},
{0x1721,0x2FC1,97U},
{0,0,0},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx6[] = {
{0x0,0x3137,0U},
{0x0,0x5D,2U},
{0x0,0x60,1U},
};
size_t mb_cm_dectab_bv_GBK2Kx6[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
2U,
3U,
3U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx6[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx6x0,NULL,NULL,{mb_cm_nodev_GBK2Kx6x0,1U,mb_cm_dectab_GBK2Kx6x0,mb_cm_dectab_bv_GBK2Kx6x0}},
{0x40,0xA0,mb_cm_wcv_GBK2Kx6x1,NULL,NULL,{NULL,14U,NULL,NULL}},
{0xA1,0xFE,mb_cm_wcv_GBK2Kx6x2,NULL,NULL,{NULL,13U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx7[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0xFC3F0,0xFCEC7,2776U},
{0x17DE,0x183B,94U},
{0x2FC2,0x3022,97U},
{0x2654C,0x28BAB,9824U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx7[] = {
{0x0,0x9D7,0U},
{0x9D8,0xAD7,1U},
{0x0,0x5D,4U},
{0x0,0x60,3U},
{0x0,0x3EB,1U},
{0x3EC,0x265F,2U},
};
size_t mb_cm_dectab_bv_GBK2Kx7[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
2U,
3U,
4U,
6U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx7[] = {
{0x30,0x31,mb_cm_wcv_GBK2Kx7x0,NULL,NULL,{mb_cm_nodev_GBK2Kx7x0,1U,mb_cm_dectab_GBK2Kx7x0,mb_cm_dectab_bv_GBK2Kx7x0}},
{0x32,0x32,mb_cm_wcv_GBK2Kx7x1,NULL,NULL,{mb_cm_nodev_GBK2Kx7x1,3U,mb_cm_dectab_GBK2Kx7x1,mb_cm_dectab_bv_GBK2Kx7x1}},
{0x33,0x39,mb_cm_wcv_GBK2Kx7x2,NULL,NULL,{mb_cm_nodev_GBK2Kx7x2,1U,mb_cm_dectab_GBK2Kx7x2,mb_cm_dectab_bv_GBK2Kx7x2}},
{0x40,0xA0,mb_cm_wcv_GBK2Kx7x3,NULL,NULL,{NULL,14U,NULL,NULL}},
{0xA1,0xFE,mb_cm_wcv_GBK2Kx7x4,NULL,NULL,{NULL,13U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_wcv_GBK2Kx8[] = {
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0,0,0},
{0x183C,0x2283,94U},
{0x3023,0x3ABE,97U},
{0x28BAC,0x7EDCB,12600U},
};
mb_wchar_range_t mb_cm_dectab_GBK2Kx8[] = {
{0x0,0x5D,2U},
{0x0,0x60,1U},
{0x0,0x3137,0U},
};
size_t mb_cm_dectab_bv_GBK2Kx8[] = {
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
0U,
1U,
2U,
3U,
};
mb_char_node_t mb_cm_nodev_GBK2Kx8[] = {
{0x30,0x39,mb_cm_wcv_GBK2Kx8x0,NULL,NULL,{mb_cm_nodev_GBK2Kx8x0,1U,mb_cm_dectab_GBK2Kx8x0,mb_cm_dectab_bv_GBK2Kx8x0}},
{0x40,0xA0,mb_cm_wcv_GBK2Kx8x1,NULL,NULL,{NULL,14U,NULL,NULL}},
{0xA1,0xFE,mb_cm_wcv_GBK2Kx8x2,NULL,NULL,{NULL,13U,NULL,NULL}},
};
mb_wchar_range_t mb_cm_dectab_GBK2K[] = {
{0x0,0x1A05,0U},
{0x0,0x83D,0U},
{0x0,0x2FC,1U},
{0x0,0x374,1U},
{0x0,0x1D6,1U},
{0x0,0x174,1U},
{0x0,0x1B8,1U},
{0x0,0x2BE,1U},
{0x0,0x45C,1U},
{0x45D,0x24A9,2U},
{0x0,0x1D5,2U},
{0x1D6,0x202,3U},
{0x0,0x405,3U},
{0x0,0x19,3U},
{0x0,0x344B7,5U},
{0x344B8,0xFC3EF,6U},
{0xFC3F0,0xFCEC7,7U},
{0x0,0x17DD,6U},
{0x17DE,0x183B,7U},
{0x183C,0x2283,8U},
{0x0,0xBE,1U},
{0xBF,0x17D,2U},
{0x17E,0x23C,3U},
{0x23D,0xA71,4U},
{0xA72,0x1720,5U},
{0x1721,0x2FC1,6U},
{0x2FC2,0x3022,7U},
{0x3023,0x3ABE,8U},
{0x0,0xFB2,0U},
{0xFB3,0x1AA7,1U},
{0x1AA8,0x1AF8,2U},
{0x1AF9,0x47E3,3U},
{0x47E4,0x2654B,4U},
{0x2654C,0x28BAB,7U},
{0x28BAC,0x7EDCB,8U},
};
size_t mb_cm_dectab_bv_GBK2K[] = {
0U,
1U,
2U,
3U,
4U,
5U,
6U,
7U,
8U,
10U,
12U,
13U,
14U,
17U,
20U,
28U,
35U,
};
mb_char_node_t mb_cm_nodev_GBK2K[] = {
{0x81,0x81,mb_cm_wcv_GBK2Kx0,NULL,NULL,{mb_cm_nodev_GBK2Kx0,7U,mb_cm_dectab_GBK2Kx0,mb_cm_dectab_bv_GBK2Kx0}},
{0x82,0x82,mb_cm_wcv_GBK2Kx1,NULL,NULL,{mb_cm_nodev_GBK2Kx1,7U,mb_cm_dectab_GBK2Kx1,mb_cm_dectab_bv_GBK2Kx1}},
{0x83,0x83,mb_cm_wcv_GBK2Kx2,NULL,NULL,{mb_cm_nodev_GBK2Kx2,3U,mb_cm_dectab_GBK2Kx2,mb_cm_dectab_bv_GBK2Kx2}},
{0x84,0x84,mb_cm_wcv_GBK2Kx3,NULL,NULL,{mb_cm_nodev_GBK2Kx3,4U,mb_cm_dectab_GBK2Kx3,mb_cm_dectab_bv_GBK2Kx3}},
{0x85,0x8F,mb_cm_wcv_GBK2Kx4,NULL,NULL,{mb_cm_nodev_GBK2Kx4,2U,mb_cm_dectab_GBK2Kx4,mb_cm_dectab_bv_GBK2Kx4}},
{0x90,0xA0,mb_cm_wcv_GBK2Kx5,NULL,NULL,{mb_cm_nodev_GBK2Kx5,2U,mb_cm_dectab_GBK2Kx5,mb_cm_dectab_bv_GBK2Kx5}},
{0xA1,0xE1,mb_cm_wcv_GBK2Kx6,NULL,NULL,{mb_cm_nodev_GBK2Kx6,3U,mb_cm_dectab_GBK2Kx6,mb_cm_dectab_bv_GBK2Kx6}},
{0xE2,0xE2,mb_cm_wcv_GBK2Kx7,NULL,NULL,{mb_cm_nodev_GBK2Kx7,5U,mb_cm_dectab_GBK2Kx7,mb_cm_dectab_bv_GBK2Kx7}},
{0xE3,0xFE,mb_cm_wcv_GBK2Kx8,NULL,NULL,{mb_cm_nodev_GBK2Kx8,3U,mb_cm_dectab_GBK2Kx8,mb_cm_dectab_bv_GBK2Kx8}},
};
mb_char_map_t mb_cm_GBK2K = {
mb_cm_nodev_GBK2K,
9U,
mb_cm_dectab_GBK2K,
mb_cm_dectab_bv_GBK2K,
};
mb_wchar_range_t mb_wcv_GBK2K[] = {
{0x452,0x1E57,6662U},
{0x2643,0x2E80,2110U},
{0x361B,0x3917,765U},
{0x3CE1,0x4055,885U},
{0x4160,0x4336,471U},
{0x44D7,0x464B,373U},
{0x478E,0x4946,441U},
{0x49B8,0x4C76,703U},
{0x9FA6,0xC44F,9386U},
{0xE865,0xEA67,515U},
{0xFA2A,0xFE2F,1030U},
{0xFFE6,0xFFFF,26U},
{0x10000,0x10CEC7,1035976U},
{0x216084,0x218307,8836U},
{0x368000,0x36BABE,15039U},
{0x370000,0x3EEDCB,519628U},
};
mb_wchar_range_t mb_decmap_tab_GBK2K[] = {
{0x0,0x7F,0U},
{0x452,0x1E57,1U},
{0x2643,0x2E80,2U},
{0x361B,0x3917,3U},
{0x3CE1,0x4055,4U},
{0x4160,0x4336,5U},
{0x44D7,0x464B,6U},
{0x478E,0x4946,7U},
{0x49B8,0x4C76,8U},
{0x9FA6,0xC44F,9U},
{0xE865,0xEA67,10U},
{0xFA2A,0xFE2F,11U},
{0xFFE6,0xFFFF,12U},
{0x10000,0x10CEC7,13U},
{0x216084,0x218307,14U},
{0x368000,0x36BABE,15U},
{0x370000,0x3EEDCB,16U},
};
mb_decoder_t mb_decmap_destv_GBK2K[] = {
{NULL,NULL,0x0,&mb_cm_CLGL,0U},
{NULL,NULL,0x452,&mb_cm_GBK2K,0U},
{NULL,NULL,0x2643,&mb_cm_GBK2K,1U},
{NULL,NULL,0x361B,&mb_cm_GBK2K,2U},
{NULL,NULL,0x3CE1,&mb_cm_GBK2K,3U},
{NULL,NULL,0x4160,&mb_cm_GBK2K,4U},
{NULL,NULL,0x44D7,&mb_cm_GBK2K,5U},
{NULL,NULL,0x478E,&mb_cm_GBK2K,6U},
{NULL,NULL,0x49B8,&mb_cm_GBK2K,7U},
{NULL,NULL,0x9FA6,&mb_cm_GBK2K,8U},
{NULL,NULL,0xE865,&mb_cm_GBK2K,9U},
{NULL,NULL,0xFA2A,&mb_cm_GBK2K,10U},
{NULL,NULL,0xFFE6,&mb_cm_GBK2K,11U},
{NULL,NULL,0x10000,&mb_cm_GBK2K,12U},
{NULL,NULL,0x216084,&mb_cm_GBK2K,13U},
{NULL,NULL,0x368000,&mb_cm_GBK2K,14U},
{NULL,NULL,0x370000,&mb_cm_GBK2K,15U},
};
mb_decoder_map_t mb_decmap_GBK2K = {
mb_decmap_tab_GBK2K,
17U,
mb_decmap_destv_GBK2K,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_GBK2K(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_GBK2K);
}
size_t
mb_conv_ws_to_GBK2K(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_GBK2K);
}
#endif
static const char *mb_namev_GBK2K[] = {
"x-gb-18030-2000",
"x-gbk2k",
};
static mb_conv_t mb_convv_GBK2K[] = {
#ifdef USE_UCS
mb_conv_to_GBK2K,
#else
#endif
NULL,
};
mb_ces_t mb_ces_GBK2K = {
mb_namev_GBK2K,
mb_flag_plus,0,
{mb_G0,mb_GBK2K,{mb_94,mb_nSETs,mb_nSETs,mb_nSETs},{0x42,0x00,0x00,0x00}},
mb_convv_GBK2K,
&mb_decmap_GBK2K,
NULL,
1,
};
mb_encoder_t mb_encmap_nodev_GBK2K[] = {
{mb_wcv_CL,&mb_cm_nodev_CL[0U]},
{mb_wcv_CL,&mb_cm_nodev_CL[1U]},
{mb_wcv_CL,&mb_cm_nodev_CL[2U]},
{mb_wcv_CL,&mb_cm_nodev_CL[3U]},
{mb_wcv_GL,&mb_cm_nodev_GL[0U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[0U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[1U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[2U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[3U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[4U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[5U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[6U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[7U]},
{mb_wcv_GBK2K,&mb_cm_nodev_GBK2K[8U]}
};
short mb_encmap_iv_GBK2K[] = {
-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,-1,1,1,2,2,
-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,-1,-1,-1,-1,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
-1,5,6,7,8,9,9,9,9,9,9,9,9,9,9,9,
10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,
11,11,12,13,13,13,13,13,13,13,13,13,13,13,13,13,
13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,-1
};
mb_encoder_map_t mb_encmap_GBK2K = {
mb_encmap_nodev_GBK2K,
mb_encmap_iv_GBK2K,
};
#endif
mb_wchar_range_t mb_decmap_tab_TIS620[] = {
{0x0,0x7F,0U},
{0x206640,0x20669F,1U},
};
mb_decoder_t mb_decmap_destv_TIS620[] = {
{mb_CLGL_decoder,&mb_G0SL,0x0,&mb_cm_CLGL,0U},
{mb_96R_decoder,&mb_G1SR,0x205E00,&mb_cm_96R,0U},
};
mb_decoder_map_t mb_decmap_TIS620 = {
mb_decmap_tab_TIS620,
2U,
mb_decmap_destv_TIS620,
};
#ifdef USE_UCS
mb_wchar_t
mb_conv_to_TIS620(mb_wchar_t enc, mb_ces_t *ces)
{
return mb_conv_for_decoder(enc, &mb_decmap_TIS620);
}
size_t
mb_conv_ws_to_TIS620(mb_wchar_t *ws, mb_wchar_t *ws_end, mb_info_t *info)
{
return mb_conv_for_specific_ces(ws, ws_end, &mb_ces_TIS620);
}
#endif
static const char *mb_namev_TIS620[] = {
"tis-620",
"iso-8859-11",
};
static mb_conv_t mb_convv_TIS620[] = {
#ifdef USE_UCS
mb_conv_to_TIS620,
#else
#endif
NULL,
};
mb_ces_t mb_ces_TIS620 = {
mb_namev_TIS620,
mb_flag_plus,0,
{mb_G0,mb_G1,{mb_94,mb_96,mb_nSETs,mb_nSETs},{0x42,0x54,0x00,0x00}},
mb_convv_TIS620,
&mb_decmap_TIS620,
NULL,
1,
};
