#! /bin/bash

dir="${0%/*}"

if [ "X$dir" = "X$0" -o "X$dir" = X. ]; then
 dir=$PWD
fi

if [ "X$dir" = X -o "X$dir" = X. ]; then
 dir=${HOME}/src/cvs/libmoe/devel
fi

export LD_LIBRARY_PATH=${dir}:${LD_LIBRARY_PATH}
exec ${dir}/mbconv "$@"
