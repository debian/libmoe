# You need GNU C, GNU Make, GNU fileutils, and GNU binutils.

include version.mak

DESTDIR=
INSTALL=sh ./install.sh
PREFIX=/usr/local
INCDIR=${PREFIX}/include/moe
LIBSODIR=${PREFIX}/lib
LIBADIR=${LIBSODIR}
LIBEXECDIR=${PREFIX}/libexec/moe
BINDIR=${PREFIX}/bin
MANDIR=${PREFIX}/man
MANSECT=1
MANCOMPR=gzip -9c
MANX=.gz

UNICODE_DOC_DIR=${HOME}/doc/unicode
RFC_DIR=${HOME}/doc/rfc

DEST=libmoe
Os=altmalloc.o btri.o uirx.o wcrx.o mbces.o iso2mb.o mb2iso.o mbinit.o mbfind.o mbio.o jis0208-extra.o
Hs=mbversion.h mbcesdefs.h mbcesconf.h mblangconf.h altmalloc.h btri.h uirx.h wcrx.h mbces.h mb.h

BTRI_Hs=\
notascii.h \
mbwcweight.h \
jis1flag.h \
jis0208-to-ucs-extra.h \
ucs-to-jis0208-extra.h \
full-to-half.h \
half-to-full.h \
iso-to-ucs.h \
ucs-to-iso.h \
xterm_ucswidth.h \
eaw_a2n_ucswidth.h \
eaw_a2w_ucswidth.h \
mbtermtab.h \
prop.h \
mbcestab.h \
mbconvtab.h \
mbflagtab.h \
mbdetectortab.h

BTRI_Cs=

ALL=${DEST}.so
INST=${Hs}
INSTLIBA=${DEST}.a
INSTLIBSO=${DEST}.so ${DEST}.so.${VER}.${RELEASE} ${DEST}.so.${VER}.${RELEASE}.${LEVEL}
INSTLIBEXEC=mk_btri${EXT}
INSTBIN=mbconv${EXT}

DEBUG=
CF=${DEBUG} -O2 -Wall -fpic -I.
LF=-shared -Wl,-soname,${DEST}.so.${VER}.${RELEASE}
CC=gcc
CPP=gcc -E
LD=gcc
RANLIB=ranlib
AR=ar

#  Hideyuki SHIRAI <shirai@rdmg.mgcs.mei.co.jp> informed me that
#  Solaris7 requires definitions:
#LF=-dy -G -I  ${DEST}.so.${VER}.${RELEASE}
#LD=ld

#  Tsutomu OKADA <tsutomu.okada@furuno.co.jp> informed me that
#  Cygwin requires command extension:
#EXT=.exe

all: ${INSTLIBA} mbconv mbconv.man

mbconv: mbconv.c ${ALL}
	${CC} ${CF} -o $@ $< -L. -lmoe


${DEST}.so: ${DEST}.so.${VER}.${RELEASE}
	ln -sf $< $@

${DEST}.so.${VER}.${RELEASE}: ${DEST}.so.${VER}.${RELEASE}.${LEVEL}
	ln -sf $< $@

${DEST}.so.${VER}.${RELEASE}.${LEVEL}: ${Os}
	${LD} ${LF} -o $@ $^

${DEST}.a: ${Os}
	${AR} cru $@ $^
	${RANLIB} $@

%.o: %.c
	${CC} ${CF} -c $<

altmalloc.o: altmalloc.c altmalloc.h
btri.o: btri.c btri.h altmalloc.h
uirx.o: uirx.c uirx.h altmalloc.h
wcrx.o: wcrx.c wcrx.h altmalloc.h uirx.h
iso2mb.o: iso2mb.c ${Hs} notascii.h mbwcweight.h
mb2iso.o: mb2iso.c ${Hs} jis1flag.h domestic-ascii.h full-to-half.h half-to-full.h iso-to-ucs.h ucs-to-iso.h ucs-to-iso-pool.h
mbfind.o: mbfind.c ${Hs} xterm_ucswidth.h eaw_a2n_ucswidth.h eaw_a2w_ucswidth.h mbtermtab.h prop.h
mbio.o: mbio.c ${Hs}
jis0208-extra.o: jis0208-extra.c ${Hs} jis0208-to-ucs-extra.h ucs-to-jis0208-extra.h
mbinit.o: mbinit.c ${Hs} mbcestab.h mbconvtab.h mbflagtab.h mbdetectortab.h
mbces.o: mbces.c ${Hs}

mbversion.h: version.mak
	echo "#define MB_VERSION_STRING \"${VER}.${RELEASE}.${LEVEL}${EXTRA}\"" >$@
	echo "#define MB_VERSION (${VER}*10000+${RELEASE}*100+${LEVEL})" >>$@

${BTRI_Hs}: %.h: %.mk_btri.h mk_btri
	-rm -f $@
	${CPP} ${CF} $< | ./mk_btri >$@-new && mv $@-new $@

${BTRI_Cs}: %.c: %.mk_btri.c mk_btri
	-rm -f $@
	./mk_btri <$< >$@-new && mv $@-new $@

mk_btri: btri.c btri.h altmalloc.o
	${CC} ${CF} -DMAIN -o $@ $< altmalloc.o

mbconv.man: mbconv.pod
	pod2man --section=${MANSECT} --center=' ' $< >$@

install: install-bin install-lib install-man

install-bin: mbconv mk_btri
	test "X${INSTLIBSO}" \!= X && { \
		rm -f \
		${DESTDIR}${LIBSODIR}/${DEST}.so \
		${DESTDIR}${LIBSODIR}/${DEST}.so.${VER}.${RELEASE} \
		${DESTDIR}${LIBSODIR}/${DEST}.so.${VER}.${RELEASE}.* ; \
		mkdir -p ${DESTDIR}${LIBSODIR} ; \
		test -d ${DESTDIR}${LIBSODIR} && ${INSTALL} ${DESTDIR}${LIBSODIR} ${INSTLIBSO} ; \
	}
	test "X${INSTBIN}" \!= X && { \
		mkdir -p ${DESTDIR}${BINDIR} ; \
		test -d ${DESTDIR}${BINDIR} && ${INSTALL} ${DESTDIR}${BINDIR} ${INSTBIN} ; \
	}

install-lib: ${INSTLIBA}
	test "X${INST}" \!= X && { \
		mkdir -p ${DESTDIR}${INCDIR} ; \
		test -d ${DESTDIR}${INCDIR} && ${INSTALL} ${DESTDIR}${INCDIR} ${INST} ; \
	}
	test "X${INSTLIBA}" \!= X && { \
		mkdir -p ${DESTDIR}${LIBADIR} ; \
		test -d ${DESTDIR}${LIBADIR} && ${INSTALL} ${DESTDIR}${LIBADIR} ${INSTLIBA} ; \
	}
	test "X${INSTLIBEXEC}" \!= X && { \
		mkdir -p ${DESTDIR}${LIBEXECDIR} ; \
		test -d ${DESTDIR}${LIBEXECDIR} && ${INSTALL} ${DESTDIR}${LIBEXECDIR} ${INSTLIBEXEC} ; \
	}

install-man: mbconv.man
	mkdir -p ${DESTDIR}${MANDIR}/man${MANSECT}
	test -d ${DESTDIR}${MANDIR}/man${MANSECT} && \
		${MANCOMPR} <mbconv.man >${DESTDIR}${MANDIR}/man${MANSECT}/mbconv.${MANSECT}${MANX}

clean:
	rm -f ${DEST}.a ${DEST}.so* *.o mk_btri mbconv ${BTRI_Hs}

clean-oldso:
	for f in ${DEST}.so.${VER}.${RELEASE}.*; do \
		test "X$$f" = "X${DEST}.so.${VER}.${RELEASE}.${LEVEL}" || rm -f $$f ; \
	done

mbenclib.ph: mb.h
	-rm -f $@
	sed -ne '/ BEGIN h2ph /,/ END h2ph /p' $^ | h2ph -use_eval=0 >$@-new && mv $@-new $@

ucs-to-iso: mbenclib.ph
	perl ucs-to-iso.pl ${UNICODE_DOC_DIR}/UnicodeData-Latest.txt ${UNICODE_DOC_DIR}/*.TXT ${UNICODE_DOC_DIR}/*.xml

mbwcweight: mbenclib.ph
	perl $@.pl

domestic-ascii: mbenclib.ph
	perl domestic-ascii.pl

eaw_ucswidth:
	perl eaw_ucswidth.pl ${UNICODE_DOC_DIR}/EastAsianWidth.txt

jis0208-extra:
	perl jis0208-extra.pl ${UNICODE_DOC_DIR}/JIS0208.TXT.extra

release: clean ucs-to-iso mbwcweight domestic-ascii eaw_ucswidth jis0208-extra

btri_hs: ${BTRI_Hs}

btri_cs: ${BTRI_Cs}

.PHONY: all install install-bin install-lib install-man clean clean-oldso \
ucs-to-iso mbwcweight domestic-ascii eaw_ucswidth release \
btri_hs btri_cs
