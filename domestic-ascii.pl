# Perl5 script to make a table of domestic ascii based on
#   <URL:http://kanji.zinbun.kyoto-u.ac.jp/~yasuoka/CJK.html>.
# Invoke as:
#   perl domestic-ascii.pl

BEGIN {require 'mbenclib.pl'}

$da_h = 'domestic-ascii.h' if (!defined($da_h));

local (*H);

open(H, ">$da_h") || die "open(H, \">$da_h\"): $!";

my @da = 
  (
   0x40,
   0x41,
   0x42,
   0x43,
   0x47,
   0x48,
   0x4A,
   0x4B,
   0x4C,
   0x52,
   0x54,
   0x59,
   0x5A,
   0x60,
   0x61,
   0x66,
   0x67,
   0x68,
   0x69,
   0x6E,
   0x75,
   0x77,
   0x78,
   0x7A,
   0x81,
   0x82,
   );

my $hole = 0;

foreach $fc (@da) {
  for (; $hole < $fc ; ++$hole) {
    printf H "0,\n";
  }

  ++$hole;
  printf H "1,\n";
}
