target=$1
shift

for f in "$@"
do
 if [ -L "$f" ]
 then
  ln -s `ls -l "$f" | sed 's/.*  *->  *//'` "$target/$f"
 else
  cp -p "$f" "$target"
 fi
done
