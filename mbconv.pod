=head1 NAME

B<mbconv> - Character encoding scheme converter

=head1 SYNOPSIS

  mbconv [options] <file> ...

=head1 DESCRIPTION

This is an application of
a library
to handle multiple octets character encoding:

  http://pub.ks-and-ks.ne.jp/prog/libmoe/

mainly written for debugging of the library.

It reads octet by octet from files given on command line
(or standard input if no file is specified),
converts character encoding scheme (CES) as specified by command line options
(described below),
and output to standard output
(or a file specified by B<-t> option or B<-a> option).

=head2 Options

=over 4

=item B<-?>, B<-h>, B<--help>

display summary of options and exits.

=item B<-a> I<file>, B<--append-to>=I<file>

output is appended to I<file>.

=item B<-c> I<converters>, B<--convert-to>=I<converters>

specifies character encoding conversion.
I<converters> must be comma separated list of
words described in L<Conversion specifiers>.

=item B<-f> I<flags>, B<--flag>=I<flags>

specifies flags to change behavior of conversion.
I<flags> must be comma separated list of
words describe in L<Flag specifiers>.

=item B<-i>, B<--input>

succeeding options apply to input stream.

=item B<-m> I<string>, B<--mime-charset>=I<string>

mime encoding conforming to RFC2047 is performed.
<string> is used as charset name.

=item B<-n>, B<--line-number>

line number (E<gt>= 1) is inserted to beginning of each line.

=item B<-o>, B<--output>

succeeding options apply to output stream.

=item B<-t> I<file>, B<--to>=I<file>

output to file (truncated).

=item B<-w>, B<--width>

output width of each line.

=item B<-cs> <string>, B<--charset>=I<string>

specifies charset name.
Some language specifications are also accepted as well as MIME charset names,
which are used to restrict candidates of encoding scheme of input stream.
Acceptable languages are listed in L<Acceptable languages>.

=item B<--format>=I<string>

specifies output format

=item B<--which>

output charset name of each input stream to stderr,
in the form

I<file name>C<:> I<charset name>

if two or more files are specifed on the command line, or

I<charset name>

otherwise.

=item B<--regex>=I<regular expression>

specifies regeular expression to filter output.
Character encoding of I<regular expression> can be specified
by putting string of the form

C<*>I<charset name>C<*>

at the beginning of the regular expression,
otherwise B<UTF-8>.
Character encoding of the expression
is converted to that of output stream
before matching.

=back

=head2 Conversion specifiers

Conversion is applied
just before each character is output to stream.
Conversion setup is automatically performed
based on CES.
So in most cases,
yo need not to specify converters explicitly.

=over 4

=item B<ascii>

domestic ASCII converted to US-ASCII,

=item B<ces>

converted appropriately according to
the CES bound to input/output stream,

=item B<to-ucs>

converted to Unicode,

=item B<f2h>, B<full-to-half>

Fullwidth compatibility characters are converted to
corresponding halfwidth ones,

=item B<h2f>, B<half-to-full>

Halfwidth compatibility characters are converted to
corresponding fullwidth ones,

=item B<jisx0213>

Codepoints in JIS C 6226 or in JIS X 0208 which are bound to no
character are converted into JIS X 0213 plane 1,

=item B<jisx0213-aggressive>

All codepoints in JIS C 6226 or in JIS X 0208 are converted into JIS X
0213 plane 1,

=item B<ms-latin1>

Unicode characters of code point between 0x80 and
0x9F (both inclusive) are converted to other
Unicode characters as if they are characters of those
code point in Microsoft Windows Codepage 1252.

=item B<ucs-to-jis0208-extra>, B<jis0208-to-ucs-extra>

Converters between some JIS X 0208 and Unicode characters having similar glyphs
(by the courtesy of Ambrose Li E<lt>acli@ada.dhs.orgE<gt>).

=back

=head2 Flag specifiers

=over 4

=item B<use-0x28-for-94x94inG0>, B<28>

use "1/11 2/4 2/8 F"
instead of "1/11 2/4 F"
to designate charsets with final octet
4/0, 4/1, or 4/2 to G0,

=item B<ac>, B<ascii-at-control>

escape sequence "1/11 2/8 4/2" is
output before every control character,

=item B<nossl>, B<ignore-7bit-single-shift>

escape sequence for 7 bit single shift
is ignored,

=item B<dnc>, B<discard-notprefered-char>

discard characters which CES bound
to output stream can not decode.

=back

=head2 Acceptable languages

The following words may be given instead of MIME charset name for input stream.
In that case,
encoding scheme
is automatically detected
(hopefully)
among
succeeding ones.

=over 4

=item B<c>, B<cn>, B<china>, B<chinese>

x-gb-18030-2000, cn-big5, utf-8, or x-euc-tw.

=item B<j>, B<ja>, B<jp>, B<japan>, B<japanese>

euc-jp, shift_jis, or utf-8.

=item B<k>, B<ko>, B<kr>, B<korea>, B<korean>

euc-kr, x-johab, utf-8, or x-unified-hangul.

=item B<cjk>

iso-8859-1,
x-gb-18030-2000,
cn-big5,
x-euc-tw,
euc-jp,
shift_jis,
euc-kr,
x-johab,
x-unified-hangul,
or utf-8.

=back

=head1 AUTHOR

Kiyokazu SUTO E<lt>suto@ks-and-ks.ne.jpE<gt>

=head1 DISCLAIMER etc.

This program is distributed with
absolutely no warranty.

Anyone can use, modify, and re-distibute this program
without any restriction.
