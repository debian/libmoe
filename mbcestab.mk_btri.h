#include "mbcesconf.h"
%%BEGIN default_ces_tab
"us-ascii",&mb_ces_ASCII
"ascii",&mb_ces_ASCII
"gb2312",&mb_ces_GB2312
"cn-gb",&mb_ces_GB2312
"cn-gb-isoir165",&mb_ces_GB_ISO_IR_165
"iso-2022-cn",&mb_ces_CN
"iso-2022-cn-ext",&mb_ces_CN_EXT
"euc-jp",&mb_ces_EUC_JP
"x-euc-jp",&mb_ces_EUC_JP
"ujis",&mb_ces_EUC_JP
"jis8",&mb_ces_EUC_JP
"euc-jisx0213",&mb_ces_EUC_JISX0213
"x-euc-jisx0213",&mb_ces_EUC_JISX0213
"iso-2022-jp",&mb_ces_ISO2022JP
"jis",&mb_ces_ISO2022JP
"jis7",&mb_ces_ISO2022JP
"iso-2022-jp-2",&mb_ces_ISO2022JP2
"iso-2022-jp-3",&mb_ces_ISO2022JP3
"euc-kr",&mb_ces_EUC_KR
"x-euc-kr",&mb_ces_EUC_KR
"iso-2022-kr",&mb_ces_ISO2022KR
"iso-8859-1",&mb_ces_ISO8859_1
"iso-8859-2",&mb_ces_ISO8859_2
"iso-8859-3",&mb_ces_ISO8859_3
"iso-8859-4",&mb_ces_ISO8859_4
"iso-8859-5",&mb_ces_ISO8859_5
"iso-8859-6",&mb_ces_ISO8859_6
"iso-8859-7",&mb_ces_ISO8859_7
"iso-8859-8",&mb_ces_ISO8859_8
"iso-8859-9",&mb_ces_ISO8859_9
"iso-8859-10",&mb_ces_ISO8859_10
"x-ctext",&mb_ces_XCTEXT
"utf-8",&mb_ces_UTF8
"utf-16",&mb_ces_UTF16
"utf-16be",&mb_ces_UTF16BE
"utf-16le",&mb_ces_UTF16LE
"x-moe-internal",&mb_ces_MOEINTERNAL
#ifdef USE_KOI8R
"koi8-r",&mb_ces_KOI8R
#endif
#ifdef USE_KOI8U
"koi8-u",&mb_ces_KOI8U
#endif
#ifdef USE_WIN1250
"windows-1250",&mb_ces_WIN1250
#endif
#ifdef USE_WIN1251
"windows-1251",&mb_ces_WIN1251
#endif
#ifdef USE_WIN1252
"windows-1252",&mb_ces_WIN1252
#endif
#ifdef USE_WIN1253
"windows-1253",&mb_ces_WIN1253
#endif
#ifdef USE_WIN1254
"windows-1254",&mb_ces_WIN1254
#endif
#ifdef USE_WIN1255
"windows-1255",&mb_ces_WIN1255
#endif
#ifdef USE_WIN1256
"windows-1256",&mb_ces_WIN1256
#endif
#ifdef USE_WIN1257
"windows-1257",&mb_ces_WIN1257
#endif
#ifdef USE_WIN1258
"windows-1258",&mb_ces_WIN1258
#endif
#ifdef USE_SJIS
"shift_jis",&mb_ces_SJIS
"x-sjis",&mb_ces_SJIS
"sjis",&mb_ces_SJIS
"shift-jis",&mb_ces_SJIS
#endif
#ifdef USE_SJIS0213
"shift_jisx0213",&mb_ces_SJIS0213
"shift-jisx0213",&mb_ces_SJIS0213
#endif
#ifdef USE_BIG5
"big5",&mb_ces_BIG5
"cn-big5",&mb_ces_BIG5
#endif
#ifdef USE_JOHAB
"x-johab",&mb_ces_JOHAB
#endif
#ifdef USE_UHANG
"x-unified-hangle",&mb_ces_UHANG
"x-uhc",&mb_ces_UHANG
#endif
#ifdef USE_EUC_TW
"x-euc-tw",&mb_ces_EUC_TW
"euc-tw",&mb_ces_EUC_TW
#endif
#ifdef USE_EUC_JISX0213_PACKED
"x-euc-jisx0213-packed",&mb_ces_EUC_JISX0213_PACKED
#endif
#ifdef USE_GBK
"x-gbk",&mb_ces_GBK
#endif
#ifdef USE_GBK2K
"x-gb-18030-2000",&mb_ces_GBK2K
"x-gbk2k",&mb_ces_GBK2K
#endif
"tis-620",&mb_ces_TIS620
"iso-8859-11",&mb_ces_TIS620
