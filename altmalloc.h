#ifndef ALTMALLOC_H
#define ALTMALLOC_H

#include <stdarg.h>
#include <stddef.h>
#include <stdlib.h>

typedef void *(*alt_allocater_t)(size_t);
typedef void *(*alt_reallocater_t)(void *, size_t);
typedef void (*alt_freer_t)(void *);

extern alt_allocater_t alt_malloc;
extern alt_allocater_t alt_malloc_atomic;
extern alt_reallocater_t alt_realloc;
extern alt_freer_t alt_free;

extern alt_allocater_t alt_set_allocater(alt_allocater_t new);
#define alt_call_malloc(size) (alt_malloc ? alt_malloc((size)) : NULL)
extern alt_allocater_t alt_set_atomic_allocater(alt_allocater_t new);
#define alt_call_malloc_atomic(size) (alt_malloc_atomic ? alt_malloc_atomic((size)) : NULL)
extern alt_reallocater_t alt_set_reallocater(alt_reallocater_t new);
#define alt_call_realloc(p, size) (alt_realloc ? alt_realloc((p), (size)) : NULL)
extern alt_freer_t alt_set_freer(alt_freer_t new);
#define alt_call_free(garbage) do { if (alt_free) alt_free((garbage)); } while (0)
#define alt_has_free_p() (!!alt_free)
extern int alt_malloc_vs(ptrdiff_t *p_size, ptrdiff_t n, ptrdiff_t limit, ...);

#endif
