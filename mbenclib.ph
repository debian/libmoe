sub MB_MBC_LEN_MAX () {(4);}
sub MB_UCS_BITLEN_MAX () {(21);}
sub MB_NON_UCS_LOWER () {(1 <<  &MB_UCS_BITLEN_MAX);}
sub MB_NON_UCS_MARK () {((~0 <<  &MB_UCS_BITLEN_MAX) & ~(~0 << ( &MB_MBC_LEN_MAX * 6)));}
sub MB_U2I_POOL_LAST_MARK () {(1 << 31);}
sub MB_Gn_LEN () {(2);}
sub MB_Gn_MASK () {((1 <<  &MB_Gn_LEN) - 1);}
sub MB_Sn_LEN () {(2);}
sub MB_Sn_MASK () {((1 <<  &MB_Gn_LEN) - 1);}
sub MB_GnSn_ENC {
    local($gn, $sn) = @_;
    ((($sn) <<  &MB_Gn_LEN) | ($gn))
}
sub MB_GnSn_ENC_Gn {
    local($gnsn) = @_;
    (( &gn) &  &MB_Gn_MASK)
}
sub MB_GnSn_ENC_Sn {
    local($gnsn) = @_;
    ((( &sn) >>  &MB_Gn_LEN) &  &MB_Sn_MASK)
}
sub MB_ESC_FC_LEN () {(6);}
sub MB_ESC_FC_MASK () {((1 <<  &MB_ESC_FC_LEN) - 1);}
sub MB_ESC_FC_BASE () {(1 <<  &MB_ESC_FC_LEN);}
sub MB_ESC_IC_LEN () {(2);}
sub MB_ESC_IC_MASK () {((1 <<  &MB_ESC_IC_LEN) - 1);}
sub MB_ASCII_FC () {(0x42);}
sub MB_UTF8_BEG () {"\x47";}
sub MB_NOTCHAR_FC () {(0x40);}
sub MB_CTL_FC () {(0x41);}
sub MB_UCS_FC () {(0x40);}
sub MB_FAKEUCS_FC () {(0x41);}
sub MB_94_UNIT () {(94);}
sub MB_94_LOWER () {( &MB_NON_UCS_LOWER);}
sub MB_94_UPPER () {( &MB_94_LOWER + (1 << ( &MB_ESC_IC_LEN +  &MB_ESC_FC_LEN)) *  &MB_94_UNIT);}
sub MB_96_UNIT () {(96);}
sub MB_96_LOWER () {( &MB_94_UPPER);}
sub MB_96_UPPER () {( &MB_96_LOWER + (1 << ( &MB_ESC_IC_LEN +  &MB_ESC_FC_LEN)) *  &MB_96_UNIT);}
sub MB_SBC_UNIT_LEN () {(7);}
sub MB_SBC_UNIT () {(1 <<  &MB_SBC_UNIT_LEN);}
sub MB_SBC_UNIT_MASK () {( &MB_SBC_UNIT - 1);}
sub MB_SBC_LOWER () {( &MB_96_UPPER);}
sub MB_SBC_UPPER () {( &MB_SBC_LOWER + (1 << ( &MB_ESC_IC_LEN +  &MB_ESC_FC_LEN +  &MB_SBC_UNIT_LEN)));}
sub MB_SBC_ESC_LEN () {( &MB_ESC_IC_LEN +  &MB_ESC_FC_LEN);}
sub MB_SBC_ESC_MASK () {((1 <<  &MB_SBC_ESC_LEN) - 1);}
sub MB_SBC_ESC_ENC {
    local($fc) = @_;
    ((($fc) & ~ &MB_ESC_FC_BASE) &  &MB_SBC_ESC_MASK)
}
sub MB_WORD_94_ENC {
    local($fc, $c) = @_;
    (($fc) ==  &MB_ASCII_FC ? ($c) + 0x21 :  &MB_94_LOWER +  &MB_SBC_ESC_ENC(($fc)) *  &MB_94_UNIT + ($c))
}
sub MB_WORD_96_ENC {
    local($fc, $c) = @_;
    ( &MB_96_LOWER +  &MB_SBC_ESC_ENC(($fc)) *  &MB_96_UNIT + ($c))
}
sub MB_WORD_SBC_ENC {
    local($fc, $c) = @_;
    ( &MB_SBC_LOWER +  &MB_SBC_ESC_ENC(($fc)) *  &MB_SBC_UNIT + ($c))
}
sub MB_CTL_ENC {
    local($c) = @_;
    (($c) & ~ &MB_SBC_UNIT_MASK ?  &MB_WORD_SBC_ENC( &MB_CTL_FC, ($c) &  &MB_SBC_UNIT_MASK) : ($c))
}
sub MB_MBC_LOWER () {( &MB_SBC_UPPER);}
sub MB_DBC_FC_LEN () {( &MB_ESC_FC_LEN - 1);}
sub MB_DBC_FC_MASK () {((1 <<  &MB_DBC_FC_LEN) - 1);}
sub MB_94x94_UNIT () {(94 * 94);}
sub MB_94x94_LOWER () {( &MB_MBC_LOWER);}
sub MB_94x94_UPPER () {( &MB_94x94_LOWER + (1 << ( &MB_ESC_IC_LEN +  &MB_DBC_FC_LEN)) *  &MB_94x94_UNIT);}
sub MB_DBC_UNIT_LEN () {(15);}
sub MB_DBC_UNIT () {(1 <<  &MB_DBC_UNIT_LEN);}
sub MB_DBC_UNIT_MASK () {( &MB_DBC_UNIT - 1);}
sub MB_DBC_LOWER () {( &MB_94x94_UPPER);}
sub MB_DBC_UPPER () {( &MB_DBC_LOWER + (1 << ( &MB_ESC_IC_LEN +  &MB_DBC_FC_LEN +  &MB_DBC_UNIT_LEN)));}
sub MB_DBC_ESC_LEN () {( &MB_ESC_IC_LEN +  &MB_DBC_FC_LEN);}
sub MB_DBC_ESC_MASK () {((1 <<  &MB_DBC_ESC_LEN) - 1);}
sub MB_DBC_ESC_ENC {
    local($fc) = @_;
    ((($fc) & ~ &MB_ESC_FC_BASE) &  &MB_DBC_ESC_MASK)
}
sub MB_WORD_94x94_ENC {
    local($fc, $c) = @_;
    ( &MB_94x94_LOWER +  &MB_DBC_ESC_ENC(($fc)) *  &MB_94x94_UNIT + ($c))
}
sub MB_WORD_DBC_ENC {
    local($fc, $c) = @_;
    ( &MB_DBC_LOWER +  &MB_DBC_ESC_ENC(($fc)) *  &MB_DBC_UNIT + ($c))
}
sub MB_WORD_ENC {
    local($set, $fc, $c) = @_;
    (($set) <  &mb_SBC ? (($set) <  &mb_96 ?  &MB_WORD_94x94_ENC(($fc), ($c)) : ($set) <  &mb_94 ?  &MB_WORD_96_ENC(($fc), ($c)) : ($fc) ==  &MB_ASCII_FC ? ($c) + 0x21 :  &MB_WORD_94_ENC(($fc), ($c))) : ($set) <  &mb_DBC ?  &MB_WORD_SBC_ENC(($fc), ($c)) : ($fc) ==  &MB_UCS_FC ? ($c) :  &MB_WORD_DBC_ENC(($fc), ($c)))
}
sub MB_IN_JISC6226 () {(1 << 0);}
sub MB_IN_JISX0208 () {(1 << 1);}
sub MB_IN_JISX0213_1 () {(1 << 2);}
sub MB_IN_JIS1COMMON () {( &MB_IN_JISC6226 |  &MB_IN_JISX0208 |  &MB_IN_JISX0213_1);}
sub MB_CPROP_IS_SPACE () {(1 << 0);}
sub MB_CPROP_NEVER_BOL () {(1 << 1);}
sub MB_CPROP_NEVER_EOL () {(1 << 2);}
sub MB_CPROP_MAY_BREAK () {(1 << 3);}
sub MB_CPROP_EOL_TO_NULL () {(1 << 4);}
sub MB_NCPROPS () {(5);}
1;
